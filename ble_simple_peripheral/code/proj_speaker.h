/**
 * Copyright (c) 2019, Tsingtao Freqchip
 *
 * All rights reserved.
 *
 *
 */
#ifndef PROJ_SPEAKER_H
#define PROJ_SPEAKER_H

/*
* INCLUDES (包含头文件)
*/
#include <stdbool.h>
/*
 * MACROS (宏定义)
 */

/*
 * CONSTANTS (常量定义)
 */

/*
 * TYPEDEFS (类型定义)
 */

/*
 * GLOBAL VARIABLES (全局变量)
 */



/*
 * LOCAL VARIABLES (本地变量)
 */

/*
 * LOCAL FUNCTIONS (本地函数)
 */

/*
 * EXTERN FUNCTIONS (外部函数)
 */
/*
 * PUBLIC FUNCTIONS (全局函数)
 */
void speaker_start(void);
void speaker_ini(void);


void test_speaker(void);
void test_sw_decoder(void);

#endif

