PROJECT_NAME     := AromatherapyDiffuser

BUILD_DATE := $(shell date +"%Y%m%d")
MAIN_VERSION := 0
SUB_VERSION := 0
BUILD_NUMBER := 1

SOFTWARE_VERSION := V$(MAIN_VERSION).$(SUB_VERSION).$(BUILD_NUMBER)
FIRM_REV_VERSION := $(SOFTWARE_VERSION)-$(BUILD_DATE)
HARDWARE_VERSION := V0.1.0

DIS_MANUFACTURER_NAME=UAVI TECH 
DIS_MODEL_NB_STR="Aromatherapy Diffuser L1"
DIS_FIRM_REV_STR=$(SOFTWARE_VERSION)
DIS_SYSTEM_ID=\x12\x34\x56\xFF\xFE\x9A\xBC\xDE
DIS_SW_REV_STR=$(SOFTWARE_VERSION)
DIS_HARD_REV_STR=$(HARDWARE_VERSION)


OUTPUT_DIRECTORY := build

SDK_ROOT := .
PROJECT_DIR := ./AromatherapyDiffuser
hardware_dir := ./AromatherapyDiffuser/hardware
LINKER_SCRIPT  := ldscript.ld
BUILD_DATE := $(shell date +"%Y%m%d")

TARGETS := $(PROJECT_NAME)-$(SOFTWARE_VERSION)-$(BUILD_DATE)

VERSION_FLAG = \
	-DDEV_MVID=$(MAIN_VERSION) -DDEV_SVID=$(SUB_VERSION) -DDEV_BVID=$(BUILD_NUMBER)

DEV_INFO_FLAGS = \
	-D$(PROJECT_NAME) \
	'-DDIS_MANUFACTURER_NAME="$(DIS_MANUFACTURER_NAME)"' \
	'-DDIS_MODEL_NB_STR=$(DIS_MODEL_NB_STR)' \
	'-DDIS_FIRM_REV_STR="$(DIS_FIRM_REV_STR)"' \
	'-DDIS_SYSTEM_ID="$(DIS_SYSTEM_ID)"' \
	'-DDIS_SW_REV_STR="$(DIS_SW_REV_STR)"' \
	'-DDIS_HARD_REV_STR="$(DIS_HARD_REV_STR)"' \
	$(VERSION_FLAG)

# TARGETS := $(PROJECT_NAME)-$(VERSION)-$(DATE_TIME)
# Source files common to all targets
SRC_FILES += \
  	$(PROJECT_DIR)/project/proj_main.c \
  	$(PROJECT_DIR)/project/user_task.c \
  	$(PROJECT_DIR)/project/woodmaster.c \
  	$(PROJECT_DIR)/project/ble_dev.c \
  	$(PROJECT_DIR)/project/music.c \
  	$(PROJECT_DIR)/project/proj_speaker.c \
	$(PROJECT_DIR)/project/atd_function.c \
	$(PROJECT_DIR)/project/audio_extern_flash.c \
 	$(SDK_ROOT)/components/ble/profiles/ble_woodmaster/woodmaster_service.c \
 	$(SDK_ROOT)/components/ble/profiles/ble_dev_info/dev_info_service.c \
 	$(SDK_ROOT)/components/ble/profiles/ble_batt/batt_service.c \
 	$(SDK_ROOT)/components/driver/driver_iic.c \
 	$(SDK_ROOT)/components/driver/driver_pmu.c \
 	$(SDK_ROOT)/components/driver/driver_pwm.c \
 	$(SDK_ROOT)/components/driver/driver_pmu_pwm.c \
 	$(SDK_ROOT)/components/driver/driver_pmu_qdec.c \
 	$(SDK_ROOT)/components/driver/driver_rtc.c \
 	$(SDK_ROOT)/components/driver/driver_uart.c \
 	$(SDK_ROOT)/components/driver/driver_wdt.c \
 	$(SDK_ROOT)/components/driver/driver_i2s.c \
 	$(SDK_ROOT)/components/driver/driver_codec.c \
 	$(SDK_ROOT)/components/driver/driver_exti.c \
 	$(SDK_ROOT)/components/driver/driver_timer.c \
	$(SDK_ROOT)/components/driver/driver_efuse.c \
	$(SDK_ROOT)/components/driver/driver_adc.c \
	$(SDK_ROOT)/components/modules/platform/source/exception_handlers.c \
 	$(SDK_ROOT)/components/modules/platform/source/app_boot_vectors.c \
	$(SDK_ROOT)/components/modules/patch/patch.c \
	$(SDK_ROOT)/components/modules/adpcm_ms/adpcm_ms.c \
	$(SDK_ROOT)/components/modules/adpcm_ima/adpcm_ima.c \
	$(SDK_ROOT)/components/modules/RingBuffer/ringbuffer.c \
 	$(SDK_ROOT)/components/modules/audio_decode/audio_decoder.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/alloc.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/bitalloc.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/bitalloc-sbc.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/bitstream-decode.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/decoder-oina.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/decoder-private.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/decoder-sbc.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/dequant.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/framing.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/framing-sbc.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/oi_codec_version.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/synthesis-8-generated.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/synthesis-dct8.c \
	$(SDK_ROOT)/components/modules/audio_decode/decoder/src/synthesis-sbc.c \
 	$(SDK_ROOT)/components/ble/profiles/ble_ota/ota_service.c \
 	$(SDK_ROOT)/components/ble/profiles/ble_ota/ota.c 
 	# $(SDK_ROOT)/components/ble/profiles/ble_hid/hid_service.c 
 	# $(SDK_ROOT)/components/ble/profiles/ble_audio_profile/speaker_service.c
 	# $(SDK_ROOT)/components/ble/profiles/ble_hid/hid_joystick.c 
# Include folders common to all targets
INC_FOLDERS += \
  $(SDK_ROOT)/components/ble/include \
  $(SDK_ROOT)/components/driver/include \
  $(SDK_ROOT)/components/modules/os/include \
  $(SDK_ROOT)/components/modules/sys/include \
  $(SDK_ROOT)/components/modules/platform/include \
  $(SDK_ROOT)/components/modules/common/include \
  $(SDK_ROOT)/components/modules/lowpow/include \
  $(SDK_ROOT)/components/modules/decoder \
  $(SDK_ROOT)/components/modules/button \
  $(SDK_ROOT)/components/modules/peripherals/audio \
  $(SDK_ROOT)/components/modules/adpcm_ms \
  $(SDK_ROOT)/components/modules/adpcm_ima \
  $(SDK_ROOT)/components/modules/RingBuffer \
  $(SDK_ROOT)/components/modules/audio_decode \
  $(SDK_ROOT)/components/modules/audio_decode/decoder/inc \
  $(SDK_ROOT)/components/ble/include/gap \
  $(SDK_ROOT)/components/ble/include/gatt \
  $(SDK_ROOT)/components/ble/profiles/ble_ota \
  $(SDK_ROOT)/components/ble/profiles/ble_spp \
  $(SDK_ROOT)/components/ble/profiles/ble_simple_profile \
  $(SDK_ROOT)/components/ble/profiles/ble_dev_info \
  $(SDK_ROOT)/components/ble/profiles/ble_batt \
  $(SDK_ROOT)/components/ble/profiles/ble_hid \
  $(SDK_ROOT)/components/ble/profiles/ble_prf_client_get_handle_from_discovery \
  $(SDK_ROOT)/components/ble/profiles/ble_hid_client \
  $(SDK_ROOT)/components/ble/profiles/ble_woodmaster \
  $(SDK_ROOT)/components/ble/profiles/ble_audio_profile \
  $(PROJECT_DIR) \
  $(PROJECT_DIR)/project


hardware_src = \
	$(hardware_dir)/motors/motors.c \
	$(hardware_dir)/home_button/keyScan.c \
	$(hardware_dir)/home_button/capacityKey.c \
	$(hardware_dir)/rgb/ws2812.c \
	$(hardware_dir)/rgb/rgb_mode.c \
	$(hardware_dir)/UART/uart.c \
	$(hardware_dir)/spi/hard_spi.c \
	$(hardware_dir)/W25QXX/w25qxx.c \
	$(hardware_dir)/W25QXX/getSbc.c 


# ./ble_drivers_demo/code/demo_peripheral.c 
dsp_lib_inc = \
	$(PROJECT_DIR)/dsp/include
dsp_lib_src= \
	$(PROJECT_DIR)/dsp/Source/ControllerFunctions/arm_pid_init_f32.c
hardware_inc = \
	$(hardware_dir)/motors \
	$(hardware_dir)/home_button \
	$(hardware_dir)/rgb \
	$(hardware_dir)/power \
	$(hardware_dir)/spi \
	$(hardware_dir)/UART \
	$(hardware_dir)/W25QXX 
filter_src = \
	$(PROJECT_DIR)/filter/filter.c \
	$(PROJECT_DIR)/filter/maths.c 
filter_inc = $(PROJECT_DIR)/filter
zino_src = \
	$(PROJECT_DIR)/ZINO/zino.c \
	$(PROJECT_DIR)/ZINO/dev_info.c \
	$(PROJECT_DIR)/ZINO/shell/zino_shell.c \
	$(PROJECT_DIR)/config/zino_config.c \
	$(PROJECT_DIR)/sw_digital/sw_digitalOsc.c 
	

zino_inc = \
	$(PROJECT_DIR)/ZINO \
	$(PROJECT_DIR)/ZINO/shell \
	$(PROJECT_DIR)/config \
	$(PROJECT_DIR)/sw_digital 


INC_FOLDERS += $(hardware_inc) $(filter_inc) $(zino_inc) 
SRC_FILES += $(hardware_src) $(filter_src) $(zino_src)

# Libraries common to all targets
LIB_FILES += -lfr8010h_stack

# Optimization flags
OPT = -Os -g

# C flags common to all targets
CFLAGS += $(OPT)
CFLAGS += -mcpu=cortex-m3
CFLAGS += -mthumb
# keep every function in a separate section, this allows linker to discard unused ones
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -fmessage-length=0 -fsigned-char
CFLAGS += -std=gnu11 -ffast-math -lm 
CFLAGS += $(DEV_INFO_FLAGS)

# Assembler flags common to all targets
ASMFLAGS += -g3
ASMFLAGS += -mcpu=cortex-m3
ASMFLAGS += -mthumb

# Linker flags
LDFLAGS += $(OPT)
LDFLAGS += -mthumb -specs=rdimon.specs 
LDFLAGS += -mcpu=cortex-m3
LDFLAGS +=  -T$(LINKER_SCRIPT) -L$(SDK_ROOT)/components/ble/library
LDFLAGS += $(SDK_ROOT)/components/ble/library/syscall_gcc.txt
# let linker dump unused sections
LDFLAGS += -Wl,--gc-sections -flto 
LDFLAGS += -Wl,--print-memory-usage -lm -lc -lg 

TEMPLATE_PATH := $(SDK_ROOT)/components/toolchain/gcc

include $(TEMPLATE_PATH)/Makefile.common

$(foreach target, $(TARGETS), $(call define_target, $(target)))
