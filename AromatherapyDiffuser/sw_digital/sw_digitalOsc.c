/*
 * @Author: LVGRAPE
 * @Date: 2023-12-26 10:20:46
 * @LastEditTime: 2023-12-26 10:44:37
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\sw_digital\sw_digitalOsc.c
 * 要啥没啥，爱咋咋的
 */
#include "sw_digitalOsc.h"
#include "driver_uart.h"
union ShangWaiDigitalOscilloscope
{
    uint8_t c[4];
    float f;
    int8_t __i8;
    uint8_t __u8;
    int16_t __i16;
    int32_t __i32;
    uint16_t __u16;
    uint32_t __u32;
}sw_data;
void sw_digitalOsc_head()
{
    uart_putc_noint_no_wait(UART1, 0x03);
    uart_putc_noint_no_wait(UART1, 0xfc);
}
void sw_digitalOsc_end()
{
    uart_putc_noint_no_wait(UART1, 0xfc);
    uart_putc_noint_no_wait(UART1, 0x03);
}
void sw_digitalOsc_i8(int8_t i)
{
    sw_data.__i8 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
}
void sw_digitalOsc_u8(uint8_t i)
{
    sw_data.__u8 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
}
void sw_digitalOsc_i16(int16_t i)
{
    sw_data.__i16 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
    uart_putc_noint_no_wait(UART1, sw_data.c[1]);
}
void sw_digitalOsc_u16(uint16_t i)
{
    sw_data.__u16 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
    uart_putc_noint_no_wait(UART1, sw_data.c[1]);
}
void sw_digitalOsc_i32(int32_t i)
{
    sw_data.__i32 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
    uart_putc_noint_no_wait(UART1, sw_data.c[1]);
    uart_putc_noint_no_wait(UART1, sw_data.c[2]);
    uart_putc_noint_no_wait(UART1, sw_data.c[3]);
}
void sw_digitalOsc_u32(uint32_t i)
{
    sw_data.__u32 = i;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
    uart_putc_noint_no_wait(UART1, sw_data.c[1]);
    uart_putc_noint_no_wait(UART1, sw_data.c[2]);
    uart_putc_noint_no_wait(UART1, sw_data.c[3]);
}
void sw_digitalOsc_float(float f)
{
    sw_data.f = f;
    uart_putc_noint_no_wait(UART1, sw_data.c[0]);
    uart_putc_noint_no_wait(UART1, sw_data.c[1]);
    uart_putc_noint_no_wait(UART1, sw_data.c[2]);
    uart_putc_noint_no_wait(UART1, sw_data.c[3]);
}

