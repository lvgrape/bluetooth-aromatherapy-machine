/*** 
 * @Author: LVGRAPE
 * @Date: 2023-12-26 10:36:21
 * @LastEditTime: 2023-12-26 10:40:48
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\sw_digital\sw_digitalOsc.h
 * @要啥没啥，爱咋咋的
 */

#ifndef __SW_DIGITALOSC_H_
#define __SW_DIGITALOSC_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>

void sw_digitalOsc_end();
void sw_digitalOsc_head();
void sw_digitalOsc_i8(int8_t i);
void sw_digitalOsc_u8(uint8_t i);
void sw_digitalOsc_i16(int16_t i);
void sw_digitalOsc_u16(uint16_t i);
void sw_digitalOsc_i32(int32_t i);
void sw_digitalOsc_u32(uint32_t i);
void sw_digitalOsc_float(float f);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
