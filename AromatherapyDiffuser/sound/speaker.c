#include "driver_plf.h"
#include "driver_i2s.h"
#include "driver_codec.h"
#include "driver_pmu.h"
void speaker_init()
{
    pmu_codec_power_enable();
    i2s_start();
    codec_enable_dac();

    speaker_codec_init(CODEC_SAMPLE_RATE_16000);
    i2s_init(I2S_DIR_TX,16000,1);    //8,16,32,48
    NVIC_SetPriority(I2S_IRQn, 2);
}