#include <math.h>
#include <stdint.h>
#include <stdio.h>	/* printf */ 	/* printf */ 	/* printf */ 	/* printf */ 	/* printf */

// #define PI_UINT32_X2000 6.282
// struct sound_math {
//     float freq;
//     float gain;

// };

// float sound_cal(struct sound_math* s, uint32_t time_us) {
//     float x = time_us * 1E-5f;
//     float fade = 1 / (x + 0.25f);
//     float result = s->gain * fade * sin(s->freq * M_PI * x);
//     return result;
// }
/**
 * @brief 通过频率和时间，计算出声音的幅值。
 * 
 * @param f 频率
 * @param time_us 时间（us）
 * @return float 结果±1.5f
 */
float sound_wave_cal(uint16_t f, uint32_t time_us)
{
    float x = time_us * 1E-6f;
    uint8_t sN = 25;//谐波次数
    float res = 0;
    float w, fade;
    for (uint8_t n = 0;n < sN;n++)
    {
        w = (float)(f * (sN - n)) * M_PI;
        fade = expf(-1.5f - sqrtf(f * (sN - n)) / sN * x) / 3.0f;
        res += sinf(w * x) * fade;
    }
    return res;
}

// int main()
// {
//     struct sound_math s1 = {
//         .freq = 520,
//         .gain = 1
//     };
//     uint32_t f = 1120;
//     uint32_t T = 1000000 / f;
//     for (uint32_t i = 0;i <= T;i += (T/100))
//     {

//         printf("F:%d T:%d \t R:%.06f\n", f, i, sound_cal_2(f, i));
//         // if(i%15==0)
//         // {
//         //     printf("\n");
//         // }
//     }
//     return 0;
// }
void main()
{
    for(uint8_t i=0;i<0x26;i++)
    {
        printf("static codec_reg%02x_t reg%02x={0};\n",i,i);
    }
}