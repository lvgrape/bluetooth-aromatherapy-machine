/*** 
 * @Author: LVGRAPE
 * @Date: 2023-06-09 10:21:24
 * @LastEditTime: 2023-06-09 10:21:29
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\hardware\sound\sound_math.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __SOUND_MATH_H_
#define __SOUND_MATH_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>
float sound_wave_cal(uint16_t f, uint32_t time_us);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
