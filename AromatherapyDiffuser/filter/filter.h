/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-18 11:21:11
 * @LastEditTime: 2023-05-18 11:22:49
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\filter\filter.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __FILTER_H_
#define __FILTER_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>
typedef struct 
{
    float Last_P;//上次估算协方差 不可以为0 ! ! ! ! ! 
    float Now_P;//当前估算协方差
    float out;//卡尔曼滤波器输出
    float Kg;//卡尔曼增益
    float Q;//过程噪声协方差
    float R;//观测噪声协方差
}Kalman_t;

float KalmanFilter(Kalman_t *kfp,float input);
void KalmanFilterInit(Kalman_t *kfp);
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
