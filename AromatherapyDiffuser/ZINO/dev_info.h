/***
 * @Author: LVGRAPE
 * @Date: 2023-04-13 16:52:40
 * @LastEditTime: 2023-04-14 09:27:49
 * @LastEditors: LVGRAPE
 * @Description:
 * NOTE 设备描述信息, 主要用来区分是用什么芯片（核心），什么形态，有哪些装备，及其它功能
 * 1. 核心： ZINO_I,ZINO_PREMIUM,ZINO_RC等， 不同的核心指的是不同的芯片
 * 2. 形态： 机甲之芯 天空之翼 遥控器出厂程序 引导程序 测试程序 其它 青春版 遥控器指挥
 * 3. 装备： 无 8mm水弹炮 足球套装 机械臂 选择装备 ---必选！！！
 * @FilePath: \ZINO_MCU_SOURCE\ZINO_SRC\dev_info\dev_info.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __dev_info_H_
#define __dev_info_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>

#define DEV_CORE_NAME "ZINO_BLE"
#define DEV_CORE_ID 0x000B

#define DEV_NAME "Aromatherapy" 
#define DEV_ID 0

#define DEV_EQP_NAME "RGB VOL SW"
#define DEV_EQP_ID 1

    // #define DEV_MVID 1
    // #define DEV_SVID 0
    // #define DEV_BVID 0
    typedef union {
        struct {
            uint32_t model;
            uint32_t transform;
            uint32_t equipment;
            uint32_t other;
            uint32_t version;
            uint32_t crc32;
        };
        uint8_t num[24];
    }serialnum_t;

#define ZINO_INFO_NAME_MAX 24
    typedef enum {
        Heart_Of_Mecha, Wing_Of_Sky, Bootloader,
    }DEVICE_GROUP_EN;
    typedef enum {
        NONE_EQP, WaterBulletGun, FootballSuit, RobotArm, CaterpillarBand, AllInOne,
    }
    Equipment_GROUP_EN;
    typedef struct info {
        uint16_t id;
        char name[16];
    }__attribute__((packed)) info_t;
    typedef struct {
        uint16_t CORE;
        uint16_t DEVICE;
        uint16_t EQUIPMENT;
        uint16_t OTHRE;
    }build_info_t;
    typedef struct {
        uint8_t magicbee[20];
        uint32_t vid;
        info_t core;
        info_t device;
        info_t equipment;
        info_t other; //additional info like manufacturer, serial number, etc.
        uint8_t* UID;//96bits of uuid
        void* feature;
        uint8_t* featureLen;
        serialnum_t serial_number;
        uint8_t magicbee_end[20];
    }__attribute__((packed)) dev_info_t;
    const uint8_t* get_serialNubmer();
    uint32_t get_dev_software_version();
    uint8_t get_dev_major_verion();
    uint8_t get_dev_minor_verion();
    uint8_t get_dev_build_number();
    const char* get_dev_core_name();
    const uint16_t get_dev_core_id();
    const char* get_dev_name();
    const uint16_t get_dev_id();
    const uint16_t get_eqm_id();
    const char* get_eqm_name();
    int check_dev_info();
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
