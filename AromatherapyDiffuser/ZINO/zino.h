/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-25 18:13:45
 * @LastEditTime: 2024-01-05 20:12:17
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_hypro_drone_v01\hypro\ZINO\zino.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __ZINO_H_
#define __ZINO_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <driver_system.h>
#include <stdint.h>
#include <stdbool.h>
#include "co_printf.h"
#include "zino_shell.h"

#define millis() system_get_curr_time()
#define RT_USED                     __attribute__((used))
#define RT_SECTION(x)               __attribute__((section(x)))
typedef int (*init_fn_t)(void);

#define INIT_EXPORT(fn, level) const init_fn_t __zino_init_##fn __attribute__ ((section(".zino_init_fn."level),used, aligned(4))) = fn
// #define INIT_EXPORT(fn, level) ZINO_USED const init_fn_t __zino_init_##fn SECTION(".zino_init_fn."level) = fn

/**进入BLE前要运行的电路板初始化程序,会自动执行1次*/
#define ZINO_INIT_BOARD_EXPORT(fn)         INIT_EXPORT(fn, "1")
/**进入BLE后要运行的前置初始化程序,会自动执行1次*/
#define ZINO_INIT_PRE_EXPORT(fn)           INIT_EXPORT(fn, "2")
/**进入BLE后要运行的设备初始化程序,会自动执行1次*/
#define ZINO_INIT_DEVICE_EXPORT(fn)        INIT_EXPORT(fn, "3")
/**进入BLE后要运行的组件初始化程序,会自动执行1次*/
#define ZINO_INIT_COMPONENT_EXPORT(fn)     INIT_EXPORT(fn, "4")
/**进入BLE后要运行的环境初始化程序,会自动执行1次*/
#define ZINO_INIT_ENV_EXPORT(fn)           INIT_EXPORT(fn, "5")
/**进入BLE后要运行的APP初始化程序,会自动执行1次*/
#define ZINO_INIT_APP_EXPORT(fn)           INIT_EXPORT(fn, "6")

void zino_board_init();
void zino_components_init();
void system_reboot();
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
