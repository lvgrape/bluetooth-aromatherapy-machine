/*
 * @Author: LVGRAPE
 * @Date: 2023-05-25 18:22:23
 * @LastEditTime: 2024-01-10 17:09:20
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\ZINO\zino.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include "zino.h"
#include "co_printf.h"
#include "os_timer.h"
#include "driver_plf.h"
static int zi_start(void)
{
    return 0;
}
INIT_EXPORT(zi_start, "0");

static int zi_board_start(void)
{
    return 0;
}
INIT_EXPORT(zi_board_start, "0.end");

static int zi_board_end(void)
{
    return 0;
}
INIT_EXPORT(zi_board_end, "1.end");

static int zi_end(void)
{
    return 0;
}
INIT_EXPORT(zi_end, "6.end");

void zino_board_init()
{
    const init_fn_t *fn_ptr;
    co_printf("\r\n\r\n zino_board_init: %08X-%08X \r\n",&__zino_init_zi_board_start,&__zino_init_zi_board_end);
    for (fn_ptr = &__zino_init_zi_board_start; fn_ptr < &__zino_init_zi_board_end; fn_ptr++)
    {
        (*fn_ptr)();
    }
}
void zino_components_init()
{
    const init_fn_t *fn_ptr;
    co_printf("\r\n\r\n zino_components_init: %08X-%08X \r\n",&__zino_init_zi_board_end,&__zino_init_zi_end);
    for (fn_ptr = &__zino_init_zi_board_end; fn_ptr < &__zino_init_zi_end; fn_ptr++)
    {
        (*fn_ptr)();
    }
}

#include "driver_wdt.h"
void wdt_feeddog(void *p)
{
    // co_printf("wdt_feed\r\n");
    wdt_feed();

}
int watchDogInit()
{
    static os_timer_t wdt_timer; 	/* timer for wdt_task */ 	/* timer for wdt_task */ 	
    wdt_init(WDT_ACT_RST_CHIP, 5);
    wdt_start();
    os_timer_init(&wdt_timer, wdt_feeddog, NULL);
    os_timer_start(&wdt_timer, 4500, 1);
    return 0;
}

ZINO_INIT_APP_EXPORT(watchDogInit);

__attribute__((section("ram_code"))) void wdt_isr_ram(unsigned int* hardfault_args)
{
    co_printf("wdt_rest\r\n\r\n");
    co_printf("PC    = 0x%08X\r\n",hardfault_args[6]);
    co_printf("LR    = 0x%08X\r\n",hardfault_args[5]);
    co_printf("R0    = 0x%08X\r\n",hardfault_args[0]);
    co_printf("R1    = 0x%08X\r\n",hardfault_args[1]);
    co_printf("R2    = 0x%08X\r\n",hardfault_args[2]);
    co_printf("R3    = 0x%08X\r\n",hardfault_args[3]);
    co_printf("R12   = 0x%08X\r\n",hardfault_args[4]);

    /* reset the system */
    ool_write(PMU_REG_RST_CTRL, ool_read(PMU_REG_RST_CTRL) & (~ PMU_RST_WDT_EN) );
}

void system_reboot()
{
    wdt_feed();
    wdt_stop();  /* stop the wdt task */
    platform_reset_patch(0);
}

os_timer_t zino_log_timer;

void zino_log_fun(void *p)
{
    //FIXME 定时10ms dt->10ms
    //FIXME 定时20ms dt->10ms
    //FIXME 定时50ms dt->40ms
    //FIXME 定时55ms dt->40ms
    //FIXME 定时1000ms dt->990ms
    //FIXME 定时1010ms dt->1000ms
    static uint32_t preDt;
    uint32_t dt=millis()-preDt; /* millis() is a system timer */
    preDt = millis(); /* reset the timer */
    co_printf("T:%d \t delay:%d\r\n",preDt,dt);
}
int zino_log_init()
{
    os_timer_init(&zino_log_timer, zino_log_fun, 0);
    os_timer_start(&zino_log_timer,1010,1);
    return 0;
}
// ZINO_INIT_APP_EXPORT(zino_log_init);