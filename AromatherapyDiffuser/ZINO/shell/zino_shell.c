/*
 * @Author: LVGRAPE
 * @LastEditors: LVGRAPE
 */

#include "zino.h"
#include "zino_shell.h"
#include "co_printf.h"
#include "uart.h"

#include <string.h>
#include <stdlib.h>
#include <driver_uart.h>

#define DBG_ENABLE
#define DBG_TAG "shell"
#define DBG_LVL DBG_LOG
#include <rtdbg.h>


const char __zsc_help_name[] = "help";
const char __zsc_help_desc[] = "Zino shell help";
volatile const struct zino_shell_syscall* zs_ptr;
long zino_shell_help(int argc, char** argv);
const struct zino_shell_syscall zs_start RT_SECTION(".zino_init_fn.7") =
{
    __zsc_help_name,__zsc_help_desc,zino_shell_help
};
const struct zino_shell_syscall zs_end RT_SECTION(".zino_init_fn.8.end");
long zino_shell_help(int argc, char** argv)
{
    co_printf("Zino shell help:\n");
    for (zs_ptr = &zs_start; zs_ptr < &zs_end; zs_ptr++)
    {
        co_printf("%s \t", zs_ptr->name);
        co_printf("-%s\n", zs_ptr->desc);
    }
}

static uint8_t get_argc(const char* str)
{
    uint8_t argc = 1;
    for (uint8_t i = 0; i < strlen(str); i++)
    {
        if (str[i] == ' ')
        {
            argc++;
        }
    }
    return argc;
}
#define SHELL_MAX_ARGC 8
#define SHELL_MAX_ARGV_LEN 16
static uint8_t get_argv_len(const char* str)
{
    uint8_t len = 0;
    for (uint8_t i = 0; i < strlen(str); i++)
    {
        len++;
        if (str[i] == ' ')
        {
            break;
        }
    }
    return (len > SHELL_MAX_ARGV_LEN ? SHELL_MAX_ARGV_LEN : len);
}

void shell_task(void* pvParameters)
{
    // LOG_I("shell_task");
    struct uart *pack = (struct uart *)pvParameters;
    pack->rxbufferCnt = uart_get_data_nodelay_noint(UART1, pack->rxbuffer, 32);
    // co_printf("r:%d ",pack->rxbufferCnt);
    // for(int i=0;i<pack->rxbufferCnt;i++)
    // {
    //     co_printf("%x ",pack->rxbuffer[i]);
    // }
    // co_printf("\n");
    // if (IsSerialRxHasData(&serialHandle[serial1]))
    if (pack->rxbufferCnt)
    {
        // char* rx_ptr = Serial_gets(&serialHandle[serial1]);
        char* rx_ptr = pack->rxbuffer;
        uint8_t cmd_exist = 0;
        for (zs_ptr = &zs_start; zs_ptr < &zs_end; zs_ptr++)
        {
            if (strncmp(rx_ptr, zs_ptr->name, strlen(zs_ptr->name)) == 0)
            {
                uint8_t argc = get_argc(rx_ptr);
                if (argc > 8)
                {
                    LOG_E("max argc < %d!", SHELL_MAX_ARGC);
                    break;
                }
                char arg[SHELL_MAX_ARGC][SHELL_MAX_ARGV_LEN] = { 0 };
                char* argv[SHELL_MAX_ARGC] = { 0 };
                // LOG_I("\n> %s,%d\n", rx_ptr, argc);
                for (uint8_t i = 0; i < argc; i++)
                {
                    strncpy(arg[i], rx_ptr, get_argv_len(rx_ptr));
                    argv[i] = arg[i];
                    rx_ptr = strchr(rx_ptr, ' ');
                    // LOG_I("argv[%d]:%s,%x\n", i, argv[i]);
                    if (rx_ptr == NULL)
                    {
                        break;
                    }
                    rx_ptr++;
                }
                // LOG_I("fun\n");
                zs_ptr->fun(argc, argv);
                cmd_exist++;
                break;
            }
        }
        if (!cmd_exist)
        {
            co_printf("> Unknow cmd: %s\n", rx_ptr);
        }
        co_printf("\n>");
        memset(pack->rxbuffer, 0, pack->rxbufferCnt);
        // Serial_Purge(&serialHandle[serial1]);
    }
    // return 0;
}
// int shell_init(void)
// {
//     os_timer_init(&timer, shell_task, 0);
//     os_timer_start(&timer, 1, 1);
//     LOG_I("shell_init");
//     return 0;
// }


int check_shell_list(void)
{
    volatile const struct zino_shell_syscall* zs_ptr;
    // LOG_I("\r\n\r\n check_shell_list: %08X-%08X \r\n",&__zino_init_zi_board_end,&__zino_init_zi_end);
    for (zs_ptr = &zs_start; zs_ptr < &zs_end; zs_ptr++)
    {
        LOG_I("zs_ptr:0x%08X \t", zs_ptr);
        LOG_I("name:%s \t", zs_ptr->name);
        LOG_I("desc:%s \t", zs_ptr->desc);
        LOG_I("fun:%08X \n", zs_ptr->fun);
    }
    return 0;
}
// ZINO_APP_EXPORT(shell_init);
// ZINO_APP_EXPORT(check_shell_list);

void reboot()
{
    system_reboot();
    // __set_PRIMASK(0);
    // NVIC_SystemReset();
}
// long cmd1(void) {}
// long cmd2(void) {}
// long cmd3(void) {}

ZINO_CMD_EXPORT(reboot, system reboot);
// ZINO_CMD_EXPORT(cmd2, cmd2 test);
// ZINO_CMD_EXPORT(cmd3, cmd3 test);
