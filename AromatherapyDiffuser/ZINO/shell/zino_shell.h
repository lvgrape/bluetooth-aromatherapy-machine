/*
 * @Author: LVGRAPE
 * @LastEditors: LVGRAPE
 */
#ifndef __ZINO_SHELL_H__
#define __ZINO_SHELL_H__
#include "zino.h"
typedef long (*zino_syscall_func)(int argc, char** argv);
struct zino_shell_syscall
{
    const char* name;
    const char* desc;
    zino_syscall_func fun;
};
struct uart_pack {
    uint16_t len;
    uint8_t* buffer;
};
#define ZINO_CMD_EXPORT(command, desc) ZINO_SHELL_EXPORT(command, command, desc)
#define ZINO_SHELL_EXPORT(name, cmd, desc)                                              \
    const char __zsc_##cmd##_name[] = #cmd;               \
    const char __zsc_##cmd##_desc[] = #desc;              \
    __attribute__((used)) const struct zino_shell_syscall __zsc_##cmd RT_SECTION(".zino_init_fn.8") = \
        {__zsc_##cmd##_name, __zsc_##cmd##_desc, (zino_syscall_func) & name};


void shell_task(void* pvParameters);
int check_shell_list(void);
#endif

