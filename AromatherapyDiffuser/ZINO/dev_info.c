/*
 * @Author: LVGRAPE
 * @Date: 2023-05-26 09:39:06
 * @LastEditTime: 2023-10-07 16:40:07
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE_V1\ZINO_BLE_V1\ZINO\dev_info.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
/*
 * @Author: LVGRAPE
 * @Date: 2023-04-13 18:12:33
 * @LastEditTime: 2023-04-24 10:36:50
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_MCU_SOURCE\ZINO_SRC\dev_info\dev_info.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include "dev_info.h"
#include "co_printf.h"
#include "zino.h"
#include "ble_dev.h"
#define MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0  ((uint32_t)0x1FFFF7E8)
#define MCU_UNIQUE_96BIT_ID_BASE_ADDR_63_32 ((uint32_t)0x1FFFF7EC)
#define MCU_UNIQUE_96BIT_ID_BASE_ADDR_95_64 ((uint32_t)0x1FFFF7F0)
#define MCU_UNIQUE_ID_0  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 0)
#define MCU_UNIQUE_ID_1  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 1)
#define MCU_UNIQUE_ID_2  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 2)
#define MCU_UNIQUE_ID_3  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 3)
#define MCU_UNIQUE_ID_4  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 4)
#define MCU_UNIQUE_ID_5  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 5)
#define MCU_UNIQUE_ID_6  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 6)
#define MCU_UNIQUE_ID_7  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 7)
#define MCU_UNIQUE_ID_8  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 8)
#define MCU_UNIQUE_ID_9  *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 9)
#define MCU_UNIQUE_ID_10 *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 10)
#define MCU_UNIQUE_ID_11 *(const vu8 *)(MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0 + 11)

const dev_info_t zino_dev_info = {
    .magicbee = "<<ZINO_DEV_START>>", /* Magicbee is set to 0x12345678 for zino. */
#if defined (DEV_CORE_ID) && defined (DEV_CORE_NAME)
    .core = {
        .id = DEV_CORE_ID,
        .name = DEV_CORE_NAME,
    },
    #else
    #warning "Devices must have a name - DEV_CORE_NAME"
#endif

#if defined (DEV_NAME) && defined (DEV_ID)
    .device = {
        .id = DEV_ID,
        .name = DEV_NAME,
    },
    #else
    #warning "Devices must have a name - DEV_NAME"
#endif
    .other = {
        .id = 99,
        .name = "Other", // Default name for other device.  Used for device enumeration.  Also used for device name when no name is specified.
    },
#if defined (DEV_EQP_NAME) && defined (DEV_EQP_ID)
    .equipment = {
        .id = DEV_EQP_ID,
        .name = DEV_EQP_NAME,
    },
    #else
    #warning "Devices must have a name - DEV_EQP_NAME"
#endif

#if defined(DEV_MVID) && defined(DEV_SVID) && defined(DEV_BVID)
    .vid = (((uint32_t)(DEV_MVID)) << 24) | (((uint32_t)(DEV_SVID)) << 16) | (((uint32_t)(DEV_BVID)) << 8),
    #else 
    #warning "Devices must be defined - DEV_MVID, DEV_SVID, and DEV_BVID"
#endif // DEBUG
    .UID = (uint8_t*)MCU_UNIQUE_96BIT_ID_BASE_ADDR_31_0,
    .serial_number={
        .model = DEV_CORE_ID,
        .transform = DEV_ID,
        .equipment = DEV_EQP_ID,
        .other = 0,
        .version = (((uint32_t)(DEV_MVID)) << 24) | (((uint32_t)(DEV_SVID)) << 16) | (((uint32_t)(DEV_BVID)) << 8),
        .crc32 = DEV_CORE_ID^DEV_ID^DEV_EQP_ID^0^((((uint32_t)(DEV_MVID)) << 24) | (((uint32_t)(DEV_SVID)) << 16) | (((uint32_t)(DEV_BVID)) << 8)),
    },
    .magicbee_end="<<ZINO_DEV_END>>",
};
uint32_t get_dev_software_version()
{
    return zino_dev_info.vid;
}
uint8_t get_dev_major_verion()
{
    return (zino_dev_info.vid >> 24) & 0x00FF; 	// high byte of the UID is the major version number.  low byte is the revision number.
}
uint8_t get_dev_minor_verion() 	// this is the same as the first byte of the UID.  It is
{
    return (zino_dev_info.vid >> 16) & 0x00FF;
}
uint8_t get_dev_build_number() 	// this is the same as the second byte of the UID.  It is
{
    return (zino_dev_info.vid >> 8) & 0x00FF;
}
const char* get_dev_core_name()
{
    return zino_dev_info.core.name; 	    // Return the name.  It is not a static string.  It is a pointer to a string.
}
const uint16_t get_dev_core_id() 	    // Return the unique ID of the core.  It is not a static number.  It is a 16
{
    return zino_dev_info.core.id; // number.  It is a number.  It is a number.  It is a
}
const char* get_dev_name()
{
    return zino_dev_info.device.name; // Return the name.  It is not a static string.  It is a pointer to a string.
}
const uint16_t get_dev_id() 	    // Return the unique ID of the device.  It is not a static number.  It is a
{
    return zino_dev_info.device.id;
}
const uint16_t get_eqm_id()
{
    return zino_dev_info.equipment.id;
}
const char* get_eqm_name() 	    // Return the name of the device.  It is not a static string.  It is a pointer
{
    return zino_dev_info.equipment.name; // to a string.  It is a pointer to a string.  It is a pointer to a string
}
const uint16_t get_dev_other_id()
{
    return zino_dev_info.other.id; // Return the unique ID of the device.  It is not a static number.  It is a 16 number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It is a number.  It
}
const char *get_dev_other_name()
{
    return zino_dev_info.other.name; // Return the name of the device.  It is not a static string.  It is a pointer to a
}
const uint8_t *get_serialNubmer()
{
    return zino_dev_info.serial_number.num;
}
int check_dev_info()
{
    co_printf("\n\n* Dev info: %s %d\n",zino_dev_info.magicbee,sizeof(dev_info_t));
    co_printf("* Core:%d,%s\n", get_dev_core_id(), get_dev_core_name());
    co_printf("* Device:%d,%s\n", get_dev_id(), get_dev_name()); 	    // Print the name.  It is
    co_printf("* Eqm:%d,%s\n", get_eqm_id(), get_eqm_name()); // Print the name.  It is.  It is
    co_printf("* other:%d,%s\n", get_dev_other_id(), get_dev_other_name()); // Print the name.  It is.  It is
    co_printf("* VID: %d.%d.%d\n", get_dev_major_verion(), get_dev_minor_verion(), get_dev_build_number());
    co_printf("* Build date:%s %s\n\n", __DATE__,zino_dev_info.magicbee_end);
    co_printf("Serial numer: ");
    printf_hex((uint8_t *)get_serialNubmer(), 24, 1);
    return 0;
}

ZINO_INIT_APP_EXPORT(check_dev_info);