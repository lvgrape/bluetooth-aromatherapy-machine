#include <stdio.h>
#include <stdint.h>
#include "driver_flash.h"
#include "decoder.h"
#include "speaker.h"
#include "os_mem.h"
#include "co_printf.h"
#include "sys_utils.h"
#include "driver_gpio.h"
#define MUTE_PIN GPIO_PORT_C, GPIO_BIT_7 //PC7
/**
 * @brief 静音控制
 *
 * @param enable 0：静音，1：非静音
 */
void mute_ctrl(uint8_t enable)
{
    system_set_port_mux(MUTE_PIN, PORTA0_FUNC_A0);
    gpio_set_dir(MUTE_PIN, GPIO_DIR_OUT);
    gpio_set_pin_value(MUTE_PIN, enable);
}



extern int read_sbc_from_flash(uint8_t * sbc_buff, uint32_t read_len);
extern void decoder_start(uint32_t start, uint32_t end, uint32_t tot_data_len, uint16_t frame_len,
                                uint32_t start_offset, uint8_t type);
extern uint16_t decoder_calc_adpcm_ms_frame_len(uint8_t **header_org);

