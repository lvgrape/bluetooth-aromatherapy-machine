//***********************
#ifndef _AUDIO_EXTERN_FLASH_H_
#define _AUDIO_EXTERN_FLASH_H_

void tone_play_state_set(uint8_t state);

uint8_t tone_play_state_get(void);

void tone_delay_play_set(uint8_t idx);

uint8_t tone_delay_play_get(void);

void tone_speaker_from_flash(void);


#endif

