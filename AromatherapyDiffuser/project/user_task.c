#include <stdint.h>

#include "os_task.h"
#include "os_msg_q.h"

#include "co_printf.h"
#include "user_task.h"
#include "button.h"
#include "adpcm_ms.h"
#include "speaker.h"
#include "decoder.h"
#include "os_mem.h"
#include "driver_i2s.h"
#include "driver_plf.h"

// #include "proj_speaker.h"
#include "audio_extern_flash.h"


uint16_t user_task_id;
uint16_t audio_task_id;
uint16_t delay_tone_task_id;

extern void tone_key_change_ctrl(void);

static int user_task_func(os_event_t* param)
{
	// switch (param->event_id)
	// {
	// case USER_EVT_BUTTON:
	// {
	// 	struct button_msg_t* button_msg;
	// 	const char* button_type_str[] = {
	// 										"BUTTON_PRESSED",
	// 										"BUTTON_RELEASED",
	// 										"BUTTON_SHORT_PRESSED",
	// 										"BUTTON_MULTI_PRESSED",
	// 										"BUTTON_LONG_PRESSED",
	// 										"BUTTON_LONG_PRESSING",
	// 										"BUTTON_LONG_RELEASED",
	// 										"BUTTON_LONG_LONG_PRESSED",
	// 										"BUTTON_LONG_LONG_RELEASED",
	// 										"BUTTON_COMB_PRESSED",
	// 										"BUTTON_COMB_RELEASED",
	// 										"BUTTON_COMB_SHORT_PRESSED",
	// 										"BUTTON_COMB_LONG_PRESSED",
	// 										"BUTTON_COMB_LONG_PRESSING",
	// 										"BUTTON_COMB_LONG_RELEASED",
	// 										"BUTTON_COMB_LONG_LONG_PRESSED",
	// 										"BUTTON_COMB_LONG_LONG_RELEASED",
	// 	};

	// 	button_msg = (struct button_msg_t*)param->param;

	// 	co_printf("KEY 0x%08x, TYPE %s.\r\n", button_msg->button_index, button_type_str[button_msg->button_type]);
	// 	if (button_msg->button_type == BUTTON_SHORT_PRESSED)
	// 	{
	// 		//                    tone_key_change_ctrl();
	// 		// test_speaker();
	// 	}
	// }
	// break;
	// }

	return EVT_CONSUMED;
}

//int delay_play_tone_task(os_event_t *param)
//{   
//    co_printf("====test===\r\n");
//    if(tone_delay_play_get() < 0xff)
//        tone_speaker_from_flash();
//    tone_delay_play_set(0xff);

//    return EVT_CONSUMED;
//}

// void send_delay_play_tone_event(void)
// {
//    os_event_t tone_event;

//    tone_event.event_id = 0;
//    tone_event.param = NULL;
//    tone_event.param_len = 0;
//    os_msg_post(delay_tone_task_id, &tone_event);    
// }

/*********************************************************************
* @fn      audio_task_func
*
* @brief   Audio task function, handles audio events.
*
* @param   param   - OS events of audio.
*
*
* @return  int     - EVT_CONSUMED.
*/
static int audio_task_func(os_event_t* param)
{
	struct decoder_prepare_t* decoder_param = NULL;
	ADPCMContext* context = NULL;
	switch (param->event_id)
	{
		//音频解码准备
	case DECODER_EVENT_PREPARE:

		decoder_param = (struct decoder_prepare_t*)(param->param);
		decoder_env.decoder_context = os_zalloc(sizeof(ADPCMContext));
		context = (ADPCMContext*)decoder_env.decoder_context;
		context->channel = 1;
		context->block_align = decoder_param->frame_len;
		decoder_env.data_start = decoder_param->data_start;
		decoder_env.data_end = decoder_param->data_end;
		decoder_env.current_pos = decoder_param->data_start + decoder_param->start_offset;
		decoder_env.tot_data_len = decoder_param->tot_data_len;
		decoder_env.store_type = decoder_param->store_type;
		decoder_env.data_processed_len = 0;
		decoder_env.frame_len = decoder_param->frame_len;
		stop_flag = 0;

		co_printf("preparing,fram_len:%d\r\n", decoder_env.frame_len);
		co_list_init(&decoder_env.pcm_buffer_list);
		decoder_env.pcm_buffer_counter = 0;
		decoder_play_next_frame();
		decodeTASKState = DECODER_STATE_BUFFERING;

		break;
		//音频解码下一帧数据处理
	case DECODER_EVENT_NEXT_FRAME:
		decoder_play_next_frame_handler(&decodeTASKState);
		break;

		//音频解码停止
	case DECODER_EVENT_STOP:
		NVIC_DisableIRQ(I2S_IRQn);

		while (1)
		{
			struct co_list_hdr* element = co_list_pop_front(&decoder_env.pcm_buffer_list);
			if (element == NULL)
				break;
			os_free((void*)element);
		}

		if (decoder_env.decoder_context != NULL)
		{
			os_free((void*)decoder_env.decoder_context);
			decoder_env.decoder_context = NULL;
		}
		decodeTASKState = DECODER_STATE_IDLE;
		speaker_stop_hw();
		decoder_end_func();
		decoder_hold_flag = false;

		tone_play_state_set(0);
		// send_delay_play_tone_event();
		break;
	}
	return EVT_CONSUMED;
}

void user_task_init(void)
{
	// user_task_id = os_task_create(user_task_func);
	// audio_task_id = os_task_create(audio_task_func);//创建音频任务
	//    delay_tone_task_id = os_task_create(delay_play_tone_task);
}

