/*
 * @Author: LVGRAPE
 * @Date: 2024-02-19 18:14:15
 * @LastEditTime: 2024-02-20 09:18:32
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\project\audio_extern_flash.c
 * 要啥没啥，爱咋咋的
 */
//**********************

#include <stdio.h>
#include <stdint.h>
#include "driver_flash.h"
#include "decoder.h"
#include "speaker.h"
#include "os_mem.h"
#include "co_printf.h"
#include "sys_utils.h"

#include "audio_extern_flash.h"
// #include "tone.h"

uint8_t tone_playing = 0,tone_back_idx = 0xff;

extern int read_sbc_from_flash(uint8_t * sbc_buff, uint32_t read_len);
extern void decoder_start(uint32_t start, uint32_t end, uint32_t tot_data_len, uint16_t frame_len,
                                uint32_t start_offset, uint8_t type);
extern uint16_t decoder_calc_adpcm_ms_frame_len(uint8_t **header_org);

#define TONE0_LEN   0x284b
#define TONE1_LEN   0x2733
#define TONE2_LEN   0x27e7
#define TONE3_LEN   0x2ccb
#define TONE4_LEN   0x4914
#define TONE5_LEN   0x4d41
#define TONE6_LEN   0x5030 // 0x36b24

void tone_buff_show(void)
{
//    co_printf("1=%x=%x,%x\r\n",&auto_mode[0],auto_mode[0],sizeof(auto_mode));
//    show_reg((uint8_t *)&auto_mode[0],32,1);
//    
//    show_reg((uint8_t *)&zhenjiu_mode[0],32,1);
}

void tone_play_state_set(uint8_t state)
{
    tone_playing = state;
}

uint8_t tone_play_state_get(void)
{
    return tone_playing;
}

void tone_delay_play_set(uint8_t idx)
{
    tone_back_idx = idx;
}

uint8_t tone_delay_play_get(void)
{
    return tone_back_idx;
}

static uint8_t tone_idx = 0;
void tone_key_change_ctrl(void)
{
    tone_idx++;
    if(tone_idx > 3)
        tone_idx = 0;
    tone_delay_play_set(tone_idx);
    tone_speaker_from_flash();
}

void tone_speaker_from_flash(void)
{
    uint32_t len = 0;
    uint8_t playing_tone_idx = 0;
//    static uint8_t tone_idx = 0;

    if(tone_play_state_get())
    {
        co_printf("=playing=\r\n");
        tone_delay_play_set(tone_idx);
        test_end_speaker();
        return;
    }
    if( sbc_buff != NULL)
        goto _Exit;
    co_printf("speaker_flash_start\r\n");

    tone_play_state_set(1);
    //memset((void *)&sbc_sotre_env, 0, sizeof(sbc_sotre_env));
    //flash_read(USER_FLASH_BASE_ADDR, sizeof(sbc_sotre_env), (uint8_t *)&sbc_sotre_env);//读取Flash存储的audio信息
//    tone_idx = 0;
    if(tone_delay_play_get() < 0xff)
    {
        playing_tone_idx = tone_delay_play_get();
        tone_delay_play_set(0xff);
    }
    switch(playing_tone_idx)
    {
        //NOTE sbc flash 存储地址
        // case 0: 
        //     len = TONE0_LEN;
        //     sbc_sotre_env.start_base = (uint32_t)&callin_ch[0]&0x7FFFF;
        //     break;
        // case 1:
        //     len = TONE1_LEN;
        //     sbc_sotre_env.start_base = (uint32_t)&callin_en[0]&0x7FFFF;
        //     break;
        // case 2:
        //     len = TONE2_LEN;
        //     sbc_sotre_env.start_base = (uint32_t)&callover_ch[0]&0x7FFFF;
        //     break;
        // case 3:
        //     len = TONE3_LEN;
        //     sbc_sotre_env.start_base = (uint32_t)&callover_en[0]&0x7FFFF;
        //     break;
//        case 4:
//            len = TONE4_LEN;
//            sbc_sotre_env.start_base = (uint32_t)&tone4_buff[0]&0x7FFFF;
//            break;
//        case 5:
//            len = TONE5_LEN;
//            sbc_sotre_env.start_base = (uint32_t)&tone5_buff[0]&0x7FFFF;
//            break;
//        case 6:
//            len = TONE6_LEN;
//            sbc_sotre_env.start_base = (uint32_t)&tone6_buff[0]&0x7FFFF;
//            break;
        default:
            break;
    }

//    len = TONE6_LEN;
//    sbc_sotre_env.start_base = (uint32_t)&welcome[0]&0x7FFFF;
//    tone_idx++;
//    if(tone_idx > 6)
//        tone_idx = 0;
    sbc_sotre_env.last_page_idx = len/0x1000;
    sbc_sotre_env.last_offset = len-len/0x1000*0x1000;
    sbc_sotre_env.tot_data_len = len;
    
    co_printf("%x,%d,%d\r\n",sbc_sotre_env.start_base,sbc_sotre_env.last_page_idx,sbc_sotre_env.last_offset);
    if(sbc_sotre_env.start_base == 0xffffffff)//异常
    {
        memset((void *)&sbc_sotre_env,0x00,sizeof(sbc_sotre_env));
		Flash_data_state = false;//flash没有音频数据
        goto _Exit;
    }
	Flash_data_state = true;//flash中有音频数据
    speaker_init();//speaker 初始化

    sbc_buff = (uint8_t *)os_zalloc(10*1024);//申请10kbuffer


    uint8_t *tmp_buf;

    flash_read(sbc_sotre_env.start_base, 512, sbc_buff);//读取512个字节的数据

    memset((void *)&speaker_env, 0, sizeof(speaker_env));
    tmp_buf = sbc_buff;
    speaker_env.sbc_frame_len = decoder_calc_adpcm_ms_frame_len(&tmp_buf);//获取sbc_frame_len
    //sbc_sotre_env.start_base += (tmp_buf - sbc_buff);
    speaker_env.last_read_offset = (tmp_buf - sbc_buff);
    speaker_env.last_read_page_idx = 0;
    if(speaker_env.sbc_data_tot_len == 0)
        speaker_env.sbc_data_tot_len = (10240 - 10240%speaker_env.sbc_frame_len)&(~0x1);
    speaker_env.store_data_len += read_sbc_from_flash(sbc_buff,speaker_env.sbc_data_tot_len>>1);

    decoder_start((uint32_t)sbc_buff, (uint32_t)sbc_buff + speaker_env.sbc_data_tot_len
                  , speaker_env.store_data_len, speaker_env.sbc_frame_len, 0, 0);

_Exit:
    ;
}


