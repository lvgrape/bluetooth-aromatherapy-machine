/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-31 18:37:53
 * @LastEditTime: 2023-05-31 18:37:53
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\ble_role\woodmaster.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __WOODMASTER_H_
#define __WOODMASTER_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

void woodmaster_start_adv(void);
void woodmaster_start_scan(void);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
