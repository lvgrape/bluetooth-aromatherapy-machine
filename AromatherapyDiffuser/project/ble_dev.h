/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-30 18:14:05
 * @LastEditTime: 2023-06-05 17:48:17
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\ble_role\ble_dev.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __BLE_DEV_H_
#define __BLE_DEV_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>
#include "gap_api.h"
#define BLE_NAME_LEN_MAX 24
struct ble_dev_info {
    gap_mac_addr_t  src_addr;
    uint8_t rssi;
    uint8_t name[BLE_NAME_LEN_MAX];
    uint8_t name_len;
    struct ble_dev_info* next;
};
struct ble_dev_info* find_ble_device(struct ble_dev_info* newDev);
struct ble_dev_info* ble_dev_last();
struct ble_dev_info* ble_find_by_name(char* name, uint8_t namelen);
struct ble_dev_info*  ble_dev_add(struct ble_dev_info* newDev);
void show_found_dev();
void ade_rsp_get_dev_name(gap_evt_adv_report_t* rsp, struct ble_dev_info* dev);
void adv_rsp_data_decode(uint8_t length, uint8_t* buffer);
struct ble_dev_info*  adv_rsp_data_decode_dev(gap_evt_adv_report_t* rsp);
void printf_hex(uint8_t *data,uint32_t len,uint8_t dbg_on);
void notify_data(uint8_t con_idx,uint8_t svc_id,uint8_t att_idx,uint8_t *data,uint16_t len);
uint8_t get_slave_link_conidx(void);
const char* svc_att_type_str(uint8_t type);
const char* get_gatt_msg_str(uint8_t msg);
void get_gatt_prop_str(uint16_t prop);
void reset_scan_timeout(void);
uint8_t is_scan_timeout();
void ble_dev_clear();
extern uint8_t slave_link_conidx;
/*********************************************************************
 * Profile Attributes - Table
 * 每一项都是一个attribute的定义。
 * 第一个attribute为Service 的的定义。
 * 每一个特征值(characteristic)的定义，都至少包含三个attribute的定义；
 * 1. 特征值声明(Characteristic Declaration)
 * 2. 特征值的值(Characteristic value)
 * 3. 特征值描述符(Characteristic description)
 * 如果有notification 或者indication 的功能，则会包含四个attribute的定义，除了前面定义的三个，还会有一个特征值客户端配置(client characteristic configuration)。
 *
 */
/*********************************************************************/
#define GATT_ATTRIBUTE16(IDX, UUID, RPOP, SIZE, DATA) \
[IDX] = { \
    {UUID_SIZE_2,UUID16_ARR(UUID)}, \
    RPOP,\
    SIZE,\
    (uint8_t*)DATA \
}
/**Service 的的定义 */
#define GATT_ATT_SVC16(IDX,SVC) \
    GATT_ATTRIBUTE16(IDX,GATT_PRIMARY_SERVICE_UUID,GATT_PROP_READ,2,SVC)
/**特征值声明(Characteristic Declaration) */
#define GATT_ATT_DECL(IDX) \
    GATT_ATTRIBUTE16(IDX,GATT_CHARACTER_UUID,GATT_PROP_READ,0,NULL)
/**特征值的值(Characteristic value)，如果打开了NOTIFY 则需要跟着 GATT_ATT_CCCD*/
#define GATT_ATT_VALU(IDX,UUID,RPOP,SIZE) \
    GATT_ATTRIBUTE16(IDX,UUID,RPOP,SIZE,NULL)
/**特征值描述符(Characteristic description) */
#define GATT_ATT_DESC(IDX,SIZE,DESC) \
    GATT_ATTRIBUTE16(IDX,GATT_CHAR_USER_DESC_UUID,GATT_PROP_READ,SIZE,DESC)
/**特征值客户端配置,一般要跟在需要NOTIFY的后面 */
#define GATT_ATT_CCCD(IDX,RPOP) \
    GATT_ATTRIBUTE16(IDX,GATT_CLIENT_CHAR_CFG_UUID,RPOP,2,NULL)
    
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
