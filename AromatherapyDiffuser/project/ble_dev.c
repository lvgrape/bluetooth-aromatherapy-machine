#include "ble_dev.h"
#include <string.h>
#include "os_mem.h"
#include "co_printf.h"
#include "gatt_api.h"

uint8_t slave_link_conidx;

static uint16_t scanWait = 0;
static struct ble_dev_info* getG6_device = 0;
struct ble_dev_info* bleDevsCur = 0;
struct ble_dev_info* bleDevsRoot = 0;
static uint16_t ble_dev_count = 0;
#define SCAN_MAX_WAITE 2500
uint8_t get_slave_link_conidx(void)
{
    return 	slave_link_conidx;
}
uint16_t get_scanwait()
{
    return scanWait;
}
uint8_t is_scan_timeout()
{
    return (scanWait >= SCAN_MAX_WAITE);
}
void reset_scan_timeout(void)
{
    scanWait = 0;
}
struct ble_dev_info* find_ble_device(struct ble_dev_info* newDev)
{
    struct ble_dev_info* dev;
    dev = bleDevsRoot;
    while (dev)
    {
        if (memcmp(&dev->src_addr, &newDev->src_addr, sizeof(gap_mac_addr_t)) == 0)
        {
            return dev;
        }
        dev = dev->next;
    }
    return 0;
}
struct ble_dev_info* ble_dev_last()
{
    struct ble_dev_info* dev;
    dev = bleDevsRoot;
    if (dev)
    {
        while (dev->next)/**后面还有 */
        {
            dev = dev->next;/**下一个 */
        }
    }
    return dev;
}
void ble_dev_clear()
{
    struct ble_dev_info* dev;
    struct ble_dev_info* cur;

    dev = bleDevsRoot;
    uint8_t devCnt=0;
    // cur = bleDevsRoot;
    if (dev)
    {
        do {
            cur = dev;
            dev = dev->next;
            // co_printf("Free:%08X-%d \r\n",cur,devCnt);
            devCnt++;
            os_free(cur);/**释放dev */
        } while (dev);
    }
    bleDevsRoot = 0;
    dev = 0;
    ble_dev_count = 0;
}
struct ble_dev_info* ble_find_by_name(char* name, uint8_t namelen)
{
    struct ble_dev_info* dev;
    dev = bleDevsRoot;
    while (dev)/**后面还有 */
    {
        if (memcmp(name, dev->name, namelen) == 0)
        {
            return dev;
        }
        dev = dev->next;/**下一个 */
    }
    return 0;
}
/**
 * @brief
 *
 * @param newDev
 * @return struct ble_dev_info* of new dev
 */
struct ble_dev_info* ble_dev_add(struct ble_dev_info* newDev)
{
    struct ble_dev_info* dev;
    dev = ble_dev_last();
    // co_printf("ble add: %08X ", dev);
    if (dev)
    {
        dev->next = (struct ble_dev_info*)os_malloc(sizeof(struct ble_dev_info));
        // co_printf("new malloc dev:0x%08X - %d\r\n", dev->next,ble_dev_count);
        /**添加设备 */
        memcpy(dev->next, newDev, sizeof(struct ble_dev_info));
        ble_dev_count++;
        dev = dev->next;
    }
    else
    {
        /**分配地址 */
        bleDevsRoot = (struct ble_dev_info*)os_malloc(sizeof(struct ble_dev_info));
        // co_printf("first malloc dev:0x%08X -%d\r\n", bleDevsRoot,ble_dev_count);
        memcpy(bleDevsRoot, newDev, sizeof(struct ble_dev_info));
        ble_dev_count++;
        dev = bleDevsRoot;
    }
    return dev;
}
void show_found_dev()
{
    struct ble_dev_info* dev;
    dev = bleDevsRoot;
    uint8_t devCnt=0;
    co_printf("Dev found: %d\n",ble_dev_count);
    while (dev)
    {
        co_printf("*%02d - %02X:%02X:%02X:%02X:%02X:%02X rssi: %d %s\n",
            devCnt++,
            dev->src_addr.addr.addr[5],
            dev->src_addr.addr.addr[4],
            dev->src_addr.addr.addr[3],
            dev->src_addr.addr.addr[2],
            dev->src_addr.addr.addr[1],
            dev->src_addr.addr.addr[0],
            dev->rssi,
            dev->name
        );
        dev = dev->next;
    }
}
void ade_rsp_get_dev_name(gap_evt_adv_report_t* rsp, struct ble_dev_info* dev)
{
    uint8_t len;
    uint8_t type;
    uint8_t* data;
    uint8_t index = 0;
    memset(dev->name, 0, BLE_NAME_LEN_MAX);
    dev->name_len = 0;
    do
    {
        len = rsp->data[index];
        type = rsp->data[index + 1];
        if (type == GAP_ADVTYPE_LOCAL_NAME_COMPLETE)//广播设备名称
        {
            memcpy(dev->name, &rsp->data[index + 2], len);
            dev->name_len = len;
        }
        index += (len + 1);
    } while (index < rsp->length);
    if (dev->name_len == 0)
    {
        memcpy(dev->name, "Unknow", 7);
        dev->name_len = 8;
    }
}



void adv_rsp_data_decode(uint8_t length, uint8_t* buffer)
{
    uint8_t len;
    uint8_t type;
    uint8_t* data;
    uint8_t index = 0;

    do
    {
        len = buffer[index];
        type = buffer[index + 1];
        // co_printf("len:%d type:%x data:",len,type);
        if (type == GAP_ADVTYPE_LOCAL_NAME_COMPLETE)//广播设备名称
        {
            // ble_dev_list.addr
            co_printf("\t\t>>get device name:%s >>\t\t\n", &buffer[index + 2]);
            if ((buffer[index + 2] == 'G') && (buffer[index + 3] == '6') && (buffer[index + 4] == 0))
            {
                co_printf("\t\t****get G6 device\t\t");
                // getG6_device = 1;
            }
        }
        // for(int i=0;i<len;i++)
        // {
        //     co_printf("%02X ",buffer[index+2+i]);
        // }
        // co_printf("\n");
        index += (len + 1);
    } while (index < length);



}
/**
 * @brief 将广播数据转换为对应的设备添加设备列表
 *
 * @param rsp
 */
struct ble_dev_info* adv_rsp_data_decode_dev(gap_evt_adv_report_t* rsp)
{
    struct ble_dev_info newDev = { 0 };
    memcpy(&newDev.src_addr, &rsp->src_addr, sizeof(gap_mac_addr_t));
    scanWait++;
    if (find_ble_device(&newDev) == 0)
    {
        newDev.rssi = rsp->rssi;
        newDev.next = 0;
        ade_rsp_get_dev_name(rsp, &newDev);
        // co_printf("\n Found BLE %d: %02X:%02X:%02X:%02X:%02X:%02X p:%d r:%d - %s \r\n", 
        //     ble_dev_count, 
        //     newDev.src_addr.addr.addr[5],
        //     newDev.src_addr.addr.addr[4],
        //     newDev.src_addr.addr.addr[3],
        //     newDev.src_addr.addr.addr[2],
        //     newDev.src_addr.addr.addr[1],
        //     newDev.src_addr.addr.addr[0],
        //     rsp->tx_pwr,
        //     rsp->rssi,
        //     newDev.name
        // );
        return ble_dev_add(&newDev);
    }
    return 0;
}
void printf_hex(uint8_t* data, uint32_t len, uint8_t dbg_on)
{
    uint32_t i = 0;
    if (len == 0 || (dbg_on == 0)) return;
    for (i=0; i < len; i++)
    {
        co_printf("0x%02X, ", data[i]);
    }
    co_printf("\r\n");
}
void notify_data(uint8_t con_idx, uint8_t svc_id, uint8_t att_idx, uint8_t* data, uint16_t len)
{
    gatt_ntf_t ntf_att;
    ntf_att.att_idx = att_idx;
    ntf_att.conidx = con_idx;
    ntf_att.svc_id = svc_id;
    ntf_att.data_len = len;
    ntf_att.p_data = data;
    gatt_notification(ntf_att);
}

const char _svc_att_type_str[5][37] =
{
    "No Attribute Information",
    "Included Service",
    "Characteristic Declaration",
    "Characteristic Attribute Value",
    "Characteristic Attribute Descriptor",
};
const char* svc_att_type_str(uint8_t type)
{
    if (type < 5)
        return _svc_att_type_str[type];
    else
        return "Unknown";
}
const char GATTC_MSG_CHAR[10][23] = {
    "GATTC_MSG_READ_REQ",
    "GATTC_MSG_WRITE_REQ",
    "GATTC_MSG_ATT_INFO_REQ",
    "GATTC_MSG_NTF_REQ",
    "GATTC_MSG_IND_REQ",
    "GATTC_MSG_READ_IND",
    "GATTC_MSG_CMP_EVT",
    "GATTC_MSG_LINK_CREATE",
    "GATTC_MSG_LINK_LOST",
    "GATTC_MSG_SVC_REPORT",
};
const char* get_gatt_msg_str(uint8_t msg)
{
    if (msg < 10)
        return GATTC_MSG_CHAR[msg];
    else
        return "unknow";
}
const char GATT_PROP_STR[10][11] = {
"BROADCAST",//        (1<<0)  //!< Attribute is able to broadcast
"READ",//        (1<<1)  //!< Attribute is Readable
"WRITE_CMD",//        (1<<2)  //!< Attribute supports write with no response
"WRITE_REQ",//        (1<<3)  //!< Attribute supports write request
"NOTIFY",//        (1<<4)  //!< Attribute is able to send notification
"INDICATE",//        (1<<5)  //!< Attribute is able to send indication
"AUTH_WRTIE",//        (1<<6)  //!< Attribute supports authenticated signed write
"EXTEND",//        (1<<7)  //!< Attribute supports extended properities
"WRITE",//        (1<<8)  //!< Attribute is Writable (support both write_req and write_cmd)
"AUTHEN",//        (1<<9)  //!< Attribute requires Authentication
};
void get_gatt_prop_str(uint16_t prop)
{
    for (int i = 0;i < 9;i++)
    {
        if (prop & (1 << i))
        {
            co_printf("%s,", GATT_PROP_STR[i]);
        }
    }
}

/**
 * @brief 
 * 
[14:20:07.085] first malloc dev:0x2000B734 -0
[14:20:07.096] new malloc dev:0x2000AB74 - 1
[14:20:07.104] new malloc dev:0x2000AF40 - 2
[14:20:07.116] new malloc dev:0x2000AF14 - 3
[14:20:07.124] new malloc dev:0x2000AB48 - 4
[14:20:07.136] new malloc dev:0x2000AB14 - 5
[14:20:07.136] new malloc dev:0x2000B0D0 - 6
[14:20:07.144] new malloc dev:0x2000AE9C - 7
[14:20:07.176] new malloc dev:0x2000AEE8 - 8
[14:20:07.184] new malloc dev:0x2000AE38 - 9
[14:20:07.193] new malloc dev:0x2000AE70 - 10
[14:20:07.204] new malloc dev:0x2000ADD4 - 11
[14:20:07.224] new malloc dev:0x2000AE0C - 12
[14:20:07.243] new malloc dev:0x2000AD18 - 13
[14:20:07.253] new malloc dev:0x2000A778 - 14

[14:20:42.054] Free:2000B734-0 
[14:20:42.054] Free:2000AB74-1 
[14:20:42.054] Free:2000AF40-2 
[14:20:42.054] Free:2000AF14-3 
[14:20:42.065] Free:2000AB48-4 
[14:20:42.065] Free:2000AB14-5 
[14:20:42.065] Free:2000B0D0-6 
[14:20:42.065] Free:2000AE9C-7 
[14:20:42.065] Free:2000AEE8-8 
[14:20:42.065] Free:2000AE38-9 
[14:20:42.065] Free:2000AE70-10 
[14:20:42.076] Free:2000ADD4-11 
[14:20:42.076] Free:2000AE0C-12 
[14:20:42.076] Free:2000AD18-13 
[14:20:42.076] Free:2000A778-14 
 * 
 */