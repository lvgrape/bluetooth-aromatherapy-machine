/*
 * @Author: LVGRAPE
 * @Date: 2024-01-09 13:51:36
 * @LastEditTime: 2024-02-22 11:04:52
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\project\atd_function.c
 * 要啥没啥，爱咋咋的
 */
#include "zino.h"
#include "motors.h"
#include "rgb_mode.h"
#include "key.h"
#include "os_timer.h"
#include "driver_gpio.h"
 /**
  * @brief 香薰机功能：
  *
  * K1:按1下开机进入手动模式 ，再按1下切换到自动模式，再按关机。
  * K2:手动模式下，按1下开启雾化器，再按1下关闭雾化器。
  * K3:手动模式下，按1下开启RGB，再按1下关闭RGB。
  * K4:手动模式下，按1下播放音乐（音乐循环播放），再按1下关闭音乐（需要APP，暂不支持）。
  *
  * 自动模式：每30秒钟自动启动10秒钟，雾化器、RGB同时打开（音乐不开）。
  * 后期可通过蓝牙同步时间，然后整点雾化器、RGB、音乐同时打开，音乐前面加个整点报时。
  *
  */
enum KEY_INDEX {
    K1 = 0,
    K2,
    K3,
    K4
};
enum atd_mode {
    ATD_MODE_MANUAL = 0,
    ATD_MODE_AUTO = 1,
    ATD_MODE_SLEEP = 2,
};

#define LED_STRIP_ON 0X01
#define LED_STRIP_OFF
#define LED_STRIP_ADDING 0X02
#define LED_STRIP_MAX_BRIGHTNESS 75
#define LED_STRIP_MIN_BRIGHTNESS 5

struct atd_function_t {
    enum atd_mode mode;
    uint8_t scnt;
    uint32_t second_cnt;
    uint32_t interval_time;
    uint32_t hold_time;
    uint8_t state;
    uint8_t ledStrip;
    uint8_t ledStripState;
    bool nebulization;
    bool rgb;
    bool music;
}atd_handle =
{
    .interval_time = 30,
    .hold_time = 10,
};

static os_timer_t atd_timer;

void led_on()
{
    gpio_set_dir(GPIO_PORT_A, GPIO_BIT_1, GPIO_DIR_OUT);
    gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 0);
}
void led_off()
{
    gpio_set_dir(GPIO_PORT_A, GPIO_BIT_1, GPIO_DIR_OUT);
    gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 1);
}
void led_mode(enum atd_mode mode)
{
    static uint8_t cnt;
    cnt++;
    if (mode == ATD_MODE_MANUAL)
    {
        if (cnt < 50)
            led_on();
        else
            led_off();
        if (cnt > 100)cnt = 0;
    }
    else if (mode == ATD_MODE_AUTO)
    {
        if (cnt < 5) led_on();
        else if (cnt < 10) led_off();
        else if (cnt < 15) led_on();
        else if (cnt < 20) led_off();
        else if (cnt > 250) cnt = 0;
    }
    else
    {
        led_off();
    }
}
void rgb_loop(void)
{
#define STEP_SET 100
    const uint8_t rainbow_rgb[] = {
        0xff,0x00,0x00,//红色
        0xff,0xa5,0x00,//橙色
        0xff,0xff,0x00,//黄色
        0x00,0xff,0x00,//绿色
        0x00,0x00,0xff,//蓝色
        0x00,0xee,0xee,//蓝色
        0x4b,0x00,0x82,//紫色   
        0x4b,0x22,0x82,//紫色   
    };
    static uint16_t step = 0;
    static uint8_t cnt = 0;
    uint8_t r, g, b;
    step++;
    if (step <= STEP_SET)
    {
        r = rainbow_rgb[cnt * 3 + 0] * step / STEP_SET;
        g = rainbow_rgb[cnt * 3 + 1] * step / STEP_SET;
        b = rainbow_rgb[cnt * 3 + 2] * step / STEP_SET;
    }
    else if (step > STEP_SET && step <= (STEP_SET * 2))
    {
        r = rainbow_rgb[cnt * 3 + 0] * (STEP_SET * 2 - step) / STEP_SET;
        g = rainbow_rgb[cnt * 3 + 1] * (STEP_SET * 2 - step) / STEP_SET;
        b = rainbow_rgb[cnt * 3 + 2] * (STEP_SET * 2 - step) / STEP_SET;
    }
    else if (step > (STEP_SET * 2))
    {
        step = 0;
        cnt++;
    }
    if (cnt >= (sizeof(rainbow_rgb) / 3))
    {
        cnt = 0;
    }
    // rgb_led_set(cnt, r, g, b);
    rgb_led_set_all(r, g, b);
    rgb_led_show();
}

void atd_function_handle(void* p)
{
    struct atd_function_t
        * atd = (struct atd_function_t*)p;
    if (atd->rgb)
    {
        rgb_loop();
    }
    else
    {
        rgb_led_clear();
        rgb_led_show();
    }
    main_motor_output(0, atd->nebulization * 999);

}
void atd_function_led_strip(void* p)
{
    struct atd_function_t
        * atd = (struct atd_function_t*)p;
    if (atd->ledStripState & LED_STRIP_ON)
    {
        main_motor_output(1, atd->ledStrip * 10);
    }
    else
    {
        main_motor_output(1, 0);
    }

}
void adt_function_switch(void* p)
{
    struct atd_function_t
        * atd = (struct atd_function_t*)p;
    if (atd->mode == ATD_MODE_MANUAL) {
        if (getKeyAct(K1) == 1)
        {
            clearKeyAct(K1);
            atd->music = 0;
            atd->rgb = 0;
            atd->nebulization = 0;
            atd->mode = ATD_MODE_AUTO;
            atd->second_cnt = 0;
            atd->scnt = 0;
            co_printf("ATD_MODE_AUTO \r\n");

        }
        if (getKeyAct(K2) == 1)
        {
            clearKeyAct(K2);
            atd->nebulization = !atd->nebulization;
            co_printf("nebulization = %d \r\n", atd->nebulization);
        }

        if (getKeyAct(K3) == 1)
        {
            clearKeyAct(K3);
            atd->rgb = !atd->rgb;
            co_printf("rgb = %d \r\n", atd->rgb);
        }

        if (getKeyAct(K4) == 1)
        {
            clearKeyAct(K4);
            // atd->music = !atd->music;
            // co_printf("music = %d \r\n", atd->music);

            if (atd->ledStripState & LED_STRIP_ON)
            {
                atd->ledStripState &= ~LED_STRIP_ON;
                
                co_printf("ledStripState off = %d \r\n", atd->ledStripState);
            }
            else
            {
                atd->ledStripState |= LED_STRIP_ON;
                if(atd->ledStrip < LED_STRIP_MIN_BRIGHTNESS) atd->ledStrip = LED_STRIP_MIN_BRIGHTNESS;
                co_printf("ledStripState on = %d \r\n", atd->ledStripState);
            }
        }


        if (isKeyLongPress(K4))
        {

            if (atd->ledStripState & LED_STRIP_ON)
            {
                if (atd->ledStripState & LED_STRIP_ADDING)
                {
                    if (atd->ledStrip > LED_STRIP_MIN_BRIGHTNESS)
                    {
                        atd->ledStrip -= 1;
                    }
                    else
                    {
                        atd->ledStrip = LED_STRIP_MIN_BRIGHTNESS;

                    }
                }
                else
                {
                    if (atd->ledStrip < LED_STRIP_MAX_BRIGHTNESS)
                    {
                        atd->ledStrip += 1;

                    }
                    else
                    {
                        atd->ledStrip = LED_STRIP_MAX_BRIGHTNESS;

                    }
                }
                co_printf("ledStrip = %d \r\n", atd->ledStrip);
            }
        }
        else
        {
            if (atd->ledStripState & LED_STRIP_ON)
            {
                if ((atd->ledStripState & LED_STRIP_ADDING) && (atd->ledStrip == LED_STRIP_MIN_BRIGHTNESS))
                {
                    atd->ledStripState &= ~LED_STRIP_ADDING;

                }
                if (!(atd->ledStripState & LED_STRIP_ADDING) && (atd->ledStrip == LED_STRIP_MAX_BRIGHTNESS))
                {
                    atd->ledStripState |= LED_STRIP_ADDING;
                }
            }
        }

    }
    else if (atd->mode == ATD_MODE_AUTO) {
        if (getKeyAct(K1) == 1)
        {
            clearKeyAct(K1);
            atd->mode = ATD_MODE_SLEEP;
            co_printf("ATD_MODE_SLEEP \r\n");

            atd->music = 0;
            atd->rgb = 0;
            atd->nebulization = 0;
        }
    }
    else if (atd->mode == ATD_MODE_SLEEP)
    {
        if (getKeyAct(K1) == 1)
        {
            clearKeyAct(K1);
            atd->mode = ATD_MODE_MANUAL;
            co_printf("ATD_MODE_MANUAL \r\n");
            // 打开雾化器、RGB、音乐
        }
    }
}
void atd_auto_mode(void* p)
{
    struct atd_function_t
        * atd = (struct atd_function_t*)p;
    if (atd->mode != ATD_MODE_AUTO) return;
    if (atd->scnt++ >= 100)
    {
        atd->second_cnt++;
        atd->scnt = 0;
        co_printf("Auto %d\r\n", atd->second_cnt);

        if (atd->second_cnt >= atd->interval_time)
        {
            // 打开雾化器、RGB、音乐
            atd->rgb = 1;
            atd->music = 1;
            atd->nebulization = 1;
            co_printf("ATD_MODE_AUTO ON \r\n");
            if (atd->second_cnt >= (atd->interval_time + atd->hold_time))
            {
                atd->second_cnt = 0;
                // 关闭雾化器、RGB、音乐
                atd->rgb = 0;
                atd->music = 0;
                atd->nebulization = 0;
                co_printf("ATD_MODE_AUTO OFF \r\n");
            }
        }
    }
}
void atd_function_task(void* p)
{
    struct atd_function_t
        * atd = (struct atd_function_t*)p;
    adt_function_switch(p);
    atd_function_handle(p);
    atd_function_led_strip(p);
    led_mode(atd->mode);
    atd_auto_mode(p);

}

int atd_function_init(void) {

    os_timer_init(&atd_timer, atd_function_task, &atd_handle);
    os_timer_start(&atd_timer, 10, 1);
    co_printf("atd_function_init ... \r\n");
    return 0;
};

// ZINO_INIT_APP_EXPORT(atd_function_init);