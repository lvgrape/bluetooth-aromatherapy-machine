/*
 * @Author: LVGRAPE
 * @Date: 2023-05-30 18:10:41
 * @LastEditTime: 2024-02-20 09:31:12
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\project\woodmaster.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
 /**
  * Copyright (c) 2019, Freqchip
  *
  * All rights reserved.
  *
  *
  */

  /*
  * INCLUDES (包含头文件)
  */
#include <stdbool.h>
#include "gap_api.h"
#include "gatt_api.h"
#include "driver_gpio.h"
#include "ota_service.h"
#include "sys_utils.h"
#include "flash_usage_config.h"
#include "dev_info_service.h"
#include "batt_service.h"
#include "ota_service.h"
#include "hid_service.h"
#include "speaker_service.h"
#include "os_mem.h"
#include "hid_client.h"
#include <string.h>
#include "ble_dev.h"
#include "woodmaster_service.h"
#include "zino.h"
#include "woodmaster.h"
  /*
   * MACROS (宏定义)
   */

   /*
    * CONSTANTS (常量定义)
    */

    // GAP - Advertisement data (max size = 31 bytes, though this is
    // best kept short to conserve power while advertisting)
    // GAP-广播包的内容,最长31个字节.短一点的内容可以节省广播时的系统功耗.
#define MAX_BOND_INFO_NUM 8
const uint8_t woodmaster_adv_data[] =
{
    // service UUID, to notify central devices what services are included
    // in this peripheral. 告诉central本机有什么服务, 但这里先只放一个主要的.
    13,
    GAP_ADVTYPE_LOCAL_NAME_COMPLETE,
    'W','T','-','A','D','0','1','-','L','J','0','1',
    
    // 0x03,   // length of this data
    // GAP_ADVTYPE_16BIT_MORE,      // some of the UUID's, but not all
    // 0xFF,
    // 0xFE,

    // 0x03, 0x03, 0x12, 0x18, //HID UID
    0x03, 0x19, 0xc3, 0x03 //JOY STICK

};

const uint8_t woodmaster_scan_rsp_data[]={
    23,
    GAP_ADVTYPE_LOCAL_NAME_COMPLETE,
    'W','O','O','D','W','O','W',' ','W','o','o','d','m','a','s','t','e','r', ' ', 'L', '1',0,
    0x02,
    GAP_ADVTYPE_POWER_LEVEL,
    0
};
/*
 * LOCAL FUNCTIONS (本地函数)
 */


/*********************************************************************
 * @fn      app_gap_evt_cb
 *
 * @brief   Application layer GAP event callback function. Handles GAP evnets.
 *
 * @param   p_event - GAP events from BLE stack.
 *
 *
 * @return  None.
 */
void woodmaster_app_gap_evt_cb(gap_event_t* p_event)
{
    // co_printf("app_gap_evt_cb:type:%d %d \r\n", p_event->type, p_event->conidx);
    switch (p_event->type)
    {
        /****************** Slave role events ******************/
    case GAP_EVT_ADV_END:
    {
        co_printf("adv_end,status:0x%02x\r\n", p_event->param.adv_end.status);
        //gap_start_advertising(0);
    }
    break;

    case GAP_EVT_ALL_SVC_ADDED:
    {
        co_printf("GAP_EVT_ALL_SVC_ADDED \r\n");
        woodmaster_start_adv();
        // woodmaster_start_scan();
#ifdef USER_MEM_API_ENABLE
        //show_mem_list();
        //show_msg_list();
        //show_ke_malloc();
#endif
    }
    break;

    case GAP_EVT_SLAVE_CONNECT:
    {
        co_printf("slave[%d]:%02X:%02X:%02X:%02X:%02X:%02X,connect. link_num:%d\r\n", p_event->param.slave_connect.conidx,
            p_event->param.slave_connect.peer_addr.addr[0],
            p_event->param.slave_connect.peer_addr.addr[1],
            p_event->param.slave_connect.peer_addr.addr[2],
            p_event->param.slave_connect.peer_addr.addr[3],
            p_event->param.slave_connect.peer_addr.addr[4],
            p_event->param.slave_connect.peer_addr.addr[5],
            gap_get_connect_num());
        // hid_service_enable(p_event->param.slave_connect.conidx);
        // co_printf("HID service enable\n");
        slave_link_conidx = p_event->param.slave_connect.conidx;
    }
    break;

    case GAP_SEC_EVT_SLAVE_ENCRYPT:
        co_printf("slave[%d]_encrypted\r\n", p_event->param.slave_encrypt_conidx);
        break;

        /****************** Master role events ******************/
    case GAP_EVT_SCAN_END:
        co_printf("scan_end,status:0x%02x\r\n", p_event->param.scan_end_status);
        break;

    case GAP_EVT_ADV_REPORT:
        adv_rsp_data_decode_dev(p_event->param.adv_rpt);
        if (is_scan_timeout())
        {
            gap_stop_scan();
            reset_scan_timeout();
            // show_found_dev();
        }
        break;

    case GAP_EVT_MASTER_CONNECT:
    {
        co_printf("master[%d],connect. link_num:%d\r\n", p_event->param.master_connect.conidx, gap_get_connect_num());
        //master_link_conidx = (p_event->param.master_connect.conidx);
#if 1
        if (gap_security_get_bond_status())
            gap_security_enc_req(p_event->param.master_connect.conidx);
        else
            gap_security_pairing_req(p_event->param.master_connect.conidx);
#else
#endif
        // gatt_discovery_all_peer_svc(woodmaster_client_id, p_event->param.master_encrypt_conidx);
        // gatt_discovery_all_peer_svc(hid_client_id, p_event->param.master_encrypt_conidx);
        /**NOTE 连接到从机后可以连接扫描，再连接多个！ */
        // woodmaster_start_scan();
    }
    break;


    /****************** Common events ******************/
    case GAP_EVT_DISCONNECT:
    {
        co_printf("Link[%d] disconnect,reason:0x%02X\r\n", p_event->param.disconnect.conidx
            , p_event->param.disconnect.reason);
        woodmaster_start_adv();
#ifdef USER_MEM_API_ENABLE
        show_mem_list();
        //show_msg_list();
        show_ke_malloc();
#endif
    }
    break;

    case GAP_EVT_LINK_PARAM_REJECT:
        co_printf("Link[%d]param reject,status:0x%02x\r\n"
            , p_event->param.link_reject.conidx, p_event->param.link_reject.status);
        break;

    case GAP_EVT_LINK_PARAM_UPDATE:
        co_printf("Link[%d]param update,interval:%d,latency:%d,timeout:%d\r\n", p_event->param.link_update.conidx
            , p_event->param.link_update.con_interval, p_event->param.link_update.con_latency, p_event->param.link_update.sup_to);
        break;

    case GAP_EVT_PEER_FEATURE:
        co_printf("peer[%d] feats ind\r\n", p_event->param.peer_feature.conidx);
        show_reg((uint8_t*)&(p_event->param.peer_feature.features), 8, 1);
        break;

    case GAP_EVT_MTU:
        co_printf("mtu update,conidx=%d,mtu=%d\r\n"
            , p_event->param.mtu.conidx, p_event->param.mtu.value);
        break;

    case GAP_EVT_LINK_RSSI:
        co_printf("link rssi %d\r\n", p_event->param.link_rssi);
        break;

    default:
        co_printf("app_gap_evt_cb:type:%d %d \r\n", p_event->type, p_event->conidx);
        break;
    }
}

/*********************************************************************
 * @fn      woodmaster_start_adv
 *
 * @brief   Set advertising data & scan response & advertising parameters and start advertising
 *
 * @param   None.
 *
 *
 * @return  None.
 */
void woodmaster_start_adv(void)
{
    // Set advertising parameters
    gap_adv_param_t adv_param;
    adv_param.adv_mode = GAP_ADV_MODE_UNDIRECT;
    adv_param.adv_addr_type = GAP_ADDR_TYPE_PUBLIC;
    adv_param.adv_chnl_map = GAP_ADV_CHAN_ALL;
    adv_param.adv_filt_policy = GAP_ADV_ALLOW_SCAN_ANY_CON_ANY;
    adv_param.adv_intv_min = 300;
    adv_param.adv_intv_max = 300;

    gap_set_advertising_param(&adv_param);
    // Set advertising data & scan response data
    gap_set_advertising_data((uint8_t*)woodmaster_adv_data, sizeof(woodmaster_adv_data));
    gap_set_advertising_rsp_data((uint8_t*)woodmaster_scan_rsp_data, sizeof(woodmaster_scan_rsp_data));
    // Start advertising
    co_printf("Start advertising...\r\n");
    gap_start_advertising(0);
}

/*********************************************************************
 * @fn      woodmaster_start_scan
 *
 * @brief   Set central role scan parameters and start scanning BLE devices.
 *
 * @param   None.
 *
 *
 * @return  None.
 */
void woodmaster_start_scan(void)
{
    // Start Scanning
    co_printf("Start scanning...\r\n");
    gap_scan_param_t scan_param;
    scan_param.scan_mode = GAP_SCAN_MODE_GEN_DISC;
    scan_param.dup_filt_pol = 0;
    scan_param.scan_intv = 32;  //scan event on-going time
    scan_param.scan_window = 20;
    scan_param.duration = 0;
    gap_start_scan(&scan_param);
}



/*********************************************************************
 * @fn      woodmaster_Init
 *
 * @brief   Initialize multi role, including peripheral & central roles,
 *          and BLE related parameters.
 *
 * @param   None.
 *
 *
 * @return  None.
 */
int woodmaster_Init(void)
{
    // Initialize security related settings.
    gap_security_param_t param =
    {
        .mitm = false, // use PIN code during bonding
        .ble_secure_conn = false,//not enable security encryption
        .io_cap = GAP_IO_CAP_NO_INPUT_NO_OUTPUT,//ble device has input ability, will input pin code. 
        .pair_init_mode = GAP_PAIRING_MODE_WAIT_FOR_REQ,//need bond
        .bond_auth = false,//need bond auth
        .password = 1234,//need bond auth
    };

    gap_security_param_init(&param);

    gap_set_cb_func(woodmaster_app_gap_evt_cb);

    gap_bond_manager_init(BLE_BONDING_INFO_SAVE_ADDR, 0, 8, true);
    //gap_bond_manager_delete_all();

    // set local device name
#if defined (DIS_MODEL_NB_STR)
    uint8_t local_name[] = DIS_MODEL_NB_STR;
#else 
    uint8_t local_name[] = "AromatherapyDiffuser a1";
#endif
    gap_set_dev_name(local_name, sizeof(local_name));
    char tempLn[32];
    gap_get_dev_name(tempLn);
    mac_addr_t addr;
    gap_address_get(&addr);
    co_printf("Local BDADDR: %2X:%2X:%2X:%2X:%2X:%2X\r\n", addr.addr[0], addr.addr[1], addr.addr[2], addr.addr[3], addr.addr[4], addr.addr[5]);
    co_printf("Local dev_name: %s\r\n", tempLn);
    dis_gatt_add_service();
    co_printf("add divce info service...\r\n");
    batt_gatt_add_service();
    co_printf("add battery service\r\n");
    woodmaster_gatt_add_service();
    co_printf("add woodmaster service...\r\n");
    // hid_gatt_add_service();
    // co_printf("add hid service...\r\n");
    ota_gatt_add_service();
    co_printf("add ota service...\r\n");

    // speaker_gatt_add_service();
    // co_printf("add speaker service...\r\n");
    // sp_gatt_add_service();
    /**NOTE have and only have client in the system */
    // woodmaster_client_create();
    // co_printf("add woodmaster client...\r\n");
    co_printf("get bond info...\r\n");
    gap_bond_info_t bondInfo;
    for (uint8_t i = 0;i < MAX_BOND_INFO_NUM;i++)
    {
        gap_bond_manager_get_info(i, &bondInfo); //get info from other device and store it in the array.
        if (bondInfo.bond_flag)
        {

            co_printf("[%d] Flag:%d t:%d ", i, bondInfo.bond_flag, bondInfo.peer_addr.addr_type);
            printf_hex(bondInfo.peer_addr.addr.addr, 6, 1);
            printf_hex(bondInfo.peer_irk, 16, 1);
            printf_hex(bondInfo.peer_ltk, 16, 1);
        }

    }
    return 0;
}

ZINO_INIT_BOARD_EXPORT(woodmaster_Init);
