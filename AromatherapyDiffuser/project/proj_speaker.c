/**
 * Copyright (c) 2019, Tsingtao Freqchip
 *
 * All rights reserved.
 *
 *
 */

 /*
 * INCLUDES (包含头文件)
 */
#include <stdbool.h>
#include <stddef.h>


#include "driver_plf.h"
#include "driver_i2s.h"
#include "driver_codec.h"
#include "driver_uart.h"
#include "driver_pmu.h"
#include "driver_gpio.h"

#include "driver_codec.h"
#include "audio_decoder.h"

#include "os_timer.h"
#include "os_mem.h"

#include "sys_utils.h"
 //#include "hid_client.h"
 //#include "proj_main.h"

 //#include "proj_gap_event.h"
#include "driver_system.h"
#include "driver_gpio.h"
#include "w25qxx.h"
#include "zino.h"
// #include "decoder.h"
// #include "tone.h"
/*
 * MACROS (宏定义)
 */
#define FLASH_PAGE_SIZE 4096
 /*
  * CONSTANTS (常量定义)
  */


  /*
   * TYPEDEFS (类型定义)
   */
typedef struct
{
    uint32_t start_base;
    uint32_t tot_data_len;
    uint32_t last_offset;
    uint8_t last_page_idx;
} sbc_store_info_t;

typedef struct
{
    uint32_t last_read_offset;
    uint8_t last_read_page_idx;
    uint8_t end_flag;

    uint32_t store_data_len;
    uint16_t sbc_frame_len;
    uint16_t sbc_data_tot_len;
} speaker_env_t;
struct sbc_info {
    uint8_t head;
    uint32_t size;
    uint8_t name_len;
    uint8_t name[17];
    uint8_t tail;
}__packed;
uint32_t get_sbc_data_len(void)
{
    uint8_t readB[24];
    w25qxx_ReadData(0, readB, 24);

    // for (uint16_t i = 0;i < 24;i++)
    // {
    //     co_printf("0x%02X, ", readB[i]);
    //     if (i % 16 == 15)
    //     {
    //         co_printf("\r\n");
    //     }
    // }
    struct sbc_info sbcInfo;
    // w25qxx_ReadData(0, (uint8_t*)&sbcInfo, sizeof(sbcInfo));
    memcpy(&sbcInfo, readB, sizeof(struct sbc_info));
    // show_reg((uint8_t*)&sbcInfo, sizeof(struct sbc_info), 1);
    if (sbcInfo.head == '<' && sbcInfo.tail == '>')
    {
        co_printf("sbc name:%s\r\n", sbcInfo.name);
        co_printf("len:%d\r\n", sbcInfo.size);
    }
    else
    {
        co_printf("error, can't find sbc info!\r\n");
    }
    return sbcInfo.size;
}
/*
 * GLOBAL VARIABLES (全局变量)
 */
sbc_store_info_t sbc_store_env = { 0 };
uint8_t* sbc_buff = NULL;
speaker_env_t speaker_env = { 0 };
uint8_t Flash_data_state = true;
/*
 * LOCAL VARIABLES (本地变量)
 */
const uint8_t callin_ch[10] = { 0 };

/*
 * LOCAL FUNCTIONS (本地函数)
 */

 /*
  * EXTERN FUNCTIONS (外部函数)
  */

  /*
   * PUBLIC FUNCTIONS (全局函数)
   */

   /** @function group ble peripheral device APIs (ble外设相关的API)
    * @{
    */

#define I2S_FIFO_DEPTH      64
#define PA_ENABLE      gpio_set_pin_value(GPIO_PORT_C, GPIO_BIT_7, 1)//gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 1)
#define PA_DISABLE     gpio_set_pin_value(GPIO_PORT_C, GPIO_BIT_7, 0)//gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 0)

    // #define PA_ENABLE      gpio_porta_write(gpio_portc_read() | (1<<GPIO_BIT_7) )//gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 1)
    // #define PA_DISABLE     gpio_porta_write(gpio_portc_read() & ~(1<<GPIO_BIT_7) )//gpio_set_pin_value(GPIO_PORT_A, GPIO_BIT_1, 0)


__attribute__((section("ram_code"))) void i2s_isr_ram(void)
{
    uint32_t last = 0;
    if (i2s_reg->status.tx_half_empty)
    {
        uint8_t i;
        struct co_list_hdr* element;
        struct decoder_pcm_t* pcm;
        uint32_t* tx_data;

        if (co_list_is_empty(&decoder_env.pcm_buffer_list))
        {
            //fputc('F', 0);
            for (i = 0; i < (I2S_FIFO_DEPTH / 2); i++)
            {
                i2s_reg->data = 0;
            }
        }
        else
        {
            element = decoder_env.pcm_buffer_list.first;

            pcm = (struct decoder_pcm_t*)element;
            tx_data = (uint32_t*)&pcm->pcm_data[pcm->pcm_offset];
            last = pcm->pcm_size - pcm->pcm_offset;
            if (last > (I2S_FIFO_DEPTH / 2))
            {
                //fputc('X', 0);
                for (i = 0; i < (I2S_FIFO_DEPTH / 2); i++)
                {
                    i2s_reg->data = ((*tx_data++) & 0xFFFF);
                }
                pcm->pcm_offset += (I2S_FIFO_DEPTH / 2);
            }
            else
            {
                //fputc('Y', 0);
                for (i = 0; i < last; i++)
                {
                    i2s_reg->data = ((*tx_data++) & 0xFFFF);
                }
                co_list_pop_front(&decoder_env.pcm_buffer_list);
                os_free((void*)pcm);
                decoder_env.pcm_buffer_counter--;
                audio_decoder_play_next_frame();

                while (!co_list_is_empty(&decoder_env.pcm_buffer_list))
                {
                    element = decoder_env.pcm_buffer_list.first;

                    pcm = (struct decoder_pcm_t*)element;
                    tx_data = (uint32_t*)&pcm->pcm_data[0];
                    last = pcm->pcm_size - pcm->pcm_offset;
                    if ((last + i) > (I2S_FIFO_DEPTH / 2))
                    {
                        //fputc('Z', 0);
                        for (; i < (I2S_FIFO_DEPTH / 2); i++)
                        {
                            i2s_reg->data = ((*tx_data++) & 0xFFFF);
                        }
                        pcm->pcm_offset = (I2S_FIFO_DEPTH / 2) - i;
                        break;
                    }
                    else
                    {
                        // fputc('W', 0);
                        last += i;
                        for (; i < last; i++)
                        {
                            i2s_reg->data = ((*tx_data++) & 0xFFFF);
                        }
                        co_list_pop_front(&decoder_env.pcm_buffer_list);
                        os_free((void*)pcm);
                        decoder_env.pcm_buffer_counter--;
                        audio_decoder_play_next_frame();
                    }
                }
            }
        }
    }
}



void speaker_stop(void)
{
    PA_DISABLE;
    pmu_codec_power_disable();
    codec_disable_dac();
    i2s_stop();
    co_printf("speaker_stop\r\n");
}
void speaker_half_buff(void)
{
    co_printf("speaker_half_buff_fn\r\n");
_Exit:
    ;
}

void speaker_start(void)
{
    pmu_codec_power_enable();
    i2s_start();
    codec_enable_dac();
    PA_ENABLE;
    //audio_decoder_start(uint32_t start, uint32_t end, uint32_t tot_data_len, uint32_t start_offset, DECODER_STORE_TYPE_RAM, speaker_stop);
    co_printf("speaker_test_start\r\n");
}

/*********************************************************************
 * @fn		PA_init_pins
 *
 * @brief	Initialize PA enable pin
 *
 * @param	None
 *
 * @return	None.
 */
void PA_init_pins(void)
{
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_7, PORTC7_FUNC_C7);
    // system_set_port_pull(GPIO_PC7, true);
    gpio_set_dir(GPIO_PORT_C, GPIO_BIT_7, GPIO_DIR_OUT);
    PA_ENABLE;
    co_printf("PA_ENABLE\r\n");
}

void speaker_ini(void)
{
    audio_decoder_init();//codec task init
    pmu_codec_power_enable();//codec power on
    speaker_codec_init(CODEC_SAMPLE_RATE_16000);//set volume
    i2s_init(I2S_DIR_TX, 16000, 1);    //8,16,32,48
    NVIC_SetPriority(I2S_IRQn, 2);
    PA_init_pins();
}





#if 1


// const uint8_t sbc_sample[] =
// {
//     0x9c,0x31,0x1e,0xa5,0xca,0x78,0x67,0x66,0x7e,0xf6,0xed,0xb6,0x03,0xdb,0xb6,0xd8,
//     0x71,0x6e,0xdb,0x62,0x37,0x38,0x94,0xcb,0x9a,0x42,0xe2,0xf2,0xc4,0x54,0x60,0xb0,
//     0x6e,0xdb,0x54,0x3d,0xbb,0x6d,0xf8,0xf6,0xed,0xb6,0x6b,0xd3,0xb6,0xd0,0x0f,0x6e,
//     0xdb,0x5d,0x42,0x3b,0x6d,0xfa,0xf6,0xed,0xb5,0xdb,0xdb,0xb6,0xd0,0x4f,0x8e,0xdb,
//     0x66,0x3d,0xbb,0x6d,0x9c,0x31,0x1e,0x2f,0xc5,0x54,0x10,0x11,0xf6,0xa8,0xab,0x55,
//     0x52,0x54,0x15,0x10,0xf5,0x1a,0x26,0x6e,0xed,0xa1,0x85,0xe6,0x5b,0x5d,0x54,0xda,
//     0x74,0xe5,0x52,0x3d,0x4c,0xa9,0x75,0xa7,0xac,0x92,0xce,0xe9,0x94,0x64,0x77,0x94,
//     0xdd,0x23,0xf4,0xc9,0xc5,0x7a,0xf6,0x35,0x19,0xb0,0x51,0x5a,0x58,0x3b,0xa2,0x91,
//     0x65,0xfe,0xcd,0x55,0x7d,0x51,0x52,0xd5,0x9c,0x31,0x1e,0x21,0xc6,0x54,0x00,0x11,
//     0x8e,0x5c,0x5c,0x14,0x20,0x9c,0xc7,0x58,0x2e,0x45,0xc5,0xbd,0xd7,0x2b,0x36,0x6c,
//     0x15,0x91,0x88,0x3c,0x7e,0x5b,0x5a,0x3a,0x8a,0x39,0xba,0xd6,0xd2,0x76,0x4c,0x92,
//     0x3c,0x14,0x71,0x91,0xd7,0x5c,0x23,0x0a,0x25,0xb6,0x75,0x14,0xa4,0x31,0xde,0x7e,
//     0x90,0xc9,0x6a,0xe5,0x4d,0x8a,0xc6,0x61,0x70,0x08,0x94,0xc5,0x9c,0x31,0x1e,0x56,
//     0xc5,0x54,0x00,0x01,0x1d,0xa6,0xdd,0x2d,0x37,0xb1,0x55,0x4e,0xa1,0xa2,0xb4,0x28,
//     0x9a,0xce,0x88,0x13,0x15,0xcd,0xf9,0xb6,0x26,0xa7,0x2e,0xf5,0x1b,0x0e,0xe0,0x84,
//     0x98,0x61,0x10,0x6f,0x3b,0x92,0x39,0x34,0x96,0x2e,0xd8,0xe3,0x94,0x58,0x5a,0x1e,
//     0xbb,0x18,0x5b,0x1c,0xaa,0xb2,0xbb,0x37,0x2e,0x0e,0x8e,0x71,0x11,0x43,0x4c,0x83,
//     0x9c,0x31,0x1e,0x21,0xc6,0x54,0x00,0x11,0x27,0xe9,0x30,0xd3,0x1f,0x7a,0x54,0x4c,
//     0xdd,0x8a,0x61,0x0b,0x36,0xd5,0x15,0x3f,0xb2,0x5b,0x47,0x66,0xa6,0x46,0xab,0x25,
//     0x94,0xa1,0x47,0xe7,0x53,0x95,0x5b,0x6a,0xac,0xd7,0x96,0x85,0xc9,0x59,0x51,0x91,
//     0x39,0x85,0xb7,0x61,0x44,0x79,0xa7,0x62,0x97,0x9f,0xb0,0xf5,0x57,0x6d,0xa7,0xc2,
//     0x46,0x66,0xf2,0x46,0x9c,0x31,0x1e,0x9b,0xc5,0x54,0x00,0x11,0x97,0xe8,0xba,0x37,
//     0x8b,0xab,0x68,0xa5,0x90,0xb1,0x7c,0x48,0x96,0xd5,0xc9,0xb2,0xa0,0x35,0xd7,0x50,
//     0xb5,0x8a,0x44,0x0e,0x35,0xcb,0x4d,0x36,0xa7,0x72,0xc9,0xd6,0xb4,0x47,0x00,0xa4,
//     0x8e,0x82,0xbd,0xf2,0x99,0x52,0xf6,0x99,0x0d,0xd8,0x07,0x9b,0x2a,0x94,0x55,0x33,
//     0x22,0x2e,0x4e,0x43,0x9a,0x09,0xc0,0xed,0x9c,0x31,0x1e,0x1e,0xc6,0x44,0x00,0x12,
//     0xe0,0x57,0xd5,0x46,0x25,0x75,0x32,0x91,0xde,0x11,0x21,0x61,0x19,0x58,0x78,0xdf,
//     0x8a,0xd5,0x95,0xb5,0x51,0xf2,0x52,0x36,0x3c,0xed,0x67,0xea,0x7a,0xa8,0xd7,0x9c,
//     0x41,0x55,0x4d,0x54,0x72,0x12,0xea,0x78,0xca,0x6e,0x1b,0xf6,0x89,0xc8,0x18,0x95,
//     0x54,0xf7,0x5b,0x51,0x54,0x1d,0xdb,0x9a,0x72,0x99,0x55,0xa5,0x9c,0x31,0x1e,0x4a,
//     0xc5,0x55,0x10,0x02,0xb2,0xe4,0xb8,0x44,0xb7,0x3c,0xdb,0x15,0x8a,0x53,0x52,0x75,
//     0xfc,0xa9,0xe5,0x9a,0x21,0x68,0x24,0x97,0x94,0xee,0xd7,0x31,0x8e,0x74,0x76,0xd7,
//     0x3d,0x2d,0x7f,0x29,0x77,0x34,0x91,0x73,0xa7,0x18,0xe1,0xc6,0x8b,0x76,0x66,0x35,
//     0x8c,0x65,0xdd,0x27,0x38,0xb5,0x8a,0xd8,0xda,0x56,0x13,0x82,0x73,0x16,0x5e,0x89,
//     0x9c,0x31,0x1e,0x9b,0xc5,0x54,0x00,0x11,0x4f,0x1a,0xc7,0x94,0xea,0xa9,0xca,0x5b,
//     0xad,0xb0,0x9d,0x6f,0x24,0xce,0xd0,0x3d,0x26,0x75,0x35,0x38,0x84,0x2b,0x1c,0x92,
//     0xb3,0x24,0x69,0xb4,0xa5,0xcd,0x30,0xd6,0xca,0xb9,0x92,0xcd,0x6e,0x9d,0x32,0x30,
//     0x22,0x63,0xc2,0xa6,0x8d,0x2b,0xec,0xe6,0x31,0xf3,0x9b,0x59,0x1d,0x3a,0x88,0xbc,
//     0x5d,0xa5,0xb5,0x6d,0x9c,0x31,0x1e,0x2f,0xc5,0x54,0x10,0x11,0x2d,0x97,0x62,0x5a,
//     0x55,0xab,0x29,0x5c,0xf6,0xc8,0xd5,0x57,0xb0,0xce,0xe1,0x37,0xf6,0xb1,0x92,0xa6,
//     0x89,0x91,0x5c,0x0e,0xb6,0x34,0x53,0x38,0x52,0xd5,0x46,0x60,0xdc,0x4a,0xed,0x6b,
//     0x99,0x9b,0x0a,0x91,0x35,0x4f,0x46,0x46,0x56,0x59,0xaa,0x87,0x5b,0x1a,0x7c,0xe5,
//     0x49,0xc1,0x71,0x85,0x0d,0x6a,0xa8,0xd5,0x9c,0x31,0x1e,0x83,0xc5,0x44,0x00,0x11,
//     0x6e,0xe1,0x06,0x47,0x33,0x4e,0xf4,0x58,0x59,0xaa,0x93,0x8c,0x79,0x09,0x49,0x85,
//     0x28,0xc7,0xd7,0x2c,0x6a,0x48,0x96,0xf8,0xa8,0x92,0x4d,0xc9,0x33,0x0d,0x9a,0xef,
//     0x98,0x33,0x12,0x62,0x4c,0x95,0xa9,0x09,0x9c,0x10,0x29,0x3e,0x70,0xac,0xe3,0x25,
//     0xb6,0xdd,0x91,0x26,0xd4,0xb1,0x38,0x42,0x54,0x16,0xbd,0x4d,0x9c,0x31,0x1e,0x5a,
//     0xc5,0x54,0x30,0x11,0xbb,0xac,0xd8,0xd6,0x9a,0xa2,0xcb,0x53,0xdd,0x10,0x8c,0x58,
//     0xaa,0xba,0x35,0xc2,0xca,0x94,0xd6,0x48,0x9d,0x33,0x53,0xb6,0x31,0x8d,0x5d,0xf5,
//     0xab,0x36,0xc5,0xde,0xe4,0xe5,0xf7,0x8d,0x2b,0x53,0xa1,0xaf,0x4c,0xa3,0x0a,0x1b,
//     0xb5,0xc2,0x09,0x58,0xd1,0xa9,0x74,0x54,0x44,0x12,0x2d,0x30,0x27,0x97,0xc6,0x55,
// };

#if 1
#define SBC_BUFF_SIZE (10240)//20K
void speaker_end_fn(void)
{
    pmu_codec_power_disable();
    codec_disable_dac();
    i2s_stop();
    co_printf("speaker_test_stop\r\n");
}
extern void led_on();
extern void led_off();
void speaker_half_buff_fn(void)
{
    // co_printf("speaker_half_buff_fn\r\n");
    // led_on();
    uint32_t storeLeft = sbc_store_env.tot_data_len - (sbc_store_env.last_offset - sbc_store_env.start_base);
    uint32_t buffLeft = decoder_env.data_end - decoder_env.current_pos;
    uint32_t buffNeed = SBC_BUFF_SIZE - buffLeft;
    uint32_t thisRead = buffNeed;
    // co_printf("<l:%d n:%d,r:%d,s:%d\r\n",buffLeft,buffNeed,thisRead,storeLeft);
    if (storeLeft > buffNeed)
    {
        thisRead = buffNeed;
        decoder_env.tot_data_len = SBC_BUFF_SIZE;
    }
    else
    {
        decoder_env.tot_data_len = storeLeft;
        thisRead = storeLeft;
    }
    // co_printf("l:%d n:%d,r:%d,s:%d,o:%d>\r\n",buffLeft,buffNeed,thisRead,storeLeft,sbc_store_env.last_offset);
    memcpy(sbc_buff, (uint8_t*)decoder_env.current_pos, buffLeft);
    w25qxx_ReadData(sbc_store_env.last_offset, (sbc_buff + buffLeft), thisRead);
    decoder_env.current_pos = (uint32_t)sbc_buff;
    decoder_env.data_processed_len = 0;
    sbc_store_env.last_offset += thisRead;

    // wdt_feed();
    // led_off();
    // decoder_buff_t buf_env =
    // {
    //     (uint32_t)sbc_buff,
    //     (uint32_t)sbc_buff + thisRead,
    //     thisRead,
    //     0,
    //     DECODER_STORE_TYPE_RAM,
    //     speaker_half_buff_fn,
    //     speaker_end_fn,
    // };
    // audio_decoder_start(buf_env);
}

// void test_speaker1(void)
// {
//     pmu_codec_power_enable();
//     i2s_start();
//     codec_enable_dac();
//     co_printf("speaker_test_start\r\n");

//     decoder_buff_t buf_env =
//     {
//         (uint32_t)sbc_sample,
//         (uint32_t)sbc_sample + sizeof(sbc_sample),
//         sizeof(sbc_sample),
//         0,
//         DECODER_STORE_TYPE_RAM,
//         speaker_half_buff_fn,
//         speaker_end_fn,
//     };
//     audio_decoder_start(buf_env);
// }
#define codec_read(addr)                (uint8_t)frspim_rd(FR_SPI_CODEC_CHAN, addr, 1)
void play_music_1()
{
    pmu_codec_power_enable();
    i2s_start();
    codec_enable_dac();
    codec_set_vol(100);
    co_printf("speaker_test_start;%02X\r\n", codec_read(0x11));

    sbc_store_env.start_base = 24;
    sbc_store_env.tot_data_len = get_sbc_data_len();

    if (sbc_buff == NULL)
    {
        co_printf("sbc_buff new\r\n");
        sbc_buff = (uint8_t*)os_malloc(SBC_BUFF_SIZE);//10K ram buffer
        // return;
    }
    if (!sbc_buff)
    {
        co_printf("sbc_buff malloc fail\r\n");
        return;
    }
    uint32_t thisRead = sbc_store_env.tot_data_len < (SBC_BUFF_SIZE) ?
        sbc_store_env.tot_data_len : (SBC_BUFF_SIZE);
    co_printf("thisRead = %d\r\n", thisRead);
    w25qxx_ReadData(sbc_store_env.start_base, sbc_buff, thisRead);
    sbc_store_env.last_offset = sbc_store_env.start_base + thisRead;
    decoder_buff_t buf_env =
    {
        (uint32_t)sbc_buff,
        (uint32_t)sbc_buff + thisRead,
        thisRead,
        0,
        DECODER_STORE_TYPE_RAM,
        speaker_half_buff_fn,
        speaker_end_fn,
    };
    audio_decoder_start(buf_env);
    co_printf("codec_read 10;%02X\r\n", codec_read(0x10));
    co_printf("codec_read 11;%02X\r\n", codec_read(0x11));
}
void play(int argc, char* argv[])
{
    uint8_t music_idx = atoi(argv[1]);
    uint8_t music_state = atoi(argv[2]);
    switch (music_idx)
    {
    case 1:if (music_state) play_music_1();else audio_decoder_stop();
        co_printf("play music 1\r\n");
        /* code */
        break;

    default:
        break;
    }
}
void vol(int argc, char* argv[])
{
    uint8_t volume = atoi(argv[1]);
    codec_set_vol(volume);
    co_printf("volume:%d\r\n", volume);
}
ZINO_CMD_EXPORT(play, play music);
ZINO_CMD_EXPORT(vol, set volume);
#else
void speaker_end_fn(void)
{
    pmu_codec_power_disable();
    codec_disable_dac();
    i2s_stop();
    co_printf("speaker_test_stop\r\n");
}
uint8_t* play_buff = NULL;
uint32_t played_len = 0;
uint32_t sbc_dec_idx = 0;
uint32_t play_offset = 0;


void speaker_half_buff_fn(void)
{
    uint32_t offset = played_len % 10472;
    uint16_t pos = 0;
    for (uint8_t i = 0; i < 7; i++)        //748 * 7 =5236
    {
        memcpy(play_buff + offset + pos, sbc_sample, 748);
        pos += 748;
    }
    played_len += pos;
    audio_decoder_update_date_len(played_len);
    co_printf("speaker_half_buff_fn\r\n");
_Exit:
    ;
}
#if 1
/*********************************************************************
 * @fn		read_sbc_from_flash
 *
 * @brief	Get adpcm frame len
 *
 * @param	header_org     - Pointer to pointer to ADPCM audio data
 *
 * @return	None.
 */
int read_sbc_from_flash(uint8_t* sbc_buff, uint32_t read_len)
{
    uint32_t r_len = read_len;
    uint32_t pos = 0;
    while (r_len > 0)
    {
        if (speaker_env.last_read_page_idx < sbc_store_env.last_page_idx)
        {
            if ((r_len + speaker_env.last_read_offset) >= FLASH_PAGE_SIZE)
            {
                flash_read(sbc_store_env.start_base + speaker_env.last_read_page_idx * FLASH_PAGE_SIZE + speaker_env.last_read_offset
                    , FLASH_PAGE_SIZE - speaker_env.last_read_offset, sbc_buff + pos);
                r_len -= (FLASH_PAGE_SIZE - speaker_env.last_read_offset);
                pos += (FLASH_PAGE_SIZE - speaker_env.last_read_offset);
                speaker_env.last_read_offset = 0;
                speaker_env.last_read_page_idx++;
            }
            else
            {
                flash_read(sbc_store_env.start_base + speaker_env.last_read_page_idx * FLASH_PAGE_SIZE + speaker_env.last_read_offset
                    , r_len, sbc_buff + pos);
                pos += r_len;
                speaker_env.last_read_offset += r_len;
                r_len = 0;
            }
        }
        else if (speaker_env.last_read_page_idx == sbc_store_env.last_page_idx)
        {
            if (speaker_env.last_read_offset >= sbc_store_env.last_offset)
            {
                return 0;
            }
            else
            {
                if ((r_len + speaker_env.last_read_offset) > sbc_store_env.last_offset)
                {
                    flash_read(sbc_store_env.start_base + speaker_env.last_read_page_idx * FLASH_PAGE_SIZE + speaker_env.last_read_offset
                        , sbc_store_env.last_offset - speaker_env.last_read_offset, sbc_buff + pos);
                    uint32_t no_read_len = (r_len + speaker_env.last_read_offset - sbc_store_env.last_offset);
                    pos += r_len;
                    speaker_env.last_read_offset = sbc_store_env.last_offset;
                    r_len = 0;
                    return (read_len - no_read_len);
                }
                else
                {
                    flash_read(sbc_store_env.start_base + speaker_env.last_read_page_idx * FLASH_PAGE_SIZE + speaker_env.last_read_offset
                        , r_len, sbc_buff + pos);
                    pos += r_len;
                    speaker_env.last_read_offset += r_len;
                    r_len = 0;
                }
            }
        }
        else
            return 0;
    }

    return read_len;
}


/*********************************************************************
 * @fn		decoder_half_processed
 *
 * @brief	Put new playback data into the cache and update the total audio data length tot_data_len.
 *
 * @param	None
 *
 * @return	None.
 */
void decoder_half_processed(void)
{
    if (speaker_env.end_flag)
        goto _Exit;
    else
    {
        uint32_t pos = (speaker_env.store_data_len % speaker_env.sbc_data_tot_len);
        //fputc('z',0);
        uint32_t read_len = read_sbc_from_flash(sbc_buff + pos, speaker_env.sbc_data_tot_len >> 1);
        if (read_len > 0)
        {
            speaker_env.store_data_len += read_len;
            audio_decoder_update_date_len(speaker_env.store_data_len);
        }
        else
            speaker_env.end_flag = 1;
    }
_Exit:
    ;
}
#endif
void test_speaker(void)
{
    pmu_codec_power_enable();
    i2s_start();
    codec_enable_dac();

#if 1   
    if (play_buff == NULL)
    {
        play_buff = os_malloc(10472);        //748 -> 10472
    }

    sbc_dec_idx = sizeof(sbc_sample);

    uint16_t pos = 0;
    for (uint8_t i = 0; i < 7; i++)        //748 * 7 =5236
    {
        memcpy(play_buff + pos, sbc_sample, 748);
        pos += 748;
    }
    played_len += pos;
#else
    play_offset = (uint32_t)&callin_ch[0]; // &0x7FFFF;
#endif
    co_printf("speaker_test_start:%x\r\n", play_offset);

    decoder_buff_t buf_env =
    {
        (uint32_t)play_offset,
        (uint32_t)play_offset + 0xd2f0,
        0xd2f0,
        0,
        DECODER_STORE_TYPE_RAM,
        NULL,
        speaker_end_fn,
    };
#if 1
    uint8_t* tmp_buf;
    memset((void*)&speaker_env, 0, sizeof(speaker_env));
    tmp_buf = sbc_buff;
    speaker_env.sbc_frame_len = 68; // decoder_calc_adpcm_ms_frame_len(&tmp_buf);//获取sbc_frame_len
    speaker_env.last_read_offset = 0;
    speaker_env.last_read_page_idx = 0;
    if (speaker_env.sbc_data_tot_len == 0)
        speaker_env.sbc_data_tot_len = (10240 - 10240 % speaker_env.sbc_frame_len) & (~0x1);
    speaker_env.store_data_len += read_sbc_from_flash(sbc_buff, speaker_env.sbc_data_tot_len >> 1);
#endif
    audio_decoder_start(buf_env);
}
#endif
#if 0
void tone_speaker_from_flash(void)
{
    uint32_t len = 0;
    uint8_t playing_tone_idx = 0;
    //    static uint8_t tone_idx = 0;
#if 0
    if (tone_play_state_get())
    {
        co_printf("=playing=\r\n");
        tone_delay_play_set(tone_idx);
        test_end_speaker();
        return;
    }
#endif
    if (sbc_buff != NULL)
        goto _Exit;
    co_printf("speaker_flash_start\r\n");
#if 0
    tone_play_state_set(1);
    //memset((void *)&sbc_store_env, 0, sizeof(sbc_store_env));
    //flash_read(USER_FLASH_BASE_ADDR, sizeof(sbc_store_env), (uint8_t *)&sbc_store_env);//读取Flash存储的audio信息
//    tone_idx = 0;
    if (tone_delay_play_get() < 0xff)
    {
        playing_tone_idx = tone_delay_play_get();
        tone_delay_play_set(0xff);
    }
    switch (playing_tone_idx)
    {
    case 0:
        len = TONE0_LEN;
        sbc_store_env.start_base = (uint32_t)&callin_ch[0] & 0x7FFFF;
        break;
    case 1:
        len = TONE1_LEN;
        sbc_store_env.start_base = (uint32_t)&callin_en[0] & 0x7FFFF;
        break;
    case 2:
        len = TONE2_LEN;
        sbc_store_env.start_base = (uint32_t)&callover_ch[0] & 0x7FFFF;
        break;
    case 3:
        len = TONE3_LEN;
        sbc_store_env.start_base = (uint32_t)&callover_en[0] & 0x7FFFF;
        break;
        //        case 4:
        //            len = TONE4_LEN;
        //            sbc_store_env.start_base = (uint32_t)&tone4_buff[0]&0x7FFFF;
        //            break;
        //        case 5:
        //            len = TONE5_LEN;
        //            sbc_store_env.start_base = (uint32_t)&tone5_buff[0]&0x7FFFF;
        //            break;
        //        case 6:
        //            len = TONE6_LEN;
        //            sbc_store_env.start_base = (uint32_t)&tone6_buff[0]&0x7FFFF;
        //            break;
    default:
        break;
    }

    //    len = TONE6_LEN;
    //    sbc_store_env.start_base = (uint32_t)&welcome[0]&0x7FFFF;
    //    tone_idx++;
    //    if(tone_idx > 6)
    //        tone_idx = 0;
#endif
    len = sizeof(sbc_sample);
    // sbc_store_env.start_base = sbc_sample;
    sbc_store_env.start_base = 24;

    sbc_store_env.last_page_idx = len / 0x1000;
    sbc_store_env.last_offset = len - len / 0x1000 * 0x1000;
    // sbc_store_env.tot_data_len = len;
    sbc_store_env.tot_data_len = get_sbc_data_len();

    co_printf("%x,%d,%d\r\n", sbc_store_env.start_base, sbc_store_env.last_page_idx, sbc_store_env.last_offset);
    if (sbc_store_env.start_base == 0xffffffff)//异常
    {
        memset((void*)&sbc_store_env, 0x00, sizeof(sbc_store_env));
        Flash_data_state = false;//flash没有音频数据
        goto _Exit;
    }
    Flash_data_state = true;//flash中有音频数据
    // speaker_init();//speaker 初始化

    sbc_buff = (uint8_t*)os_zalloc(10 * 1024);//申请10kbuffer

    uint8_t* tmp_buf;

    flash_read(sbc_store_env.start_base, 512, sbc_buff);//读取512个字节的数据

    memset((void*)&speaker_env, 0, sizeof(speaker_env));
    tmp_buf = sbc_buff;
    speaker_env.sbc_frame_len = decoder_calc_adpcm_ms_frame_len(&tmp_buf);//获取sbc_frame_len
    //sbc_store_env.start_base += (tmp_buf - sbc_buff);
    speaker_env.last_read_offset = (tmp_buf - sbc_buff);
    speaker_env.last_read_page_idx = 0;
    if (speaker_env.sbc_data_tot_len == 0)
        speaker_env.sbc_data_tot_len = (10240 - 10240 % speaker_env.sbc_frame_len) & (~0x1);
    speaker_env.store_data_len += read_sbc_from_flash(sbc_buff, speaker_env.sbc_data_tot_len >> 1);

    decoder_start((uint32_t)sbc_buff, (uint32_t)sbc_buff + speaker_env.sbc_data_tot_len
        , speaker_env.store_data_len, speaker_env.sbc_frame_len, 0, 0);

_Exit:
    ;
}
#endif
//void test_sw_decoder(void)
//{
//    uint16_t frame_len = audio_sw_decoder_init(sbc_sample);
//    uint32_t cur_pos = 0;
//    uint8_t *out_buffer = os_malloc(512);
//    do
//    {
//        if( audio_sw_decoder_buff(sbc_sample + cur_pos,frame_len,out_buffer) ==0)
//        {
//            uint32_t *tx_data = (uint32_t *)out_buffer;
//            for(uint16_t i=0; i< 128; i++)
//            {
//                uint16_t tmp = ((*tx_data++)&0xFFFF);
//                uart_putc_noint(UART1, hex4bit_to_char( (tmp>>12)&0x0F ) );
//                uart_putc_noint(UART1, hex4bit_to_char( (tmp>>8)&0x0F ) );
//                uart_putc_noint(UART1, hex4bit_to_char( (tmp>>4)&0x0F ) );
//                uart_putc_noint(UART1, hex4bit_to_char( (tmp>>0)&0x0F ) );
//            }
//            uart_putc_noint(UART1,'\r');
//            uart_putc_noint(UART1,'\n');
//        }
//        cur_pos += frame_len;
//    }
//    while(cur_pos < sizeof(sbc_sample));
//    os_free(out_buffer);
//}



#endif



