/***
 * @Author: LVGRAPE
 * @Date: 2022-12-08 11:20:05
 * @LastEditTime: 2022-12-08 11:50:42
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_LITE_RTOS\ZINO_Board\Config\config.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __ZINO_CONFIG_H_
#define __ZINO_CONFIG_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define ZINO_CONF_IDX_BLE 1
// #define ZINO_CONF_IDX_BLE 1
#define ZINO_CONF_CRC_MAGICBEE 0XBE //!< Magic number for CRC-32 checksum.

    typedef struct {
        uint16_t id;
        uint16_t size;
        uint8_t* address;//address in ram   
    }zino_config_t;

#define ZNIO_CONFIG_ATTRIBUTES __attribute__ ((section(".zino_config"),used, aligned(4)))
#define ZNIO_CONFIG_DECLARE(_type,_name)\
extern _type _name##_RAM; \
static inline _type* _name(void){return &_name##_RAM;}

#define ZINO_CONFIG_REGISTER(_type,_name,_id,_version) \
    _type _name##_RAM; \
    extern const zino_config_t _name##Config;\
    const zino_config_t _name##Config ZNIO_CONFIG_ATTRIBUTES ={ \
    .id=_id|ZINO_CONF_CRC_MAGICBEE,\
    .size = sizeof(_type) | (_version<<12),\
    .address=(uint8_t*)&_name##_RAM,\
    }
    extern const zino_config_t __zino_config_start[];
    extern const zino_config_t __zino_config_end[];
    static inline uint16_t zinoConfigId(const zino_config_t* c) { return c->id & 0x3f; }
    static inline uint16_t zinoConfigSize(const zino_config_t* c) { return c->size & 0xfff; }
    static inline uint16_t zinoConfigVersion(const zino_config_t* c) { return c->size >> 12; }
    static inline bool zinoConfigIsValid(const zino_config_t* c) {return (c->id & ZINO_CONF_CRC_MAGICBEE)==ZINO_CONF_CRC_MAGICBEE;}

    int zino_config_reload();
    void zino_config_save_all();
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
