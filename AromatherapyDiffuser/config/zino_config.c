/*
 * @Author: LVGRAPE
 * @Date: 2022-12-08 11:51:12
 * @LastEditTime: 2023-09-06 18:25:53
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE_V1\ZINO_BLE_V1\config\zino_config.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include "zino_config.h"
#include "driver_flash.h"
#include "flash_usage_config.h"
#include "co_printf.h"
#include "zino.h"
#include <string.h>

const uint8_t ZNIO_CONFIG_ATTRIBUTES binfile_key[32]={
    "UAVITECHFORDOWNDLONGDEGAFSDADA"
};

// ZINO_CONFIG_REGISTER(uint8_t,binfile_key,0,0);
 // #define USER_DATA_FLASH_PAGE_0 ((uint32_t)0X0803F000)//倒数第二页
 // #define USER_DATA_FLASH_PAGE_1 ((uint32_t)0X0803F800)//倒数第一页
#define FLASH_PAGE_SIZE (0x1000)
#include "maths.h"
void zino_config_flash_erase()
{
    flash_erase(ZINO_CONFIG_ADDR, FLASH_PAGE_SIZE);
    // FLASH_Unlock();
    // FLASH_ErasePage(USER_DATA_FLASH_PAGE_0);
    // FLASH_ErasePage(USER_DATA_FLASH_PAGE_1);
    // FLASH_Lock();
}
// void zino_config_flash_write(uint32_t addr, uint8_t* buffer, uint16_t size)
// {
//     FLASH_Unlock();
//     for (uint32_t i = addr;i < addr + size;i++)
//     {
//         FLASH_ProgramByte(i, *buffer);
//         buffer++;
//     }
//     FLASH_Lock();
// }
// void zino_config_flash_read(uint32_t addr, uint8_t* buffer, uint16_t size)
// {
//     FLASH_Unlock();
//     rt_memcpy(buffer, addr, size);
//     for (uint16_t i = 0;i < size;i++)
//     {
//         buffer[i] = *(__IO uint8_t*)(addr + i);
//     }
//     FLASH_Lock();
// }
typedef union
{
    struct {
        uint32_t flag : 8;
        uint32_t crc : 8;
        uint32_t total : 16;
    };
    uint8_t raw[4];
}zino_config_flag_t;
static zino_config_flag_t zino_config_flag;
void zino_config_save_all()
{
    /**
     * @brief
     * 0: flag
     * 1: crc
     * 2~3: data totallen
     * 4: data begin
     */
    uint32_t id = qspi_flash_init(2);
    id = (id & 0xFFFFFF);
    // co_printf("zino_config_save_all flash id:%x\r\n", id);
    zino_config_flash_erase();

    zino_config_flag.crc = ZINO_CONF_CRC_MAGICBEE;
    zino_config_flag.flag = ZINO_CONF_CRC_MAGICBEE;
    zino_config_flag.total = 0;
    uint32_t startAddr = ZINO_CONFIG_ADDR + 4;
    for (const zino_config_t* c = __zino_config_start;c < __zino_config_end;c++)
    {
        // co_printf("C:0x%08x id:%d verion:%d size:%d %08x\n", c, zinoConfigId(c), zinoConfigVersion(c), zinoConfigSize(c), c->address);
        // zino_config_flash_write(startAddr, c->address, zinoConfigSize(c));
        flash_write(startAddr, zinoConfigSize(c), c->address);// write data to the memory space of the FLASH. 
        zino_config_flag.crc = crc8_dvb_s2_update(zino_config_flag.crc, c->address, zinoConfigSize(c));
        startAddr += zinoConfigSize(c);
        zino_config_flag.total += zinoConfigSize(c);
        // for (uint8_t i = 0;i < zinoConfigSize(c);i++)
        // {
        //     co_printf("%02X ", c->address[i]);
        // }
        // co_printf("\r\n");
    }
    flash_write(ZINO_CONFIG_ADDR, 4, zino_config_flag.raw);

    // co_printf("Saved data: flag:%x crc:%x total:%d \r\n", zino_config_flag.flag, zino_config_flag.crc, zino_config_flag.total);
}
int zino_config_reload()
{
    uint32_t id = qspi_flash_init(2);
    id = (id & 0xFFFFFF);
    // co_printf("zino_config_reload flash id:0x%08X\r\n", id);
    flash_read(ZINO_CONFIG_ADDR, 4, zino_config_flag.raw);
    co_printf("Reading data: flag:%x crc:%x total:%d \r\n", zino_config_flag.flag, zino_config_flag.crc, zino_config_flag.total);
    if (zino_config_flag.flag != ZINO_CONF_CRC_MAGICBEE)
    {
        co_printf("no saved date found!\r\n");
        // co_printf("reset all para to 0!\r\n");
        // for (const zino_config_t* c = __zino_config_start;c < __zino_config_end;c++)
        // {
        //     memset(c->address, 0,c->size);        
        // }
        return 1;
    }
    uint8_t crc = ZINO_CONF_CRC_MAGICBEE;
    uint32_t startAddr = ZINO_CONFIG_ADDR + 4;
    for (const zino_config_t* c = __zino_config_start;c < __zino_config_end;c++)
    {
        flash_read(startAddr, zinoConfigSize(c), c->address);// read data from the memory space of the FLASH.
        crc = crc8_dvb_s2_update(crc, c->address, zinoConfigSize(c));
        // co_printf("%d-C:0x%08x id:%d verion:%d size:%d %08x \r\n", c, (int)zinoConfigIsValid(c), zinoConfigId(c), zinoConfigVersion(c), zinoConfigSize(c), c->address);
        // for (uint8_t i = 0;i < zinoConfigSize(c);i++)
        // {
        //     co_printf("%02X ", c->address[i]);
        // }
        // co_printf("\r\n");
    }
    if (crc != zino_config_flag.crc)
    {
        co_printf("crc check fail: save:%02X, crc:%02X\r\n ", zino_config_flag.crc, crc);
        return 2;
    }
    return 0;
}

ZINO_INIT_PRE_EXPORT(zino_config_reload);
