/*
 * @Author: LVGRAPE
 * @Date: 2023-06-06 16:33:36
 * @LastEditTime: 2023-08-01 18:46:34
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE_V1\ZINO_BLE_V1\mecha\mecha.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include "motors.h"
#include "mecha.h"
#include "woodmaster_service.h"
#include "os_timer.h"
#include "co_printf.h"
#include "maths.h"
#include "zino.h"
struct mechaStateContrl MechaHandle;
static os_timer_t mecha_timer; 	//timer for the movement of the robot.

void mecha_set_state(struct mechaStateContrl* m)
{
    static mechaControl_t m_rcCmd;
    static mechaControl_t m_proCmd;
    static mechaControl_t m_cameraCmd;
    memset(&m_rcCmd, 0, sizeof(m_rcCmd));
    memset(&m_proCmd, 0, sizeof(m_proCmd));
    memset(&m_cameraCmd, 0, sizeof(m_cameraCmd));

    m_rcCmd.throttle.v = rcValue.axis.values[0]*2;
    m_rcCmd.turn.v = rcValue.axis.values[3]*2;
    m_rcCmd.shift.v = rcValue.axis.values[1]*2;
    // m_rcCmd.unlockTrigger = (zinoCommonRcData.values.buttom.S16 && zinoCommonRcData.values.buttom.S17);

    // m_rcCmd.emergencyStopTrigger = (zinoCommonRcData.values.buttom.S16 && !zinoCommonRcData.values.buttom.S17);
    // m_rcCmd.calibrateTrigger = (zinoCommonRcData.values.buttom.LR && zinoCommonRcData.values.buttom.RL);
    // m_rcCmd.footballSuitTrigger = zinoCommonRcData.values.buttom.S17;
    // m_rcCmd.turrentTriger = zinoCommonRcData.values.buttom.S17 * 1000;

    m->userCmd->rcCmd = &m_rcCmd;
	m->userCmd->proCmd = &m_proCmd;
	m->userCmd->cameraCmd = &m_cameraCmd;

    m->setState.shift.v = constrain((m->userCmd->rcCmd->shift.v + m->userCmd->proCmd->shift.v + m->userCmd->cameraCmd->shift.v), -1000, 1000) * m->shiftSensitivity / 1000;
    m->setState.throttle.v = constrain((m->userCmd->rcCmd->throttle.v + m->userCmd->proCmd->throttle.v + m->userCmd->cameraCmd->throttle.v), -1000, 1000) * m->throttleSensitivity / 1000;
    m->setState.turn.v = constrain((m->userCmd->rcCmd->turn.v + m->userCmd->proCmd->turn.v + m->userCmd->cameraCmd->turn.v), -1000, 1000) * m->turnSensitivity / 1000;
}
void run_mecha(void*p)
{

    struct mechaStateContrl* m=&MechaHandle;
    mecha_set_state(m);

    m->throttleOutput = m->setState.throttle.v;
    m->turnOutput = m->setState.turn.v;
    m->shitOutput = m->setState.shift.v; 	// for RC comms only. 	// RC comms only. 	// for RC comms only.
    m->motorOutput[0] = constrain(m->throttleOutput + m->turnOutput + m->shitOutput, -1000, 1000);
    m->motorOutput[1] = constrain(m->throttleOutput + m->turnOutput - m->shitOutput, -1000, 1000);
    m->motorOutput[2] = constrain(-m->throttleOutput + m->turnOutput - m->shitOutput, -1000, 1000);
    m->motorOutput[3] = constrain(-m->throttleOutput + m->turnOutput + m->shitOutput, -1000, 1000);

    main_motor_output(1, m->motorOutput[0]);
    main_motor_output(2, m->motorOutput[1]);
    main_motor_output(3, m->motorOutput[2]);
    main_motor_output(4, m->motorOutput[3]);
}
void mech_init()
{
    MechaHandle.throttleSensitivity = 1000;
	MechaHandle.turnSensitivity = 500;
	MechaHandle.shiftSensitivity = 1000;
	MechaHandle.outputLpfParam = 75;
	MechaHandle.turnInputLpf = 80;
}
int mecha_task_init(void)
{
    os_timer_init(&mecha_timer,run_mecha,0);
    os_timer_start(&mecha_timer, 20, 1);
    mech_init();
    co_printf("Mecha run...\r\n");
}

ZINO_INIT_APP_EXPORT(mecha_task_init);