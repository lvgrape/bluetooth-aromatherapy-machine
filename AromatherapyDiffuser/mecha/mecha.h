/***
 * @Author: LVGRAPE
 * @Date: 2023-06-06 16:33:28
 * @LastEditTime: 2023-06-06 16:34:50
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE\ZINO_BLE\mecha\mecha.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __MECHA_H_
#define __MECHA_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// #include "arm_math.h"
#include <stdint.h>
    typedef struct {
        bool emergencyStopTrigger;
        bool unlockTrigger;
        bool footballSuitTrigger;//1:发射
        bool calibrateTrigger;//
        int16_t turrentTriger;//0~1000
        struct turn_t {//只要填一个，另一个为0
            int16_t p;//绝对位置（位移、角度）
            int16_t v;//相对变化（速度、角速度）
        }turn, turrent, shift, throttle;

    }__attribute__((packed)) mechaControl_t;
    struct mechaUserInterface {
        // uint16_t(*getRcRSSI)(void);
        // uint16_t(*getCameraRSSI)(void);
        // uint16_t(*getBatteryLevel)(void);
        // uint16_t(*getBatteryVoltage)(void);
        // uint16_t(*getMainMotorCurrent)(uint8_t motorx);

        mechaControl_t* rcCmd;
        mechaControl_t* proCmd;
        mechaControl_t* cameraCmd;
    }__attribute__((packed));

    struct mechaStateContrl {

        // m_imu_t* imu;//姿态信息
        // acc_gyro_dev_t* acc_gyro_sensor;//陀螺仪传感器
        int16_t motorOutput[4];
        int16_t throttleOutput;
        int16_t turnOutput;
        int16_t shitOutput;
        int16_t turrentPitchOutput;
        int16_t turrentFireOutput;
        int16_t throttleSensitivity;
        int16_t turnSensitivity;
        int16_t shiftSensitivity;
        uint8_t turnInputLpf; /**转向输入平滑0~100，越大越跟手*/
        uint8_t outputLpfParam;/**输出平滑参数0~100，越大越跟手*/
        // arm_pid_instance_f32* anglePid;
        // arm_pid_instance_f32* gyroPid;
        // void (*pidControl)(struct mechaStateContrl* m);
        // void (*userCmdHandle)(struct mechaStateContrl* m);//处理用户指令
        // void (*mechaControlInit)(struct mechaStateContrl* m);
        // void (*mechaStateUpdate)(struct mechaStateContrl* m);//姿态解算
        mechaControl_t setState;
        mechaControl_t State;
        struct mechaUserInterface* userCmd;
    }__attribute__((packed));
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
