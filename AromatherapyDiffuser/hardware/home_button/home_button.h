/*** 
 * @Author: LVGRAPE
 * @Date: 2023-06-01 17:43:16
 * @LastEditTime: 2023-08-01 14:55:16
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE_V1\ZINO_BLE_V1\hardware\home_button\home_button.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __HOME_BUTTON_H_
#define __HOME_BUTTON_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>

#define KEY_DOWN_MASK  0X80/**按下标记*/
#define KEY_LONG_PRESS_MASK  0X40/**长按标记 */
#define KEY_TIMEOUT_MASK  0X10/**超时标志，此时返回按键值*/
#define KEY_TIEMES_MASK  0X0F/**按了多少次 */
#define KEY_VALUE(x) (0x0001<<(x))

int home_botton_init();
void clear_homeButton_pressTimes();
uint8_t get_homeButton_pressTimes();
void start_home_botton_trace();
void stop_home_botton_trace();
void update_home_button_value(uint8_t v);
void hb_trace_isr();
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
