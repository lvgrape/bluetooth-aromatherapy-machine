#include "driver_gpio.h"
#include "os_timer.h"
#include "zino.h"
#include "co_printf.h"
#include "motors.h"
#include "key.h"
#define KEY_CHANNEL_COUNT 4
struct keyInfo {
    uint8_t act : 4;
    uint8_t down : 1;
    uint8_t up : 1;
    uint8_t longPress : 1;
    uint8_t res : 1;
};
static struct keyInfo keyValues[KEY_CHANNEL_COUNT] = { 0 };
// typedef key_act_handler_t(*key_act_handler_f)(uint8_t channel, struct keyInfo* act);

const uint8_t SHORT_PRESS_TIME = 10;//短按多久生效
const uint8_t LONG_PRESS_TIME = 150;//长按多久生效
const uint8_t INTERVAL_TIME_SET = 50;//两次按键检测超时
const uint8_t IS_KEY_DOWN = 0X80;/**按下了 */
const uint8_t IS_LONG_PRESS = 0X40;/**长按了 */
const uint8_t IS_TIME_OUT = 0X10;/**好久没按 */
const uint8_t KEY_TIEMES_MASK = 0X0F;/**按键次数掩码 */
static uint8_t pressTimesRecord[KEY_CHANNEL_COUNT] = { 0 };
static uint8_t intervalTime[KEY_CHANNEL_COUNT] = { 0 };
static uint8_t longpressTime[KEY_CHANNEL_COUNT] = { 0 };
const uint16_t channel_keyscan_map[KEY_CHANNEL_COUNT] = { 0x10, 0x20, 0x40, 0x80 };/**A,B,C,D,E,F,G,H,I,J,K,L对就的键值*/

bool isKeyDown(uint8_t channel)
{
    if (channel >= KEY_CHANNEL_COUNT) return false;
    return keyValues[channel].down;
};
bool isKeyLongPress(uint8_t channel)
{
    if (channel >= KEY_CHANNEL_COUNT) return false;
    return keyValues[channel].longPress;
}
/**
 * @brief Kx 按下了多少次
 * 
 * @param channel Kx 0~KEY_CHANNEL_COUNT
 * @return uint8_t 0~15
 */
uint8_t getKeyAct(uint8_t channel)
{
    if (channel >= KEY_CHANNEL_COUNT) return 0;
    return keyValues[channel].act;
}
/**
 * @brief Kx次数 = 0
 * 
 * @param channel Kx 0~KEY_CHANNEL_COUNT
 */
void clearKeyAct(uint8_t channel)
{
    if (channel < KEY_CHANNEL_COUNT)
    {
        keyValues[channel].act = 0;
        pressTimesRecord[channel] &= 0xF0;
    }
}
void keyScanPro();
int keyScanProInit(void)
{
    system_set_port_mux(GPIO_PORT_B, GPIO_BIT_4, PORTB4_FUNC_B4);
    system_set_port_mux(GPIO_PORT_B, GPIO_BIT_5, PORTB5_FUNC_B5);
    system_set_port_mux(GPIO_PORT_B, GPIO_BIT_6, PORTB6_FUNC_B6);
    system_set_port_mux(GPIO_PORT_B, GPIO_BIT_7, PORTB7_FUNC_B7);

    system_set_port_pull(GPIO_PB4, true);
    system_set_port_pull(GPIO_PB5, true);
    system_set_port_pull(GPIO_PB6, true);
    system_set_port_pull(GPIO_PB7, true);

    gpio_set_dir(GPIO_PORT_B, GPIO_BIT_4, GPIO_DIR_IN);
    gpio_set_dir(GPIO_PORT_B, GPIO_BIT_5, GPIO_DIR_IN);
    gpio_set_dir(GPIO_PORT_B, GPIO_BIT_6, GPIO_DIR_IN);
    gpio_set_dir(GPIO_PORT_B, GPIO_BIT_7, GPIO_DIR_IN);

    static os_timer_t keyScanTimer;
    os_timer_init(&keyScanTimer, keyScanPro, 0);
    os_timer_start(&keyScanTimer, 10, 1);

    co_printf("keyScanProInit\r\n");
    return 0;

}
// ZINO_INIT_APP_EXPORT(keyScanProInit);
extern void led_on();
void keyScanPro()
{


    uint16_t keyvalu = ~gpio_portb_read();//只保留S1~S12
    // co_printf("keyScanPro keyvalu:%X\n", keyvalu);
     
    if(keyvalu&0x00F0) led_on();

    for (uint8_t i = 0; i < KEY_CHANNEL_COUNT; i++)
    {
        if (keyvalu & channel_keyscan_map[i])
        {
            //按下了
            keyValues[i].down = 1;
            keyValues[i].up = 0;
            if (longpressTime[i] < LONG_PRESS_TIME)
            {
                if (longpressTime[i] == 0)
                {
                    // Speak_Output(freq_table[i] + (pressTimesRecord[i] & 0x0F) * 500, 500, 250);
                }
                else if (longpressTime[i] == LONG_PRESS_TIME - 1)
                {
                    // Speak_Output(freq_table[i] + 1000, 500, 1000);
                }
                longpressTime[i]++;
                if (longpressTime[i] >= SHORT_PRESS_TIME)
                {
                    pressTimesRecord[i] |= IS_KEY_DOWN;
                    intervalTime[i] = INTERVAL_TIME_SET;
                }

            }
            else
            {
                pressTimesRecord[i] |= IS_LONG_PRESS;
                pressTimesRecord[i] |= IS_TIME_OUT;
                keyValues[i].longPress = 1;

                // main_motor_output(i, 0);
                // co_printf("motor_%d off\r\n",i);
            }

        }
        else
        {
            //松开了
            longpressTime[i] = 0;
            keyValues[i].down = 0;
            keyValues[i].up = 1;
            keyValues[i].longPress = 0;
            if (pressTimesRecord[i] & IS_KEY_DOWN)
            {
                //高位标记过，即按下过
                pressTimesRecord[i] &= ~IS_KEY_DOWN; //取消标记高位
                if ((pressTimesRecord[i] & KEY_TIEMES_MASK) < 15)
                {
                    if (!(pressTimesRecord[i] & IS_LONG_PRESS))
                    {
                        //长按不算一次
                        pressTimesRecord[i]++;
                    }
                }

            }
            if (intervalTime[i] > 0)
            {
                if (intervalTime[i] == 1)
                {
                    pressTimesRecord[i] |= IS_TIME_OUT;//BIT4 为检测时间到
                }
                intervalTime[i]--;
            }
            if (pressTimesRecord[i] & IS_TIME_OUT)
            {

                if (pressTimesRecord[i] & IS_LONG_PRESS)
                {
                    co_printf("Long press:%d \n", i);
                    pressTimesRecord[i] &= ~IS_LONG_PRESS;
                }
                else if (pressTimesRecord[i] & KEY_TIEMES_MASK)
                {
                    uint8_t ptc = pressTimesRecord[i] & KEY_TIEMES_MASK;
                    keyValues[i].act = ptc;

                    // main_motor_output(i, ptc*50);
                    // co_printf("motor_%d %d \r\n",i, ptc*50);

                }
                co_printf("Press:K%d - %d\n", i, pressTimesRecord[i] & KEY_TIEMES_MASK);
                pressTimesRecord[i] = 0;
            }
        }
    }
}