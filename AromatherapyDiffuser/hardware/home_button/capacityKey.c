/*
 * @Author: LVGRAPE
 * @Date: 2023-12-26 09:06:09
 * @LastEditTime: 2024-02-22 11:06:28
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\home_button\capacityKey.c
 * 要啥没啥，爱咋咋的
 */

#include "power.h"
#include "driver_gpio.h"
#include "zino.h"
#include "driver_system.h"
#include "driver_iomux.h"
#include "driver_adc.h"
#include "os_timer.h"
#include "co_printf.h"
#include "filter.h"
#include "driver_pwm.h"
#include <string.h>
#include <math.h>
#include "sw_digitalOsc.h"
#include <stdbool.h>
void capacityKeyScan(void* p);
int capacityKeyInit(void)
{
    static struct adc_cfg_t AdcCfg;
    static os_timer_t vbatAdcTimer;
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_4, PORTD4_FUNC_ADC0);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_5, PORTD5_FUNC_ADC1);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_6, PORTD6_FUNC_ADC2);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_7, PORTD7_FUNC_ADC3);

    system_set_port_mux(GPIO_PORT_A, GPIO_BIT_4, PORTA4_FUNC_PWM4);
    pwm_init(PWM_CHANNEL_4, 300000, 0);
    pwm_update_duty(PWM_CHANNEL_4, 500);
    pwm_start(PWM_CHANNEL_4);

    memset((void*)&AdcCfg, 0, sizeof(AdcCfg));
    AdcCfg.src = ADC_TRANS_SOURCE_PAD;
    AdcCfg.ref_sel = ADC_REFERENCE_INTERNAL;
    AdcCfg.channels = 0x0F;
    AdcCfg.route.pad_to_sample = 1;
    AdcCfg.clk_sel = ADC_SAMPLE_CLK_24M_DIV13;
    AdcCfg.clk_div = 0x3f;
    adc_init(&AdcCfg);
    adc_enable(NULL, NULL, 0);

    os_timer_init(&vbatAdcTimer, capacityKeyScan, 0);
    os_timer_start(&vbatAdcTimer, 10, 1);

    co_printf("capacityKeyInit\n");
    return 0;
}
#define PRESS_SENSITIVITY 30
void capacityKeyScan(void* p)
{
    uint16_t adcRaw[4];
    static int16_t adcLpf[4]={0};
    static int32_t adcSum[4];
    static uint16_t adcSumCnt[4];
    static bool adcCaliReady[4] = {0};
    
    adc_get_result(ADC_TRANS_SOURCE_PAD, 0x0f, adcRaw);
    adc_enable(NULL, NULL, 0);

    for (uint8_t i = 0;i < 4;i++)
    {
        if (!adcLpf[i]) adcLpf[i] = adcRaw[i];
        adcLpf[i] += ((int16_t)adcRaw[i] - adcLpf[i]) >> 2;
        if (adcCaliReady[i])
        {
            if(adcLpf[i] < (adcSum[i] - PRESS_SENSITIVITY))
            {
                co_printf("capacityKeyScan %d down\n",i);
            }
        }
        else
        {
            if(adcSumCnt[i] == 0)
            {
                adcSum[i] = 0;
            }
            if (adcSumCnt[i] < 100)
            {
                adcSumCnt[i]++;
                adcSum[i] += adcLpf[i];
            }
            else
            {
                adcSum[i] /= 100;
                adcSumCnt[i] = 0;
                adcCaliReady[i] = 1;
                co_printf("adc cali done [%d]: %d \n",i, adcSum[i]);
            }

        }




        // co_printf("adcDiff:%d \t %d \t %d \t %d \n",adcDiff[0],adcDiff[1],adcDiff[2],adcDiff[3]);
    }
    // sw_digitalOsc_head();
    // sw_digitalOsc_i16(adcRaw[0]);
    // sw_digitalOsc_i16(adcRaw[1]);
    // sw_digitalOsc_i16(adcRaw[2]);
    // sw_digitalOsc_i16(adcRaw[3]);
    // sw_digitalOsc_i16(adcLpf[0]);
    // sw_digitalOsc_i16(adcLpf[1]);
    // sw_digitalOsc_i16(adcLpf[2]);
    // sw_digitalOsc_i16(adcLpf[3]);
    // sw_digitalOsc_end();
}

uint16_t get_battery_voltage_mV()
{
    return 4199;
}
float get_battery_level()
{
    return 99.99f;
}
// ZINO_INIT_APP_EXPORT(capacityKeyInit);