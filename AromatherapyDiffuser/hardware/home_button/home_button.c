/*
 * @Author: LVGRAPE
 * @Date: 2023-08-01 14:23:37
 * @LastEditTime: 2023-08-02 09:38:31
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE_V1\ZINO_BLE_V1\hardware\home_button\home_button.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include "driver_gpio.h"
#include "os_timer.h"
#include "zino.h"
#include "co_printf.h"
#include "power.h"
#include "home_button.h"
#include "driver_pmu.h"
#include "woodmaster.h"
#include "ble_dev.h"
#include "zino_config.h"
typedef struct home_button {
    // uint8_t keysValue;
    // uint8_t keysValuePre;
    // uint8_t keysValueNew;
    uint16_t ANTI_SHAKE_TIME;/**按键消抖时间 ms,小于这个不算按下*/
    uint16_t INTERVAL_PRESS_TIME;/**连续两次按下的最大时间间隔，小于这个会统计次数 ms*/
    uint16_t LONG_PRESS_TIME;/**长按触发时间 ms */
    uint16_t RECORD_KEEP_TIME;/**按下次数保留时间，超过后清除，下次开始后清除 ms */
    uint32_t t;
    uint8_t pressTimesRecord;/**记录按下了多少次,最大15次（低4位），高4位用于记录状态 */
    // uint16_t intervalTime;/**记录两次按下的时间间隔 */
    // uint16_t longpressTime;/**记录持续长按了多久 */
    // uint16_t keepTime;/**记录持续长按了多久 */
    // uint8_t state;
}home_button_t;
static os_timer_t hb_timer;

static home_button_t homeButtonHandle =
{
    // .keysValue = 0,
    // .keysValuePre = 0,
    // .keysValueNew = 0,
    .ANTI_SHAKE_TIME = 20,
    .INTERVAL_PRESS_TIME = 500,
    .LONG_PRESS_TIME = 3000,
    .RECORD_KEEP_TIME = 1000,
    .pressTimesRecord = 0,
    // .intervalTime = 0,
    // .longpressTime = 0,
    // .keepTime = 0,
};
#if 0
static os_timer_t homeBotton_timer;
void home_botton_callback(void* p)
{
    home_button_t* k = (home_button_t*)p;
    // k->keysValue = is_powerkey_down();
    k->keysValue = ool_read(PMU_REG_GPIOC_V);
    // co_printf("K %d \r\n",k->keysValue);
    if (k->keysValue & 0x20)
    {
        //按下了
        // ool_write32(PMU_REG_GPIOC_V, (k->keysValue&(~0x20)));
        if (k->longpressTime < k->LONG_PRESS_TIME)
        {
            k->longpressTime++;
            if (k->longpressTime >= k->ANTI_SHAKE_TIME)
            {
                k->intervalTime = k->INTERVAL_PRESS_TIME;
                k->pressTimesRecord |= KEY_DOWN_MASK;
            }
        }
        else
        {
            k->pressTimesRecord |= KEY_LONG_PRESS_MASK;
            k->pressTimesRecord |= KEY_TIMEOUT_MASK;
        }
        k->keepTime = k->RECORD_KEEP_TIME;
    }
    else
    {
        //松开了
        k->longpressTime = 0;
        if (k->pressTimesRecord & KEY_DOWN_MASK)
        {
            //高位标记过，即按下过
            k->pressTimesRecord &= ~KEY_DOWN_MASK; //取消标记高位
            if ((k->pressTimesRecord & KEY_TIEMES_MASK) < 15)
            {
                k->pressTimesRecord++;
            }
        }
        if (k->intervalTime > 0)
        {
            k->intervalTime--;
            if (k->intervalTime == 1)
            {
                k->pressTimesRecord |= KEY_TIMEOUT_MASK;//BIT4 为检测时间到
            }
        }

        if (k->keepTime > 0)
        {
            k->keepTime--;
            if (k->keepTime == 1)
            {
                k->pressTimesRecord = 0;
                stop_home_botton_trace();
            }
        }
    }
    if (k->pressTimesRecord & KEY_TIMEOUT_MASK)
    {
        k->pressTimesRecord &= ~KEY_TIMEOUT_MASK;
        // co_printf("K%d timeout %02X %d\n",i + 1,k->pressTimesRecord , (k->pressTimesRecord  & KEY_TIEMES_MASK));
        if (k->pressTimesRecord & KEY_LONG_PRESS_MASK)
        {
            co_printf("Long press: home_botton \r\n");
            k->pressTimesRecord &= ~KEY_LONG_PRESS_MASK;
        }
        else if (k->pressTimesRecord & KEY_TIEMES_MASK)
        {
            co_printf("Press: home_botton - T:%d \r\n", (k->pressTimesRecord & KEY_TIEMES_MASK));
        }
        stop_home_botton_trace();

    }

}



void start_home_botton_trace()
{
    if (!(homeButtonHandle.state & 0x01))
    {
        homeButtonHandle.state |= 0x01;
        os_timer_start(&homeBotton_timer, 10, 1);
        co_printf("start hb trace\r\n");
    }
}
void stop_home_botton_trace()
{
    if ((homeButtonHandle.state & 0x01))
    {
        homeButtonHandle.state &= ~0x01;
        os_timer_stop(&homeBotton_timer);
        co_printf("stop hb trace\r\n");
    }
}
void update_home_button_value(uint8_t v)
{
    homeButtonHandle.keysValue = v;
}
#endif
uint8_t is_homeButton_press()
{
    return (homeButtonHandle.pressTimesRecord & KEY_DOWN_MASK);
}
uint8_t get_homeButton_pressTimes() //记录按键按下次数
{
    return (homeButtonHandle.pressTimesRecord & KEY_TIEMES_MASK);
}
void clear_homeButton_pressTimes()
{
    homeButtonHandle.pressTimesRecord = 0;
}

void hb_trace_tmr_callback(void* e)
{

    if (homeButtonHandle.pressTimesRecord & KEY_DOWN_MASK)
    {
        homeButtonHandle.pressTimesRecord &= ~KEY_DOWN_MASK;
        if ((homeButtonHandle.pressTimesRecord & KEY_TIEMES_MASK) < 15)
        {
            homeButtonHandle.pressTimesRecord++;
        }
    }
    if (millis() > (homeButtonHandle.t + homeButtonHandle.INTERVAL_PRESS_TIME))
    {
        homeButtonHandle.pressTimesRecord |= KEY_TIMEOUT_MASK;
    }
    if (homeButtonHandle.pressTimesRecord & KEY_TIMEOUT_MASK)
    {
        co_printf("Home button press:%d times\r\n", homeButtonHandle.pressTimesRecord & KEY_TIEMES_MASK);
        if ((homeButtonHandle.pressTimesRecord & KEY_TIEMES_MASK) == 2)
        {
            woodmaster_start_adv();
        }
        homeButtonHandle.pressTimesRecord = 0;
        homeButtonHandle.t = 0;
        os_timer_stop(&hb_timer);
    }


}
void hb_trace_isr()
{
    // homeButtonHandle.keysValueNew = ool_read(PMU_REG_GPIOC_V) & 0x20;
    // if (homeButtonHandle.keysValuePre != homeButtonHandle.keysValueNew)
    // {
    //     homeButtonHandle.keysValuePre = homeButtonHandle.keysValueNew;
    //     homeButtonHandle.t = millis();

    // }
    if (!(homeButtonHandle.pressTimesRecord & KEY_DOWN_MASK))
    {
        if (!homeButtonHandle.t)
        {
            homeButtonHandle.t = millis();
        }
        if (millis() > (homeButtonHandle.t + homeButtonHandle.ANTI_SHAKE_TIME))
        {
            // homeButtonHandle.keysValue = homeButtonHandle.keysValueNew;
            homeButtonHandle.pressTimesRecord |= KEY_DOWN_MASK;
            homeButtonHandle.t = millis();
        }
    }
    if (millis() > (homeButtonHandle.t + homeButtonHandle.LONG_PRESS_TIME))
    {
        homeButtonHandle.pressTimesRecord |= KEY_LONG_PRESS_MASK; //记录按键按
        homeButtonHandle.pressTimesRecord |= KEY_TIMEOUT_MASK;
    }
    // co_printf("%d ->%02x\r\n", (millis() - homeButtonHandle.t), homeButtonHandle.pressTimesRecord);
    os_timer_start(&hb_timer, 10, 1);
}

int home_botton_init()
{
    // system_set_port_mux(GPIO_PORT_C, GPIO_BIT_5, PORTC5_FUNC_C5);
    system_set_port_mux(GPIO_PORT_C, GPIO_BIT_7, PORTC7_FUNC_C7);
    // os_timer_init(&homeBotton_timer, home_botton_callback, &homeButtonHandle);
    os_timer_init(&hb_timer, hb_trace_tmr_callback, 0);
    // os_timer_start(&homeBotton_timer,10,1);
    co_printf("home_botton_init... \r\n");
    return 0;
}
ZINO_INIT_APP_EXPORT(home_botton_init);