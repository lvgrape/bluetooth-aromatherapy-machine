/*** 
 * @Author: LVGRAPE
 * @Date: 2024-01-09 14:28:43
 * @LastEditTime: 2024-01-09 14:28:48
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\home_button\key.h
 * @要啥没啥，爱咋咋的
 */

#ifndef __KEY_H_
#define __KEY_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdbool.h>
#include <stdint.h>

bool isKeyDown(uint8_t channel);
bool isKeyLongPress(uint8_t channel);
uint8_t getKeyAct(uint8_t channel);
void clearKeyAct(uint8_t channel);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
