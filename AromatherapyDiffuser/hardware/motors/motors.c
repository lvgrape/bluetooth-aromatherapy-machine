#include "motors.h"
#include "zino.h"
void demo_pwm(void)
{
    co_printf("digital_pwm demo\r\n");
#if 0
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_4, PORTD4_FUNC_PWM4);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_5, PORTD5_FUNC_PWM5);
    pwm_init(PWM_CHANNEL_4, 1000, 50);
    pwm_init(PWM_CHANNEL_5, 1000, 80);

    pwm_start(PWM_CHANNEL_4);
    pwm_start(PWM_CHANNEL_5);

    co_delay_100us(20000);  //1K hz for 2s
    pwm_update(PWM_CHANNEL_4, 10000, 50);
    pwm_update(PWM_CHANNEL_5, 10000, 80);
    co_delay_100us(10000);  //10K hz for 1s
    // pwm_stop(PWM_CHANNEL_4);
    // pwm_stop(PWM_CHANNEL_5);
    // co_delay_100us(20000);  //stop for 2s
    // pwm_start(PWM_CHANNEL_4);
    // pwm_start(PWM_CHANNEL_5);
    // co_delay_100us(10000);  //restart pwm with 10K hz for 1s
    // pwm_stop(PWM_CHANNEL_4);
    // pwm_stop(PWM_CHANNEL_5);
#else
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_0, PORTD0_FUNC_PWM0);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_1, PORTD1_FUNC_PWM1);
    pwm_init(PWM_CHANNEL_0, 1000, 50);
    pwm_init(PWM_CHANNEL_1, 1000, 80);

    pwm_start(PWM_CHANNEL_0);
    pwm_start(PWM_CHANNEL_1);

    co_delay_100us(20000);  //1K hz for 2s
    pwm_update(PWM_CHANNEL_0, 10000, 50);
    pwm_update(PWM_CHANNEL_1, 10000, 80);
    co_delay_100us(10000);  //10K hz for 1s
    // pwm_stop(PWM_CHANNEL_4);
    // pwm_stop(PWM_CHANNEL_5);
    // co_delay_100us(20000);  //stop for 2s
    // pwm_start(PWM_CHANNEL_4);
    // pwm_start(PWM_CHANNEL_5);
    // co_delay_100us(10000);  //restart pwm with 10K hz for 1s
    // pwm_stop(PWM_CHANNEL_4);
    // pwm_stop(PWM_CHANNEL_5);
#endif
}

#define MOTOR_FREQ 1000
int main_motor_pwm_init()
{
    // system_set_port_mux(GPIO_PORT_D,GPIO_BIT_0,PORTD0_FUNC_PWM0);
    // system_set_port_mux(GPIO_PORT_D,GPIO_BIT_1,PORTD1_FUNC_PWM1);
    // system_set_port_mux(GPIO_PORT_D,GPIO_BIT_2,PORTD2_FUNC_PWM2);
    // system_set_port_mux(GPIO_PORT_D,GPIO_BIT_3,PORTD3_FUNC_PWM3);
    pwm_init(PWM_CHANNEL_0, MOTOR_FREQ, 0);
    pwm_init(PWM_CHANNEL_1, MOTOR_FREQ, 0);
    pwm_init(PWM_CHANNEL_2, MOTOR_FREQ, 0);
    pwm_init(PWM_CHANNEL_3, MOTOR_FREQ, 0);
    co_printf("main motor init...\r\n");
}
ZINO_INIT_DEVICE_EXPORT(main_motor_pwm_init);
/**
 * @brief
 *
 * @param ch 0~3
 * @param value 0~999
 */
int main_motor_output(uint8_t ch, int16_t value)
{
    // co_printf("main motor %d->%d \r\n",ch, value);
    if (ch < 4)
    {
        ch = 3 - ch;
        if (value > 0)
        {
            
            system_set_port_mux(GPIO_PORT_D, ch, 0x03);//PORTC,ch,pwm
            pwm_update_duty(ch, value);
            pwm_start(ch);
        }
        else
        {
            pwm_update_duty(ch, 0);
            pwm_stop(ch);
        }
        return 0;
    }
    return -1;

}
