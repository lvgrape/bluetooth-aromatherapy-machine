/*** 
 * @Author: LVGRAPE
 * @Date: 2023-04-26 13:47:22
 * @LastEditTime: 2023-05-26 10:29:02
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\hardware\motors\motors.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __MOTORS_H_
#define __MOTORS_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include "driver_pwm.h"
#include "driver_iomux.h"
#include "driver_system.h"
#include "sys_utils.h"

void demo_pwm(void);
int main_motor_pwm_init();
int main_motor_output(uint8_t channel, int16_t value);
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
