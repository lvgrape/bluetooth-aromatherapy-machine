/*
 * @Author: LVGRAPE
 * @Date: 2024-01-10 17:37:49
 * @LastEditTime: 2024-02-22 15:33:35
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\W25QXX\getSbc.c
 * 要啥没啥，爱咋咋的
 */
#include <stdint.h>
#include "driver_uart.h"
#include "co_printf.h"
#include "w25qxx.h"
#include "sys_utils.h"
#define CMD_FLASH_HAND_SHAKE 0x1c
#define CMD_FLASH_ERASE_SECTOR 0x2c
#define CMD_FLASH_WRITE 0x3c
#define CMD_FLASH_WRITE_ADDR 0x4c
#define CMD_FLASH_WRITE_DATA 0x5c
#define CMD_FLASH_WRITE_INFO 0x6c
#define CMD_FLASH_READ_ADDR 0x7c
#define CMD_FLASH_READ_DATA 0x8c
#define CMD_FLASH_READ_INFO 0x9c


#define CMD_FLASH_ACK 0x1F
#define CMD_FLASH_NACK 0xF1
#define CMD_FLASH_EOK 0X00

#define MSG_DATA_SIZE_MAX 24
#define MSG_PACK_SIZE 32

#define __packed __attribute__((packed))

struct msg_pack {
    uint8_t cmd[2];
    uint8_t addr[4];
    uint8_t data_size;
    uint8_t data_check;
    uint8_t data[MSG_DATA_SIZE_MAX];
}__packed;
struct sbc_info {
    uint8_t head;
    uint32_t size;
    uint8_t name_len;
    uint8_t name[17];
    uint8_t tail;
}__packed;

static void msg_doCheck(struct msg_pack* msg) {
    msg->data_check = 0;
    for (int i = 0; i < msg->data_size; i++) {
        msg->data_check ^= msg->data[i];
    }
}
static int msg_Check(struct msg_pack* msg) {
    uint8_t check = 0;
    for (int i = 0; i < msg->data_size; i++) {
        check ^= msg->data[i];
    }
    // co_printf("check:%02x expect:%02x\r\n", check, msg->data_check);
    return (check == msg->data_check);
}

void com_task(int len, uint8_t* data)
{
    if (!len || len > MSG_PACK_SIZE) return;
    struct msg_pack* pack = (struct msg_pack*)data;

    if ((0xff == (pack->cmd[0] | pack->cmd[1])) && (0x00 == (pack->cmd[0] & pack->cmd[1])))
    {
        // co_printf("cmd check! %02X\r\n", pack->cmd[0]);
        switch (pack->cmd[0])
        {
        case CMD_FLASH_HAND_SHAKE:
            uart_putc_noint_no_wait(UART1, CMD_FLASH_ACK);
            break;
        case CMD_FLASH_ERASE_SECTOR:
        {
            uint32_t addr = pack->addr[0] << 24 | pack->addr[1] << 16 | pack->addr[2] << 8 | pack->addr[3];
            // w25qxx_EraseSector(addr);
            // w25qxx_WaitBusy();
            uart_putc_noint_no_wait(UART1, CMD_FLASH_ACK);
            co_printf("erase sector: %08X\r\n", addr);
        }
        break;
        case CMD_FLASH_WRITE_DATA:
            if (pack->data_size > MSG_DATA_SIZE_MAX)
            {
                uart_putc_noint_no_wait(UART1, CMD_FLASH_NACK);
            }
            else
            {
                // uart_putc_noint_no_wait(UART1, CMD_FLASH_ACK); REET
                if (msg_Check(pack))
                {
                    uint32_t addr = pack->addr[0] << 24 | pack->addr[1] << 16 | pack->addr[2] << 8 | pack->addr[3];
                    // w25qxx_WaitBusy();
                    // w25qxx_PageProgram(addr, pack->data, pack->data_size);
                    uart_putc_noint_no_wait(UART1, CMD_FLASH_ACK);
                }
                else
                {
                    uart_putc_noint_no_wait(UART1, CMD_FLASH_NACK);
                }
            }
            break;
        case CMD_FLASH_READ_DATA:
            if (pack->data_size > MSG_DATA_SIZE_MAX)
            {
                uart_putc_noint_no_wait(UART1, CMD_FLASH_NACK);
            }
            else
            {
                uint32_t addr = pack->addr[0] << 24 | pack->addr[1] << 16 | pack->addr[2] << 8 | pack->addr[3];
                uint8_t txBuf[64];
                w25qxx_WaitBusy();
                // co_printf("read addr:%d size:%d\r\n",addr,pack->data_size );
                w25qxx_FastRead(addr, txBuf, pack->data_size);
                for (int i = 0; i < pack->data_size; i++)
                {
                    uart_putc_noint_no_wait(UART1, txBuf[i]);
                }
            }
            break;
        default:
            uart_putc_noint_no_wait(UART1, CMD_FLASH_NACK);
            break;
        }
    }
    // co_delay_ms(1);
    // co_printf("cmd=%02x%02x,addr=%02x%02x%02x%02x,", pack->cmd[0], pack->cmd[1], pack->addr[0], pack->addr[1], pack->addr[2], pack->addr[3]);
    // co_printf("data_check:%d size:%d check:%d\r\n", pack->data_check, pack->data_size, msg_Check(pack));

    // for (int i = 0;i < pack->data_size;i++)
    // {
    //     co_printf("%02x ", pack->data[i]);
    // }
    // co_printf("\r\n");

}