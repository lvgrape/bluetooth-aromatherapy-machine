#include <stdint.h>
#include <string.h>
#include <uart.h>
#include <driver_uart.h>
#include "co_printf.h"
#include "zino.h"
#include "driver_gpio.h"
#include "os_timer.h"
#include "woodmaster.h"
#include "rgb_mode.h"
#include "w25qxx.h"
#include "ble_dev.h"
#include "os_task.h"
#define USE_UART1

void rxbufferInChar(uart_t* u, uint8_t c, uint8_t status)
{
    if (u->rxbufferCnt >= RX_BUFFER_SIZE) u->rxbufferCnt = 0;
    u->rxbuffer[u->rxbufferCnt] = c;  // Add character to buffer. 将数据添加到队列
    u->rxbufferCnt++;
    u->status = status;  // Set status. 设置状态。 有关接收到的消息的详细信息

}
uint8_t getUartRxBufferCount(uart_t* u)
{
    return u->rxbufferCnt;
}
void UartClearRxBuffer(uart_t* u) 	// Clear the receive buffer. 清除接收到的消息队列。
{
    memset(u->rxbuffer, 0, RX_BUFFER_SIZE);
    u->rxbufferCnt = 0;  // Set the counter to 0. 将队列的内容清空。 数据从零
}
static uart_t uart1_rx1;
int UartInit()
{
#if 0
    system_set_port_pull(GPIO_PD6, true);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_6, PORTD6_FUNC_UART0_RXD);
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_7, PORTD7_FUNC_UART0_TXD);
    uart_init(UART0, BAUD_RATE_115200);
    co_printf("uart0 demo\r\n");

#endif
    // FIXME 串口波特率不起作用
    // FIXME 中断也没有，可能uart_init1函数无效
    system_set_port_pull(GPIO_PA2, true);
    system_set_port_mux(GPIO_PORT_A, GPIO_BIT_2, PORTA2_FUNC_UART1_RXD);
    system_set_port_mux(GPIO_PORT_A, GPIO_BIT_3, PORTA3_FUNC_UART1_TXD);
    //uart_init(UART1, BAUD_RATE_115200);
    uart_param_t param =
    {
        .baud_rate = 921600,
        .data_bit_num = 8,
        .pari = 0,
        .stop_bit = 1,
    };
    uart_init1(UART1, param);
    NVIC_EnableIRQ(UART1_IRQn);
    // NVIC_EnableIRQ(UART0_IRQn);
    // uart_init1(UART0, param);
    co_printf("uart1 Init\r\n");
    return 0;
}
// ZINO_INIT_APP_EXPORT(UartInit);
// extern void peripheral_demo(void);
// ZINO_INIT_APP_EXPORT(peripheral_demo);
void Uart1RxTest()
{
    co_printf("S:%d cnt:%d Buffer: ", uart1_rx1.status, uart1_rx1.rxbufferCnt);
    for (uint16_t i = 0;i < uart1_rx1.rxbufferCnt;i++) co_printf("%02X ", uart1_rx1.rxbuffer[i]);
    co_printf("\r\n");

}
extern void com_task(int len, uint8_t* data);
void Uart1RxCallback()
{
    //./loadsbc COM1 115200 ./wav/8k_good_luck_new_spring.sbc 0

    static uint32_t idleTime = 0;
    static uint16_t tol_cnt=0;
    static uint8_t rxBuff[1024];



    uart1_rx1.rxbufferCnt = uart_get_data_nodelay_noint(UART1, uart1_rx1.rxbuffer, 32);
    // com_task(uart1_rx1.rxbufferCnt, uart1_rx1.rxbuffer);
    if (uart1_rx1.rxbufferCnt)
    {
        //将收到的所有数据存起来
        idleTime=millis();
        memcpy(rxBuff+tol_cnt,uart1_rx1.rxbuffer,uart1_rx1.rxbufferCnt);
        tol_cnt+=uart1_rx1.rxbufferCnt;
    }
    else
    {
        if(millis() > (idleTime+1))//没有新数据了,且空闲了1ms
        {
            if(tol_cnt)//收到过数据
            {
                co_printf("rx_get:%d->",tol_cnt);
                for(uint16_t i=0;i<tol_cnt;i++)
                {
                    co_printf("%02X ",rxBuff[i]);
                    if(i%15==0)
                    {
                        co_printf("\r\n");
                    }
                }
                co_printf("\r\n");
                // com_task(rxBuff, tol_cnt);
                memset(rxBuff,0,tol_cnt);
                tol_cnt=0;
            }
        }
    }
}

#if 0
if (uart1_rx1.rxbufferCnt)
{
    Uart1RxTest();
    // if(uart1_rx1.rxbuffer[uart1_rx1.rxbufferCnt-1] == '\n' || uart1_rx1.rxbuffer[uart1_rx1.rxbufferCnt-2] == '\r')//收到回车开始处理
    // {
    if (strncmp(uart1_rx1.rxbuffer, "reboot", 6) == 0)
    {
        co_printf("reboot...\r\n");//重新启动应用程序。
        system_reboot();
    }
    if (strncmp(uart1_rx1.rxbuffer, "scan", 4) == 0)
    {
        co_printf("Scaning...\r\n");//重新启动应用程序。
        woodmaster_start_scan();
    }
    if (uart1_rx1.rxbuffer[0] == '#')
    {
        uint8_t rxtest[32];
        for (int i = 0;i < 128;i++)
        {
            w25qxx_ReadData(i, rxtest, 32);
            co_printf("%03d:", i);
            printf_hex(rxtest, 32, 1);
            co_printf("\r\n");

        }
    }
    // }
    if (uart1_rx1.rxbuffer[uart1_rx1.rxbufferCnt - 1] == '\n' || uart1_rx1.rxbuffer[uart1_rx1.rxbufferCnt - 1] == '\r')//收到回车开始处理
        UartClearRxBuffer(&uart1_rx1);
}
#endif

#include "zino_shell.h"
static os_timer_t rx1_timer;
int initRxRuntine()
{
    // UartInit();
    // os_timer_init(&rx1_timer, Uart1RxCallback, NULL);
    // os_timer_init(&rx1_timer, shell_task, &uart1_rx1);
    // os_timer_start(&rx1_timer, 10, 1);
    os_user_loop_event_set(Uart1RxCallback);
    co_printf("initRxRuntine ...\r\n");

    return 0;
}
ZINO_INIT_APP_EXPORT(initRxRuntine);
#ifdef USE_UART1
__attribute__((section("ram_code"))) void uart1_isr_ram(void)
{
    uint8_t int_id;
    uint8_t c;
    volatile struct uart_reg_t* uart_reg = (volatile struct uart_reg_t*)UART1_BASE;

    int_id = uart_reg->u3.iir.int_id;
    uart_putc_noint_no_wait(UART1, int_id);
    if (int_id == 0x04 || int_id == 0x0c)   /* Receiver data available or Character time-out indication */
    {
        while (uart_reg->lsr & 0x01)
        {
            c = uart_reg->u1.data;
            uart_putc_noint_no_wait(UART1, c);
        }
    }
    else if (int_id == 0x06)
    {
        //uart_reg->u3.iir.int_id = int_id;
        //uart_reg->u2.ier.erlsi = 0;
        volatile uint32_t line_status = uart_reg->lsr;
    }
}
// __attribute__((section("ram_code"))) void uart1_isr_ram(void)
// {
//     volatile struct uart_reg_t* uart_reg = (volatile struct uart_reg_t*)UART1_BASE;
//     uint8_t int_id=uart_reg->u3.iir.int_id;
//     uart_putc_noint_no_wait(UART1,int_id);
//     // uart1_rx1.rxbufferCnt++;
//     // rxbufferInChar(&uart1_rx1, uart_reg->u1.data, uart_reg->u3.iir.int_id);
//     // Uart1RxTest();
//     if (uart_reg->u3.iir.int_id == UART_INT_RDA || uart_reg->u3.iir.int_id == UART_INT_CTI)   /* Receiver data available or Character time-out indication */
//     {
//         /**DR is set when the complete incoming character is
//         received and transferred into RBR or the FIFO. */
//         while (uart_reg->lsr & 0x01)
//         {
//             // rxbufferInChar(&uart1_rx1, uart_reg->u1.data, uart_reg->u3.iir.int_id);
//             uart_putc_noint_no_wait(UART1,uart_reg->u1.data);
//         }
//     }
//     else if (uart_reg->u3.iir.int_id == UART_INT_RLS)
//     {
//         //uart_reg->u3.iir.int_id = int_id;
//         //uart_reg->u2.ier.erlsi = 0;
//         volatile uint32_t line_status = uart_reg->lsr;
//     }
// }
#endif
// __attribute__((section("ram_code"))) void uart1_isr_ram(void)
// {
//     uint8_t int_id;
//     uint8_t c;
//     volatile struct uart_reg_t* uart_reg = (volatile struct uart_reg_t*)UART1_BASE;

//     int_id = uart_reg->u3.iir.int_id;

//     if (int_id == 0x04 || int_id == 0x0c)   /* Receiver data available or Character time-out indication */
//     {
//         while (uart_reg->lsr & 0x01)
//         {
//             c = uart_reg->u1.data;
//             uart_putc_noint_no_wait(UART1, c);
//             rxbufferInChar(&uart1_rx1, c, int_id);
//         }
//     }
//     else if (int_id == 0x06)
//     {
//         //uart_reg->u3.iir.int_id = int_id;
//         //uart_reg->u2.ier.erlsi = 0;
//         volatile uint32_t line_status = uart_reg->lsr;
//     }
// }
// __attribute__((section("ram_code"))) void uart0_isr_ram(void)
// {
//     uint8_t int_id;
//     uint8_t c;
//     volatile struct uart_reg_t* uart_reg = (volatile struct uart_reg_t*)UART0_BASE;

//     int_id = uart_reg->u3.iir.int_id;

//     if (int_id == 0x04 || int_id == 0x0c)   /* Receiver data available or Character time-out indication */
//     {
//         while (uart_reg->lsr & 0x01)
//         {
//             c = uart_reg->u1.data;
//             uart_putc_noint_no_wait(UART0, c);
//         }
//     }
//     else if (int_id == 0x06)
//     {
//         //uart_reg->u3.iir.int_id = int_id;
//         //uart_reg->u2.ier.erlsi = 0;
//         volatile uint32_t line_status = uart_reg->lsr;
//     }
// }