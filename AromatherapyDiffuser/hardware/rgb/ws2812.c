/*
 * @Author: LVGRAPE
 * @Date: 2023-05-18 13:59:03
 * @LastEditTime: 2024-01-09 13:52:35
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\rgb\ws2812.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include <stdint.h>
#include "driver_plf.h"
#include "driver_system.h"
#include "driver_pwm.h"
#include "driver_gpio.h"
#include "co_printf.h"
#include "nop.h"
#include "power.h"
#include "zino.h"
#include <string.h>
#include "os_timer.h"
 /**
  * NOTE
  * 名称	描	述	            典型值	容许误差
  * T0H	0 码，高电平时间	0.3µs	± 0.05us
  * T1H	1 码，高电平时间	0.6µs	± 0.05us
  * T0L	0 码，低电平时间	0.6µs	± 0.05us
  * T1L	1 码，低电平时间	0.3µs	± 0.05us
  * Trst	Reset 码，低电平时间	≥80us
  */
  /**
   * 主频48MHz, nop()~~20ns
   */


typedef union bitValue
{
    struct {
        uint8_t bit0 : 1;//1->1, 0->0 	1->1, 0->0 	1->1, 0->
        uint8_t bit1 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit2 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit3 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit4 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit5 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit6 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
        uint8_t bit7 : 1;//1->1, 0->0 	1->1, 0->0 	1->1,
    };
    uint8_t value;//8bit 	1->1, 0->0 	1->1, 0->0 	1->1
}bitValue_t;

static bitValue_t dataBuffer;
// #define ws2812_gpio_high() REG_PL_WR(GPIO_PORTB_DATA, REG_PL_RD(GPIO_PORTB_DATA)|0x20)
// #define ws2812_gpio_low() REG_PL_WR(GPIO_PORTB_DATA, REG_PL_RD(GPIO_PORTB_DATA)&~0x20)
#define ws2812_gpio_high() REG_PL_WR(GPIO_PORTA_DATA, REG_PL_RD(GPIO_PORTA_DATA)|0x01)//A0
#define ws2812_gpio_low() REG_PL_WR(GPIO_PORTA_DATA, REG_PL_RD(GPIO_PORTA_DATA)&~0x01)//A0

#define ws2812_T1H() NOP25()
#define ws2812_T0L() NOP25()
#define ws2812_T1L() NOP5()
#define ws2812_T0H() NOP5()
#define ws2812_TRST() cpu_delay_100us() 
#define ws2812_write_1() ws2812_gpio_high();ws2812_T1H(); ws2812_gpio_low();ws2812_T1L()
#define ws2812_write_0() ws2812_gpio_high();ws2812_T0H(); ws2812_gpio_low();ws2812_T0L()

const char rainbowColor[]={
    255,0,0,
    255,165,0,
    255,255,0,
    0,255,0,
    0,255,255,
    0,0,255,
    128,0,128,
};
void ws2812_write_byte(uint8_t b)
{
    #if 0
    dataBuffer.value = b;
    if (dataBuffer.bit7) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit6) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit5) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit4) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit3) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit2) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit1) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    if (dataBuffer.bit0) { ws2812_write_1(); }
    else { ws2812_write_0(); }
    #else
    for(uint8_t i=0;i<8;i++)
    {
        if(b & 0x80) {ws2812_write_1();} else {ws2812_write_0();}
        b <<= 1;        // shift next bit into place
    }
    #endif
}
void ws2812_write(uint8_t* rgb_buffer, uint8_t rgbCnt)
{
    for (uint8_t i = 0;i < rgbCnt * 3;i += 3)
    {
        ws2812_write_byte(*(rgb_buffer + i + 1));
        ws2812_write_byte(*(rgb_buffer + i + 0));
        ws2812_write_byte(*(rgb_buffer + i + 2));
    }

}

int ws2812_init()
{
    // ENABLE_5V_POWER();
    system_set_port_mux(GPIO_PORT_A, GPIO_BIT_0, PORTA0_FUNC_A0);
    gpio_set_dir(GPIO_PORT_A, GPIO_BIT_0, GPIO_DIR_OUT);
    co_printf("RGB array init...\r\n");
}
ZINO_INIT_DEVICE_EXPORT(ws2812_init);
void ws2812_test()
{
    // powerControlInit();
    uint8_t rgb[15] = {
        0xff,0x00,0x00,
        0x00,0xFF,0x00,
        0x00,0x00,0xFF,
        0xFF,0xFF,0xFF,
        0xFF,0x00,0xFF,
    }; //black
    // ENABLE_5V_POWER();
    co_printf("ws2812_test \r\n");
    ws2812_init();
    GLOBAL_INT_DISABLE();
    ws2812_write(rgb, 5);
    GLOBAL_INT_RESTORE();
}
uint8_t rgb_buffer[33]; // buffer for 12 pixels.  12 bytes per pixel.  RGB 24 Bit.  1 byte per pixel.  1 bit per
void rainbowRGB_loop()
{
    static uint8_t curpos=0;
    static uint8_t maxRgbCnt = 9;
    for(uint8_t i=0;i<sizeof(rainbowColor);i++)
    {
        if(curpos+i>=sizeof(rgb_buffer))
        {
            rgb_buffer[curpos+i - sizeof(rgb_buffer)]=rainbowColor[i];
        }
        else
        {
            rgb_buffer[curpos+i]=rainbowColor[i];
        }
    }
    curpos+=3;
    if(curpos>27) curpos = 0;
    GLOBAL_INT_DISABLE();
    ws2812_write(rgb_buffer, 11);
    GLOBAL_INT_RESTORE();
    // co_printf("curpos:%d\n",curpos);
}
int rainbowRgbStart()
{
    static os_timer_t rainbowTimer;
    os_timer_init(&rainbowTimer,rainbowRGB_loop,NULL);
    os_timer_start(&rainbowTimer,250,1);
    co_printf("rainbowRgbStart \r\n");
    // ENABLE_5V_POWER();
    // co_printf("ENABLE_5V_POWER:%d \r\n",ENABLE_5V_POWER()) ;
}
// ZINO_INIT_APP_EXPORT(rainbowRgbStart);