/*** 
 * @Author: LVGRAPE
 * @Date: 2023-06-07 17:34:09
 * @LastEditTime: 2024-01-10 16:48:46
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\rgb\rgb_mode.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __RGB_MODE_H_
#define __RGB_MODE_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
void rgb_led_set(uint8_t index, uint8_t r, uint8_t g, uint8_t b);
void rgb_led_set_all(uint8_t r, uint8_t g, uint8_t b);
void rgb_led_set_hexs(uint8_t index, char* color);
void rgb_led_clear();
void rgb_led_show();
void rgb_ble_scaning();
void rgb_battery_mode();
void rgb_ble_connecting(uint8_t m);
void rgb_link_and_unlock_mode(uint8_t m);
void update_battery_color();
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
