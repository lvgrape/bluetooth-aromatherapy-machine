#ifndef __WS2812_H_
#define __WS2812_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus



#include <stdint.h>

void ws2812_init();
void ws2812_test();
void ws2812_write(uint8_t* rgb_buffer, uint8_t rgbCnt);
#ifdef __cplusplus
}
#endif // __cplusplus
#endif