#include "ws2812.h"
#include "driver_plf.h"
#include "driver_system.h"
#include <string.h>
#include "co_printf.h"
#include "power.h"
#include "rgb_mode.h"
#define RGB_COUNT 8

// static uint8_t rgb_buff[RGB_COUNT * 3];
static uint8_t rgb_buff[RGB_COUNT][3];
static uint8_t rgbcnt = 0;
static uint16_t dt = 0;
static uint8_t r, g, b;//定义亮3D灯泡屏幕的颜色值。 这个函数应自动
const uint8_t* rgb_main_color;


/**
 * @brief
 *
 * @param index  0~4
 * @param r 0~255
 * @param g 0~255
 * @param b 0~255
 */
void rgb_led_set(uint8_t index, uint8_t r, uint8_t g, uint8_t b)
{
    if (index >= RGB_COUNT) return;
    rgb_buff[index][0] = r;
    rgb_buff[index][1] = g;
    rgb_buff[index][2] = b;
}
/**
 * @brief
 *
 * @param pos 0~RGB COUNT
 * @param color color data buff, must be multiple of 3
 * @param len color data len, must be multiple of 3. if len less than RGB_COUNT, then it will be padded with 0.
 */
void rgb_led_sets(uint8_t pos, uint8_t* color, uint8_t len)
{
    if (pos >= RGB_COUNT) return;
    /**只填充不超过剩余的长度 */
    uint8_t fixlen = ((len / 3 + pos) > RGB_COUNT) ? (RGB_COUNT - pos) * 3 : len;
    memcpy(&rgb_buff[pos][0], color, fixlen);
}
void rgb_led_set_u32(uint8_t index, uint32_t color)
{
    rgb_led_set(index, color >> 16, color >> 8, color);
}
uint8_t strToInt(char c)
{
    if (c >= '0' && c <= '9')
    {
        c = c - '0';  //0-9 ->0-9 或者 decodeHex2 ->0x00-0x09 或 decode
    }
    else if (c >= 'A' && c <= 'F')
    {
        c = c - 'A' + 10;  //A-F -> 10-15 或 decodeHex2 -> 0x0a-0x0
    }
    else if (c >= 'a' && c <= 'f')
    {
        c = c - 'a' + 10;  //a-f -> 10-15 或 decodeHex2 -> 0x0a-0x0
    }
    else
    {
        c = 0;
    }
    return c;
}
/**
 * @brief
 *
 * @param index
 * @param color format #000000~#ffffff
 */
void rgb_led_set_hexs(uint8_t index, char* color)
{
    if (color[0] == '#')
    {
        uint8_t r = strToInt(color[1]) * 16 + strToInt(color[2]);
        uint8_t g = strToInt(color[3]) * 16 + strToInt(color[4]);
        uint8_t b = strToInt(color[5]) * 16 + strToInt(color[6]);
        rgb_led_set(index, r, g, b);
        co_printf("index:%d %d,%d,%d\r\n", index, r, g, b);
    }
    else
    {
        rgb_led_set(index, color[0], color[1], color[2]);
    }
}
void rgb_led_set_all(uint8_t r, uint8_t g, uint8_t b)
{
    for (uint8_t i = 0;i < RGB_COUNT;i ++)
    {
        rgb_buff[i][0] = r;
        rgb_buff[i][1] = g;
        rgb_buff[i][2] = b;
    }
}
void rgb_led_clear()
{
    memset(rgb_buff, 0, sizeof(rgb_buff));//清屏显示亮3D灯泡屏幕的颜色。 这个函数
}
void rgb_led_show()
{
    GLOBAL_INT_DISABLE();
    ws2812_write(rgb_buff[0], RGB_COUNT);
    GLOBAL_INT_RESTORE();
}
void rgb_ble_scaning()
{
    const uint8_t step = 15;

    update_battery_color();
    dt++;
    // rgb_led_clear();
    if (dt <= step)
    {
        r = rgb_main_color[0] * dt / step;
        g = rgb_main_color[1] * dt / step;
        b = rgb_main_color[2] * dt / step;

    }
    else if (dt > step && (dt <= step * 2))
    {
        r = rgb_main_color[0] * (2 * step - dt) / step;
        g = rgb_main_color[1] * (2 * step - dt) / step;
        b = rgb_main_color[2] * (2 * step - dt) / step;
    }
    else if (dt > step * 2)
    {
        rgbcnt++;
        dt = 0;
        if (rgbcnt > 4)
        {
            rgbcnt = 0;
        }
    }
    rgb_led_set_all(r, g, b);
    rgb_led_show();
}
void rgb_ble_brocast()
{
    const uint8_t step = 50;
    update_battery_color();
    dt++;
    // rgb_led_clear();
    if (dt <= step)
    {

        r = rgb_main_color[0] * dt / step;
        g = rgb_main_color[1] * dt / step;
        b = rgb_main_color[2] * dt / step;
        rgb_led_set(rgbcnt, r, g, b);
        if (dt == step)
        {
            rgbcnt++;
            if (rgbcnt > 4)
            {
                rgbcnt = 0;
                dt = step + 1;
            }
            else
            {
                dt = 0;
            }
        }
    }
    else if (dt > step)
    {

        r = rgb_main_color[0] * (2 * step - dt) / step;
        g = rgb_main_color[1] * (2 * step - dt) / step;
        b = rgb_main_color[2] * (2 * step - dt) / step;
        rgb_led_set_all(r, g, b);
        if (dt >= (step * 2))
        {
            dt = 0;
            rgbcnt = 0;
        }
    }
    // co_printf("dt:%d cnt:%d step:%d \r\n",dt,rgbcnt,step);
    rgb_led_show();
    // static uint8_t step = 0;
    // static uint8_t scnt = 0;
    // const uint8_t rbgMax[3] = { 227,0,140 };
    // if (scnt++ >= 5)
    // {
    //     scnt = 0;
    //     step++;
    //     if (step >= RGB_COUNT) step = 0;
    //     uint8_t r = rbgMax[0] * (RGB_COUNT - step) / RGB_COUNT;
    //     uint8_t g = rbgMax[1] * (RGB_COUNT - step) / RGB_COUNT;
    //     uint8_t b = rbgMax[2] * (RGB_COUNT - step) / RGB_COUNT;
    //     rgb_led_clear();
    //     rgb_led_set(
    //         step,
    //         r, g, b
    //     );
    //     rgb_led_show();
    // }
    // co_printf("Step:%d %d,%d,%d\r\n",step, r,g,b);
}
void rgb_ble_connecting(uint8_t m)
{
    const uint8_t step = 25 * (m + 1);
    update_battery_color();
    rgb_led_clear();
    dt++;
    if (dt <= step)
    {
        r = rgb_main_color[0] * dt / step;
        g = rgb_main_color[1] * dt / step;
        b = rgb_main_color[2] * dt / step;
    }
    else if (dt > step && (dt <= step * 2))
    {
        r = rgb_main_color[0] * (2 * step - dt) / step;
        g = rgb_main_color[1] * (2 * step - dt) / step;
        b = rgb_main_color[2] * (2 * step - dt) / step;
    }
    else if (dt > step * 2)
    {
        rgbcnt++;
        dt = 0;
        if (rgbcnt > 2)
        {
            rgbcnt = 0;
        }
    }
    rgb_led_set(rgbcnt, r, g, b);
    rgb_led_set(4 - rgbcnt, r, g, b);
    rgb_led_show();
}

const uint8_t rgb_battery_color[4][3] =
{
{0xFF,0x00,0x00},//红  <25%
{0xFF,0xFF,0x00},//黄  >25%
{0x00,0x00,0xFF},//蓝  >50%
{0x00,0xFF,0x00},//绿, >75%
};

void update_battery_color()
{
    uint8_t bl = get_battery_level() / 25;
    if (bl > 3)bl = 3;
    rgb_main_color = rgb_battery_color[bl];
}
/**
 * @brief 连接后的灯光，颜色表示电量。解锁后节奏更快。
 *
 * @param m 0 lock, 1 unlock
 */
void rgb_link_and_unlock_mode(uint8_t m)
{
    update_battery_color();

    uint8_t BREATH_STEP = ((m != 0) ? 10 : 50);
    dt++;

    if (dt <= BREATH_STEP)
    {
        r = rgb_main_color[0] * dt / BREATH_STEP;
        g = rgb_main_color[1] * dt / BREATH_STEP;
        b = rgb_main_color[2] * dt / BREATH_STEP;
        rgb_led_set(rgbcnt, r, g, b);
        if (dt == BREATH_STEP)
        {
            rgbcnt++;
            if (rgbcnt > 2)
            {
                rgbcnt = 2;
                dt = BREATH_STEP + 1;
            }
            else
            {
                dt = 0;
            }
        }
    }
    else if (dt > BREATH_STEP)
    {
        r = rgb_main_color[0] * (BREATH_STEP * 2 - dt) / BREATH_STEP;
        g = rgb_main_color[1] * (BREATH_STEP * 2 - dt) / BREATH_STEP;
        b = rgb_main_color[2] * (BREATH_STEP * 2 - dt) / BREATH_STEP;
        rgb_led_set(rgbcnt, r, g, b);
        if (dt >= BREATH_STEP * 2)
        {

            if (rgbcnt == 0)
            {
                rgbcnt = 0;
                dt = 0;
            }
            else
            {
                dt = BREATH_STEP + 1;
                rgbcnt--;
            }
        }
    }
    
    // co_printf("dt:%d rt:%d \n",dt,rgbcnt);
    // co_printf("bl:%03d %03d %03d %03d\r\n", dt, c[0][0],c[0][1],c[0][2]);
    rgb_led_show();
}
