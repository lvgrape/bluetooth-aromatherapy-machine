/*** 
 * @Author: LVGRAPE
 * @Date: 2024-01-04 17:26:31
 * @LastEditTime: 2024-01-10 13:53:36
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\spi\hard_spi.h
 * @要啥没啥，爱咋咋的
 */

#ifndef __HARD_SPI_H_
#define __HARD_SPI_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>

typedef struct fr_spi_device *fr_spi_dev_t;

struct fr_spi_device {
    uint8_t bit_width;
    uint32_t freq;
    uint8_t mode;
    void (*cs_ctrl)(uint8_t);
};
int8_t fr_spi_init(fr_spi_dev_t dev, uint8_t bit_width, uint32_t freq, uint8_t mode, void (*cs_ctrl)(uint8_t));
int8_t fr_spi_send_than_send(fr_spi_dev_t dev, const uint8_t *tx1_buf, uint32_t tx1_len, const uint8_t *tx2_buf, uint32_t tx2_len);
int8_t fr_spi_send_than_recv(fr_spi_dev_t dev, uint8_t *tx_buff, uint32_t tx_len, uint8_t *rx_buf, uint32_t rx_len);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
