/*
 * @Author: LVGRAPE
 * @Date: 2024-01-04 16:39:28
 * @LastEditTime: 2024-01-10 14:21:31
 * @LastEditors: LVGRAPE
 * @Description: inc
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\spi\hard_spi.c
 * 要啥没啥，爱咋咋的
 */
#include "driver_ssp.h"
#include "driver_plf.h"
#include "driver_system.h"
#include "driver_gpio.h"
#include "hard_spi.h"
#include "co_printf.h"

typedef unsigned int    u32;
typedef unsigned short  u16;
typedef unsigned char   u8;

struct ssp_cr0
{
    u32 dss : 4;  /* data size select : = DATASIZE - 1*/

    u32 frf : 2;  /* frame format */

    u32 spo : 1;  /* sclk polarity */
    u32 sph : 1;  /* sclk phase */
    u32 scr : 8;  /* serial clock rate */
    u32 unused : 16;
};

struct ssp_cr1
{
    u32 rie : 1;
    u32 tie : 1;
    u32 rorie : 1;

    u32 lbm : 1;  /* loop back mode */
    u32 sse : 1;  /* synchronous serial port enable*/

    u32 ms : 1;   /* master mode or slave mode */
    u32 sod : 1;  /* output disable in slave mode */

    u32 unused : 25;
};

struct ssp_dr
{
    u32 data;
};

struct ssp_sr
{
    u32 tfe : 1;  /* transmit fifo empty */
    u32 tnf : 1;  /* transmit fifo not full */
    u32 rne : 1;  /* receive fifo not empty */
    u32 rff : 1;  /* receive fifo full */
    u32 bsy : 1;  /* ssp busy flag */

    u32 unused : 27;
};

struct ssp_cpsr
{
    u32 cpsdvsr : 8;  /* clock prescale divisor 2-254 */

    u32 unused : 24;
};

struct ssp_iir
{
    uint32_t ris : 1;
    uint32_t tis : 1;
    uint32_t roris : 1;
    uint32_t reserved : 29;
};

struct ssp
{
    struct ssp_cr0 ctrl0;
    struct ssp_cr1 ctrl1; /*is also error clear register*/
    struct ssp_dr data;
    struct ssp_sr status;
    struct ssp_cpsr clock_prescale;
    struct ssp_iir iir;
};
struct ssp_env_t
{
    void (*ssp_cs_ctrl)(uint8_t);
};
static struct ssp_env_t ssp_env;
static fr_spi_dev_t spi_current_bus = 0;

/*********************************************************************
 * @fn      ssp_init_
 *
 * @brief   Initialize ssp instance.
 *
 * @param   bit_width   - trans or recv bits witdh
 *          frame_type  -
 *          ms          - indicate ssp controller working mode
 *          bit_rate    - ssp bus frame clock, min_bus_clk = 184HZ, when prescale=254,cpu_clk = 12M;
 *                        max_bus_clk = 24MHZ, when prescale=2,cpu_clk = 48M;
 *          prescale    - ssp controller prescale
 *          ssp_cs_ctrl - if cs is controlled by software, this parameter
 *                        should be corresponding function.
 *          mode        - spi mode0~mode3
 *
 * @return  None.
 */
static void ssp_init_mode(uint8_t bit_width, uint8_t frame_type, uint8_t ms, uint32_t bit_rate, uint8_t prescale, void (*ssp_cs_ctrl)(uint8_t), uint8_t mode)
{
    volatile uint8_t data;
    volatile struct ssp* ssp = (volatile struct ssp*)SSP0_BASE;

    /* reset all */
    *(int*)(&(ssp->ctrl0)) = 0;
    *(int*)(&(ssp->ctrl1)) = 0;
    ssp->ctrl1.sse = 0;

    /* data size */
    ssp->ctrl0.dss = bit_width - 1;

    /* frame type */
    ssp->ctrl0.frf = frame_type;

    // ssp->ctrl0.spo = 0;     //mode 0, clk idle is high
    // ssp->ctrl0.sph = 0;     //mode 0, clk rising edge to sample
    // ssp->ctrl0.spo = 1;     //mode 3, clk idle is high
    // ssp->ctrl0.sph = 1;     //mode 3, clk rising edge to sample
    switch (mode)
    {
    case 0:
        ssp->ctrl0.spo = 0;
        ssp->ctrl0.sph = 0;
        break;
    case 1:
        ssp->ctrl0.spo = 0;
        ssp->ctrl0.sph = 1;
        break;
    case 2:
        ssp->ctrl0.spo = 1;
        ssp->ctrl0.sph = 0;
        break;
    case 3:
        ssp->ctrl0.spo = 1;
        ssp->ctrl0.sph = 1;
        break;

    default:
        ssp->ctrl0.spo = 0;
        ssp->ctrl0.sph = 0;
        break;
    }

    /* clock rate */
    ssp->clock_prescale.cpsdvsr = prescale;
    ssp->ctrl0.scr = (system_get_pclk() / (prescale * bit_rate)) - 1;

    if (ms == 0x1)
        ssp->ctrl1.sod = 0x0;   //slave mode, slave can send
    else
        ssp->ctrl1.sod = 0x1;   //master mode, slave can't send
    /* working mode */
    ssp->ctrl1.ms = ms;

    ssp->ctrl1.sse = 1;

    /* clear rx fifo */
    while (ssp->status.rne == 1)
        data = ssp->data.data;

    //wait tx fifo to empty
    while (ssp->status.tfe == 0)
        ;

    ssp_env.ssp_cs_ctrl = ssp_cs_ctrl;

}

int8_t fr_spi_init(fr_spi_dev_t dev, uint8_t bit_width, uint32_t freq, uint8_t mode, void (*cs_ctrl)(uint8_t))
{
    dev->bit_width = bit_width;
    dev->freq = freq;
    dev->mode = mode;
    dev->cs_ctrl = cs_ctrl;
    ssp_init_mode(dev->bit_width, SSP_FRAME_MOTO, SSP_MASTER_MODE, dev->freq, 2, cs_ctrl, dev->mode);
    spi_current_bus = dev;
}
int8_t fr_spi_send_than_send(fr_spi_dev_t dev, const uint8_t* tx1_buf, uint32_t tx1_len, const uint8_t* tx2_buf, uint32_t tx2_len)
{
    if (spi_current_bus != dev)
    {
        ssp_init_mode(dev->bit_width, SSP_FRAME_MOTO, SSP_MASTER_MODE, dev->freq, 2, dev->cs_ctrl, dev->mode);
        // co_printf("ss spi_init_mode:0x%08X\r\n", dev);
        spi_current_bus = dev;
    }

    if (dev->cs_ctrl)
    {
        dev->cs_ctrl(SSP_CS_ENABLE);
    }

    volatile struct ssp* ssp = (volatile struct ssp*)SSP0_BASE;
    uint8_t* tx_fifo_addr = (uint8_t*)&ssp->data;
#if 0
    while (tx1_len)
    {
        while (ssp->status.tnf == 0);
        ssp->data.data = *tx1_buf++;
        tx1_len--;
    }
    while (tx2_len)
    {
        while (ssp->status.tnf == 0);
        ssp->data.data = *tx2_buf++;
        tx2_len--;
    }

    while (ssp->status.bsy);
    if (dev->cs_ctrl)
    {
        dev->cs_ctrl(SSP_CS_DISABLE);
    }

    volatile uint8_t data;
    /* clear rx fifo */
    while (ssp->status.rne == 1)
        data = ssp->data.data;
#else
    /* send data first */
    uint32_t blocks;
    uint8_t write_size, temp;
    blocks = tx1_len / SSP_FIFO_SIZE;
    
    while (blocks)
    {
        for (uint8_t i = 0; i < SSP_FIFO_SIZE; i++)
        {
            ssp->data.data = *tx1_buf++;
        }
        write_size = SSP_FIFO_SIZE;
        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;
        } while (write_size);

        blocks--;
        tx1_len -= SSP_FIFO_SIZE;
    }

    if (tx1_len) {
        write_size = tx1_len;
        while (tx1_len)
        {
            ssp->data.data = *tx1_buf++;
            tx1_len--;
        }
        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;
        } while (write_size);
    }

    blocks = tx2_len / SSP_FIFO_SIZE;
    write_size = SSP_FIFO_SIZE;
    while (blocks)
    {
        for (uint8_t i = 0; i < SSP_FIFO_SIZE; i++)
        {
            ssp->data.data = *tx2_buf++;
        }
        write_size = SSP_FIFO_SIZE;
        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;
        } while (write_size);

        blocks--;
        tx2_len -= SSP_FIFO_SIZE;
    }

    if (tx2_len) {
        write_size = tx2_len;
        while (tx2_len)
        {
            ssp->data.data = *tx2_buf++;
            tx2_len--;
        }
        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;
        } while (write_size);
    }

    while (ssp->status.bsy);
    volatile uint8_t data;
    /* clear rx fifo */
    while (ssp->status.rne == 1)
        data = ssp->data.data;
    if (dev->cs_ctrl)
    {
        dev->cs_ctrl(SSP_CS_DISABLE);
    }

#endif
    return 0;
}

int8_t fr_spi_send_than_recv(fr_spi_dev_t dev, uint8_t* tx_buff, uint32_t tx_len, uint8_t* rx_buf, uint32_t rx_len)
{
    if (spi_current_bus != dev)
    {
        // ssp_init_mode(dev->bit_width,0, 0,dev->freq,2,dev->cs_ctrl, dev->mode);
        ssp_init_mode(dev->bit_width, SSP_FRAME_MOTO, SSP_MASTER_MODE, dev->freq, 2, dev->cs_ctrl, dev->mode);
        spi_current_bus = dev;
        // co_printf("sr spi_init_mode:0x%08X\r\n", dev);
    }
    volatile uint32_t temp;
    volatile struct ssp* const ssp = (volatile struct ssp*)SSP0_BASE;
    uint32_t blocks;
    uint32_t write_size, read_size;

    if (ssp_env.ssp_cs_ctrl)
    {
        ssp_env.ssp_cs_ctrl(SSP_CS_ENABLE);
    }

    //ssp->ctrl1.sse = 1;

    /* send data first */
    blocks = tx_len / SSP_FIFO_SIZE;
    write_size = SSP_FIFO_SIZE;

    // co_printf("tx blocks:%d  write_size:%d \r\n",blocks, write_size);
    while (blocks)
    {
        for (uint8_t i = 0; i < SSP_FIFO_SIZE; i++)
        {
            ssp->data.data = *tx_buff++;
        }

        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;

            // co_printf("tx blocks:%d  write_size:%d \r\n",blocks, write_size);
        } while (write_size);

        blocks--;
        tx_len -= SSP_FIFO_SIZE;
    }
    // co_printf("tx_len:%d\r\n",tx_len);
    if (tx_len) {
        write_size = tx_len;
        while (tx_len)
        {
            ssp->data.data = *tx_buff++;
            tx_len--;
        }
        do {
            while (ssp->status.rne == 0);
            temp = ssp->data.data;
            write_size--;
        } while (write_size);
    }

    /* receive data */
    blocks = rx_len / SSP_FIFO_SIZE;
    // co_printf("rx blocks:%d  read_size:%d \r\n",blocks, read_size);
    while (blocks)
    {
        for (uint8_t i = 0; i < SSP_FIFO_SIZE; i++)
        {
            ssp->data.data = 0;
        }
        read_size = SSP_FIFO_SIZE;
        do {
            while (ssp->status.rne == 0);
            *rx_buf++ = (uint8_t)ssp->data.data;
            read_size--;
            // co_printf("rx blocks:%d  read_size:%d \r\n",blocks, read_size);
        } while (read_size);

        blocks--;
        rx_len -= SSP_FIFO_SIZE;
    }
    // co_printf("rx_len:%d\r\n",rx_len);
    if (rx_len) {
        read_size = rx_len;
        while (rx_len)
        {
            ssp->data.data = 0;
            rx_len--;
        }
        do {
            while (ssp->status.rne == 0);
            *rx_buf++ = (uint8_t)ssp->data.data;
            read_size--;
            // co_printf("rx2 blocks:%d  read_size:%d \r\n",blocks, read_size);
        } while (read_size);
    }

    while (ssp->status.bsy);
    //ssp->ctrl1.sse = 0;

    if (ssp_env.ssp_cs_ctrl)
    {
        ssp_env.ssp_cs_ctrl(SSP_CS_DISABLE);
    }
    return 0;
}