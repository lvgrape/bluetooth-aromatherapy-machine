/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-25 10:52:19
 * @LastEditTime: 2023-05-26 10:32:14
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\ZINO_BLE\hardware\power\power.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __POWER_H_
#define __POWER_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>

int powerControlInit();
uint16_t get_battery_voltage_mV();
float get_battery_level();
uint8_t ENABLE_5V_POWER();
uint8_t DISABLE_5V_POWER();
uint8_t DISABLE_MAIN_POWER();
uint8_t EABLE_MAIN_POWER();
uint8_t is_powerkey_down();
uint8_t is_battery_charge_on();

#ifdef __cplusplus
}
#endif // __cplusplus
#endif
