/*
 * @Author: LVGRAPE
 * @Date: 2023-08-01 14:25:18
 * @LastEditTime: 2024-02-22 10:32:06
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ble_aromatherapy_diffuser\AromatherapyDiffuser\hardware\power\power.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
/*
 * @Author: LVGRAPE
 * @Date: 2023-05-25 10:52:05
 * @LastEditTime: 2023-06-01 18:02:56
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE\ZINO_BLE\hardware\power\power.c
 * 可以输入预定的版权声明、个性签名、空行等
 */

#include "power.h"
#include "driver_gpio.h"
#include "zino.h"
#include "driver_system.h"
#include "driver_iomux.h"
#include "driver_adc.h"
#include "os_timer.h"
#include "co_printf.h"
#include "filter.h"
#include <string.h>
#include <math.h>	// for sqrt() 	// for joystick joystick test 	// for joystick joystick test 	// for joystick joystick test
#define PWR_5VEN_PIN GPIO_PORT_A, GPIO_BIT_5//控制5V开关（高电平有效）PC7
#define PWR_5VEN_ENABLE 1
#define PWR_5VEN_DISABLE 0

#define BATTERY_MIN_VOLTAGE 3300 //3300mV ~ 1800V （电压范围） （根据实际情况选择）
#define BATTERY_MAX_VOLTAGE 4200 //4200mV ~ 4250V （电压范围）  （根据实际情况选择）

static Kalman_t vbatFilter;
static float vbat_mV;
static os_timer_t vbatAdcTimer;

void routed_adc_to_vbat()
{
    static struct adc_cfg_t AdcCfg;
    system_set_port_mux(GPIO_PORT_D, GPIO_BIT_4, PORTD4_FUNC_ADC0);
    memset((void*)&AdcCfg, 0, sizeof(AdcCfg));
    AdcCfg.src = ADC_TRANS_SOURCE_VBAT;
    AdcCfg.ref_sel = ADC_REFERENCE_INTERNAL;
    AdcCfg.channels = 0x01;
    AdcCfg.route.pad_to_sample = 1;
    AdcCfg.clk_sel = ADC_SAMPLE_CLK_24M_DIV13;
    AdcCfg.clk_div = 0x3f;
    adc_init(&AdcCfg);
    adc_enable(NULL, NULL, 0);
    
    os_timer_start(&vbatAdcTimer, 100, 0);
}
void vbat_adc_callback(void *p)
{
    uint16_t adc_value = 0;
    adc_get_result(ADC_TRANS_SOURCE_VBAT, 0x01, &adc_value);
    vbat_mV = KalmanFilter(&vbatFilter, adc_value * 480 / 100);     
    // co_printf("adc_value:%d vbat_mV:%dmV\r\n", adc_value,(int)vbat_mV);
    routed_adc_to_vbat();
}
uint16_t get_battery_voltage_mV()
{
    return (uint16_t)vbat_mV;
}
float get_battery_level()
{
    return ((vbat_mV - (float)BATTERY_MIN_VOLTAGE) * 100.f / (float)(BATTERY_MAX_VOLTAGE - BATTERY_MIN_VOLTAGE));
}
int powerControlInit()
{
    system_set_port_mux(PWR_5VEN_PIN, 0X00);
    gpio_set_dir(PWR_5VEN_PIN, GPIO_DIR_OUT);
    os_timer_init(&vbatAdcTimer, vbat_adc_callback, 0);
    routed_adc_to_vbat();
    KalmanFilterInit(&vbatFilter);
    co_printf("powerControlInit...\r\n");
    return 0;
}
// ZINO_INIT_DEVICE_EXPORT(powerControlInit);
uint8_t ENABLE_5V_POWER()
{
    gpio_set_pin_value(PWR_5VEN_PIN, PWR_5VEN_ENABLE);
    return (gpio_get_pin_value(PWR_5VEN_PIN) == PWR_5VEN_ENABLE);
}
uint8_t DISABLE_5V_POWER()
{
    gpio_set_pin_value(PWR_5VEN_PIN, PWR_5VEN_DISABLE);
    return (gpio_get_pin_value(PWR_5VEN_PIN) == PWR_5VEN_DISABLE);
}
