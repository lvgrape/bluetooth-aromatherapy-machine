/**
 * Copyright (c) 2019, Freqchip
 *
 * All rights reserved.
 *
 *
 */
#ifndef _DRIVER_CODEC_H
#define _DRIVER_CODEC_H

 /*
  * INCLUDES (包含头文件)
  */
#include <stdint.h>

  /*
   * MACROS (宏定义)
   */
   /** @defgroup CODEC_SAMPLE_RATE_DEFINES
    * @{
    */
#define CODEC_SAMPLE_RATE_48000         0x44
#define CODEC_SAMPLE_RATE_44100         0xBB
#define CODEC_SAMPLE_RATE_24000         0x33
#define CODEC_SAMPLE_RATE_16000         0x22
#define CODEC_SAMPLE_RATE_8000          0x00
typedef union {
    struct {//@0x00
        uint8_t HPF_EN : 1;//  Digital ADC highass filter enable 1= enable
        uint8_t DSM_MODE : 1;//  Digital dsm module; 1=1+z^(-3) 0=1+z^(-1)
        uint8_t SCRAMBLE_EN : 1;//  Digital dsm out DEM module enable; 1=enable
        uint8_t DITHER_EN : 1;//   Digital dsm dither enable; 1=enable
        uint8_t INTRST : 1;//   nrst of int data path
        uint8_t DECRST : 1;//   nrst of dec data path
        uint8_t EN_INT : 1;//  The enable signal for the interpolating filter and sigma-delta modulator. When “1”, the interpolate filter is enabled.
        uint8_t EN_DEC : 1;//  The clock enable or data valid signal. When “1”, the decimation filter is enabled
    };
    uint8_t value;
}codec_reg00_t;
typedef union {
    struct {//@0x01
        uint8_t DITHEROW : 7;//R R/W 7’b0000100 Dither power
        uint8_t REV : 1;
    };
    uint8_t value;
}codec_reg01_t;
typedef union {
    struct {//@0x02
        uint8_t DITHEROW;//R R/W 8’b1111_1101 Dither power
    };
    uint8_t value;
}codec_reg02_t;
typedef union {
    struct {//@0x03 
        uint8_t DITHEROW;//R R/W 8’hF3 Dither power
    };
    uint8_t value;
}codec_reg03_t;
typedef union {
    struct {//@0x04
        uint8_t RSV1 : 1;//- - -  Reserved
        uint8_t INT_MUTE_R : 1;//R R/W 1’b0 Right interpolate filter mute
        uint8_t RSV0 : 3;//- - - Reserved
        uint8_t SDTG_R : 3;//R R/W 3’b000 Gain control of the right channel side tone.
    };
    uint8_t value;
}codec_reg04_t;
typedef union {
    struct {
        uint8_t INT_VOL : 4;/*The volume control for the right interpolate filter.*/
        uint8_t RSV : 4;
    };
    uint8_t value;
}codec_reg05_t;
typedef union {
    struct {//@0x06
        /*
        The volume control for the right interpolate
        filter. The int_vol range from [0,4095], the
        gain is calculated by
        29 , eg. default
        value is about 0.89
         */
        uint8_t INT_VOL_R;
    };
    uint8_t value;
}codec_reg06_t;
typedef union {
    struct {//@0x05
        // R R/W 2’b10
        // 11 = DSP Mode
        // 10 = I2S Format
        // 01 = Left justified
        // 00 = Right justified
        uint8_t FMT : 2;
        /*
        R R/W 2’b00
        11 = 32 bits
        10 = 24 bits
        01 = 20 bits
        00 = 16 bits
         */
        uint8_t DCI_WL : 2;
        /**
        R R/W 1’b0
        Right, Left and I2S modes – LRCLK
        1 = invert LRCLK polarity
        0 = normal LRCLK polarity
        DSP Mode – mode A/B select
        1 = MSB is available on 1st BCLK rising edge
        after LRC rising edge (mode B)
        0 = MSB is available on 2nd BCLK rising edge
        after LRC rising edge (mode A)
         */
        uint8_t LRP : 1;
        /*
        R R/W 1’b1
        1 = swap left and right DAC data in audio
        interface
        0 = output left and right data as normal
         */
        uint8_t LRSWAP : 1;
        uint8_t RSV : 1;
        /*
        R R/W 1’b0
        0 = BCLK not inverted
        1 = BCLK inverted
         */
        uint8_t BCLKINV : 1;
    };
    uint8_t value;
}codec_reg09_t;
typedef union {
    struct {//0X0A
        /*
        R R/W 4’b0
        DEC Sample Rate Control
        4’b0000: 8K
        4’b0001: 12K
        4’b0010: 16K
        4’b0011: 24K
        4’b0100: 48K
        4’b1000: 8.0214
        4’b1001: 11.0259
        4’b1010: 22.0588
        4’b1011: 44.1
         */
        uint8_t INT_SR : 4;
        /*
        R R/W 4’b0
        INT Sample Rate Control
        the same as above
         */
        uint8_t DEC_SR : 4;
    };
    uint8_t value;
}codec_reg0a_t;
typedef union {
    struct {//0X0B
        /*
        - - 2’b00 Reserved
         */
        uint8_t RSV : 2;
        /*
        R R/W 1’b0
        tst_resin [23:0] can be used as test control
        for analog (whenenable , DITHEROW[22:0]
        has no use)
         */
        uint8_t TEST_RES_MOD : 1;
        /*
        R R/W 4’b0000 Change digital in/out for test
         */
        uint8_t TEST_MOD : 5;
    };
    uint8_t value;
}codec_reg0b_t;
typedef union {
    struct {
        /*
        R R/W 2’b11
        The ramp up control for VMID
        00: slowest
        01: Slow
        10: Fast
        11: Fastest
         */
        uint8_t VMID_CTL : 2;
        /*
        R R/W 3’b000
        BIAS current control
        00: default
        01: 20%
        10: 40%
        11: -20%
         */
        uint8_t BIAS_IB_IS : 2;
        /*
        R R/W 1’b0
        1: MIC bias control enable
        0: MIC bias control disable
         */
        uint8_t MICBIAS_CTL : 1;
        /*
        R R/W 1’b0
        1: Low analog voltage enable
        0: Low analog voltage disable
         */
        uint8_t LVVDDA_EN : 1;
        /*
        R R/W 1’b0
        1: codec bias mode
        0: normal mode
        */
        uint8_t CODEC_BIAS_MODE : 1;
    };
    uint8_t value;
}codec_reg0c_t;
typedef union {
    struct {
        /* ADC current control  */
        uint8_t ADC_ICTL : 1;
        /* ADC dither input */
        uint8_t ADC_DITHER_IN : 1;
        /* ADC dither enable */
        uint8_t ADC_DITHER_EN : 1;
        /* ADC DEM enable */
        uint8_t ADC_DEM_EN : 1;
        /* "The amplitude setting of the dithering feed"*/
        uint8_t ADC_DITHER_AMP : 2;
        /*ADC output encoding mode*/
        uint8_t ADC_ENCODE_MODE : 1;
        uint8_t RSV : 1;
    };
    uint8_t value;
}codec_reg0e_t;
typedef union {
    struct {
        /**DAC current settings */
        uint8_t DAC_IS : 4;
        /**PA current setting */
        uint8_t PA_IS : 2;
        /*
        Mixer cap control
        00: min cap setting
        01: min + 25% setting
        10: max - 25% setting
        11: max cap setting
        */
        uint8_t MIX_CAP_CTL : 2;
    };
    uint8_t value;
}codec_reg0f_t;
typedef union {
    struct {
        uint8_t LPAVOL : 6;/**Left channel power amplifier setting. More reference follow tables. */
        uint8_t RSV : 2;
    };
    uint8_t value;
}codec_reg10_t;
typedef union {
    struct {
        uint8_t RPAVOL : 6;/**Right channel power amplifier setting. More reference follow tables. */
        uint8_t RSV : 2;
    };
    uint8_t value;
}codec_reg11_t;
typedef union {
    struct {
        /*
        Reserved
         */
        uint8_t RSV1 : 1;
        /*
        "1: Right DAC channel power done
        0: Right DAC channel power on"
         */
        uint8_t RDACD : 1;
        /*
        "1: ADC channel power done
        0: ADC channel power on"
         */
        uint8_t ADCD : 1;
        /*
        "1: Power amplifier power done
        0: Power amplifier power on"
         */
        uint8_t PADD : 1;
        /*
        Reserved
         */
        uint8_t RSV2 : 1;
        /*
        "1: Right DAC mute enable
        0: Right DAC mute disable"
         */
        uint8_t RDAC_MUTE : 1;
        /*
        "1: PGA mute enable
        0: PAG mute disable"
         */
        uint8_t PGA_MUTE : 1;
        /*
        Reserved
         */
        uint8_t PARES : 1;
    };
    uint8_t value;
}codec_reg12_t;
typedef union {
    struct {
        /*

         */
        uint8_t RSV1 : 2;
        /*
        R R/W 2’b11
        1: Right PA mute enable
        0: Right PA mute disable
         */
        uint8_t RPA_MUTE : 2;
        /*
         */
        uint8_t RSV2;
        /*
        R R/W 2’b11
        1: Right mixer mute enable
        0: Right mixer mute disable
         */
        uint8_t RMIX_MUTE;
    };
    uint8_t value;
}codec_reg13_t;
typedef union {
    struct {
        uint8_t RSV1 : 2;
        /*
        1: Right power amplifier power done
        0: Right power amplifier power on
         */
        uint8_t RPAD : 2;
        uint8_t RSV2 : 2;
        /*
        1: Right mixer power done
        0: Right mixer power on
         */
        uint8_t RMIXD : 2;
    };
    uint8_t value;
}codec_reg14_t;
typedef union {
    struct {    // 0x15 - 0x1F - R/W: 0b0000_0001_1111, RSV: 0b
        /*
        "1: PGA stage1 power done
        0: PGA stage1 power on"
         */
        uint8_t PGA_STG1D : 1;
        /*
        "1: PGA stage2 power done
        0: PGA stage2 power on"
         */
        uint8_t PGA_STG2D : 1;
        /*
        "1: PGA stage2 inverter power done
        0: PGA stage2 inverter power on"
         */
        uint8_t PGA_STG2_INVD : 1;
        /*
        "1: MICBIAS power done
        0: MICBIAS power on"
         */
        uint8_t MICBIASD : 1;
        /*
        "1: BIAS power done
        0: BIAS power on"
         */
        uint8_t BIASD : 1;
        /*
        "Cross-zero volume update enable for
        power amplifier"
         */
        uint8_t PA_CZEN : 1;
        /*
        "DAC channel clock select
        1: posedge
        0: negedge"
         */
        uint8_t DAC_CLK_EDGE : 1;
        uint8_t Rev : 1;
    };
    uint8_t value;
}codec_reg15_t;
typedef union {
    struct {
        /* */
        uint8_t RSV1 : 1;
        /* Right DAC to mixer enable*/
        uint8_t RDAC2MIX_EN : 1;
        /* Mixer current settings*/
        uint8_t MIX_IS : 2;
        /* DAC to mixer gain*/
        uint8_t DAC2MIX_GAIN : 1;
        /* */
        uint8_t RSV2 : 3;
    };
    uint8_t value;
}codec_reg17_t;
typedef union {
    struct {
        /* PGA current settings*/
        uint8_t PGA_ICTL : 2;
        /* PGA input P enable*/
        uint8_t PGA_INP_EN : 1;
        /* PGA input N enable*/
        uint8_t PGA_INN_EN : 1;
        /* PGA input P/N exchange*/
        uint8_t PGA_INN_INP_EXCHG : 1;
        /* For internal use*/
        uint8_t VMID_RAMPD : 1;
        /* For internal use*/
        uint8_t VMIDD : 1;
        /* */
        uint8_t Rev : 1;
    };
    uint8_t value;
}codec_reg18_t;
typedef union {
    struct {
        uint8_t PAG_STG1_VOL : 4;/**PGA gain detail setting, details refer to following tables */
        uint8_t PAG_STG2_VOL : 2;/**PGA boost gain setting */
        uint8_t Rev : 2;
    };
    uint8_t value;
}codec_reg19_t;
typedef union {
    struct {
        /*
        0: select right channel of I2S
        1: select left channel of I2S
         */
        uint8_t CHANNEL_CP : 1;
        uint8_t REV1 : 1;
        /*
        0: left channel has no data
        1: copy the right channel to left channel
         */
        uint8_t DEC_CP : 1;
        uint8_t REV2 : 4;
    };
    uint8_t value;
}codec_reg25_t;
enum CODEC_REG_ADDR {
    CODEC_REG00_ADDR = 0,
    CODEC_REG01_ADDR,
    CODEC_REG02_ADDR,
    CODEC_REG03_ADDR,
    CODEC_REG04_ADDR,
    CODEC_REG05_ADDR,
    CODEC_REG06_ADDR,
    CODEC_REG07_ADDR,
    CODEC_REG08_ADDR,
    CODEC_REG09_ADDR,
    CODEC_REG0a_ADDR,
    CODEC_REG0b_ADDR,
    CODEC_REG0c_ADDR,
    CODEC_REG0d_ADDR,
    CODEC_REG0e_ADDR,
    CODEC_REG0f_ADDR,
    CODEC_REG10_ADDR,
    CODEC_REG11_ADDR,
    CODEC_REG12_ADDR,
    CODEC_REG20_ADDR,
    CODEC_REG21_ADDR,
    CODEC_REG22_ADDR,
    CODEC_REG23_ADDR,
    CODEC_REG24_ADDR,
    CODEC_REG25_ADDR,
};
/*
 * PUBLIC FUNCTIONS (全局函数)
 */

 /*********************************************************************
  * @fn      codec_enable_adc
  *
  * @brief   Enable the adc (aka MIC input) channel of codec.
  *
  * @param   None.
  *
  * @return  None.
  */
void codec_enable_adc(void);

/*********************************************************************
 * @fn      codec_disable_adc
 *
 * @brief   Disable the adc (aka MIC input) channel of codec.
 *
 * @param   None.
 *
 * @return  None.
 */
void codec_disable_adc(void);

/*********************************************************************
 * @fn      codec_enable_dac
 *
 * @brief   Enable the dac (aka speaker output) channel of codec.
 *
 * @param   None.
 *
 * @return  None.
 */
void codec_enable_dac(void);

/*********************************************************************
 * @fn      codec_disable_dac
 *
 * @brief   Disable the dac (aka speaker output) channel of codec.
 *
 * @param   None.
 *
 * @return  None.
 */
void codec_disable_dac(void);

/*********************************************************************
 * @fn      codec_set_vol
 *
 * @brief   set the volumn of speaker.
 *
 * @param   value   - volumn target, should be set from 0x00(min) to 0x3f(max).
 *
 * @return  None.
 */
void codec_set_vol(uint8_t value);

/*********************************************************************
 * @fn      codec_init
 *
 * @brief   init the codec module.
 *
 * @param   sample_rate - which sample rate should the codec work at, such as
 *                        CODEC_SAMPLE_RATE_8000.
 *
 * @return  None.
 */
void codec_init(uint8_t sample_rate);

void audio_speaker_codec_init(void);
void speaker_codec_init(uint8_t sample_rate);


#endif  // _DRIVER_CODEC_H

