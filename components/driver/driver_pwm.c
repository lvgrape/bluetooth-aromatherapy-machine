#include <stdint.h>

#include "driver_plf.h"
#include "driver_system.h"
#include "driver_pwm.h"

struct pwm_ctrl_t {
    uint32_t EN:1;/** 1= PWM_CNTR start run, 0= PWM_CNTR stop run and reset */
    uint32_t HTRC_SEL:1;/** 1= Enable bit8 defined function, 0= HRC/TRC value will be updated when PWM_CNTR reach*/
    uint32_t LEVEL:1;/** 1= PWM output as reversed value, 0= PWM output normal*/
    uint32_t OE:1;/** 1= Enable PWM output, 0= Disable PWM output*/
    uint32_t SINGLE:1;/** 1= PWM_CNTR is not incremented after it reaches PWM_TRC value,  0= PWM_CNTR is restarted after it reaches value in the PWM_TRC register.*/
    uint32_t Reserved:2;/** Reserved*/
    uint32_t RST:1;/** 1= PWM_CNTR is reset, 0= PWM_CNTR is normal*/
    uint32_t SW_LOAD:1;/** 1= HRC/TRC value is updated immediately, 0= HRC/TRC value will be updated when PWM_CNTR reach*/
    uint32_t reservaed1:23;/** Reserved*/
};

struct pwm_elt_t {
    /**
     * @brief PWM counter value.
     * PWM_CNTR register is the actual counter register. It is incremented at every
     * counter clock cycle.
     * In order to count, PWM_CNTR must first be enabled with PWM_CTRL[EN].
     * PWM_CNTR can be reset by PWM_CTRL[RST].
     * PWM_CNTR can operate in either single-run mode or free-run mode. Mode is
     * selected by PWM_CTRL[SINGLE].
     */
    uint32_t CNT;
    /**
     * @brief High Reference register.
     * it is used to assert high PWM output.
     * The PWM_HRC should have lower value than PWM_TRC. This is because
     * PWM output goes first high and later low
     */
    uint32_t HRC;
    /**
     * @brief Total Reference register
     * it is used to assert low PWM output.
     * The PWM_TRC should have higher value than PWM_HRC. This is because
     * PWM output goes first high and later low.
     */
    uint32_t TRC;
    struct pwm_ctrl_t PWM_CTRL;
};

struct pwm_regs_t {
    struct pwm_elt_t channel[PWM_CHANNEL_MAX];
};

struct pwm_regs_t *pwm_ctrl = (struct pwm_regs_t *)PWM_BASE;

void pwm_init(enum pwm_channel_t channel, uint32_t frequency, uint8_t high_duty)
{
    uint32_t total_count;
    uint32_t high_count;

    total_count = system_get_pclk() / frequency;
    if(high_duty < 100) {
        high_count = total_count * (100-high_duty) / 100;
    }
    else {
        return;
    }

    *(uint32_t *)&pwm_ctrl->channel[channel].PWM_CTRL = 0;
    pwm_ctrl->channel[channel].TRC = total_count;
    pwm_ctrl->channel[channel].HRC = high_count;
}

void pwm_update(enum pwm_channel_t channel, uint32_t frequency, uint8_t high_duty)
{
    uint32_t total_count;
    uint32_t high_count;
    GLOBAL_INT_DISABLE();

    total_count = system_get_pclk() / frequency;
    if(high_duty < 100) {
        high_count = total_count * (100-high_duty) / 100;
    }
    else {
        GLOBAL_INT_RESTORE();
        return;
    }

    pwm_ctrl->channel[channel].TRC = total_count;
    pwm_ctrl->channel[channel].HRC = high_count;
    GLOBAL_INT_RESTORE();
}
/**
 * @brief 
 * 
 * @param channel 0~5
 * @param high_duty 0~1000
 */
void pwm_update_duty(enum pwm_channel_t channel, uint16_t high_duty)
{
    uint32_t total_count;
    uint32_t high_count;
    GLOBAL_INT_DISABLE();

    total_count = pwm_ctrl->channel[channel].TRC;
    if(high_duty < 1000) {
        high_count = total_count * (1000-high_duty) / 1000;
    }
    else {
        GLOBAL_INT_RESTORE();
        return;
    }
    pwm_ctrl->channel[channel].HRC = high_count;
    GLOBAL_INT_RESTORE();
}
void pwm_start(enum pwm_channel_t channel)
{
    pwm_ctrl->channel[channel].PWM_CTRL.OE = 1;
    pwm_ctrl->channel[channel].PWM_CTRL.EN = 1;
}

void pwm_stop(enum pwm_channel_t channel)
{
    pwm_ctrl->channel[channel].PWM_CTRL.EN = 0;
    pwm_ctrl->channel[channel].PWM_CTRL.OE = 0;
}


