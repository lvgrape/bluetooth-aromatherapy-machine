/**
 * Copyright (c) 2019, Freqchip
 *
 * All rights reserved.
 *
 *
 */

/*
 * INCLUDES
 */
#include <stdint.h>

#include "driver_codec.h"
#include "driver_frspim.h"

#define codec_write(addr, data)         frspim_wr(FR_SPI_CODEC_CHAN, addr, 1, (uint32_t)data)
#define codec_read(addr)                (uint8_t)frspim_rd(FR_SPI_CODEC_CHAN, addr, 1)
/** write reg value form ram to physics, XX=00 - 25*/
#define CODEC_REG_W(XX) codec_write(0x##XX,reg##XX##_.value)
/** load physics reg value to ram, XX=00 - 25*/
#define CODEC_REG_R(XX)  reg##XX##_.value=codec_read(0x##XX)
static codec_reg00_t reg00_={0};
static codec_reg01_t reg01_={0};
static codec_reg02_t reg02_={0};
static codec_reg03_t reg03_={0};
static codec_reg04_t reg04_={0};
static codec_reg05_t reg05_={0};
static codec_reg06_t reg06_={0};
// static codec_reg07_t reg07={0};
// static codec_reg08_t reg08={0};
static codec_reg09_t reg09_={0};
static codec_reg0a_t reg0a_={0};
static codec_reg0b_t reg0b_={0};
static codec_reg0c_t reg0c_={0};
// static codec_reg0d_t reg0d={0};
static codec_reg0e_t reg0e_={0};
static codec_reg0f_t reg0f_={0};
static codec_reg10_t reg10_={0};
static codec_reg11_t reg11_={0};
static codec_reg12_t reg12_={0};
static codec_reg13_t reg13_={0};
static codec_reg14_t reg14_={0};
static codec_reg15_t reg15_={0};
// static codec_reg16_t reg16={0};
static codec_reg17_t reg17_={0};
static codec_reg18_t reg18_={0};
static codec_reg19_t reg19_={0};
// static codec_reg1a_t reg1a={0};
// static codec_reg1b_t reg1b={0};
// static codec_reg1c_t reg1c={0};
// static codec_reg1d_t reg1d={0};
// static codec_reg1e_t reg1e={0};
// static codec_reg1f_t reg1f={0};
// static codec_reg20_t reg20={0};
// static codec_reg21_t reg21={0};
// static codec_reg22_t reg22={0};
// static codec_reg23_t reg23={0};
// static codec_reg24_t reg24={0};
static codec_reg25_t reg25_={0};
static void codec_hw_set_freq(uint8_t nValue)
{

    codec_write(CODEC_REG0b_ADDR, 0x00);
    codec_write(CODEC_REG0b_ADDR, nValue);
    reg0b_.TEST_RES_MOD = 1;
    codec_write(CODEC_REG0b_ADDR, reg0b_.value);

    // codec_reg0b_t reg0b={0};
    // codec_reg0a_t reg0a={0};
    // codec_write(CODEC_REG0B_ADDR, 0x00);
    // reg0a.DEC_SR = nValue>>4;
    // reg0a.INT_SR = nValue&0x0F;
    // reg0b.TEST_RES_MOD = 1;
    // codec_write(CODEC_REG0A_ADDR, reg0a);
    // codec_write(CODEC_REG0B_ADDR, reg0b);
}

void codec_enable_adc(void)
{
    codec_write(0x15, codec_read(0x15) & 0xE0); //unmute mic    0xE0
    codec_write(0x12, codec_read(0x12) & 0xBB);
    codec_write(0x0c, 0x1f);    //mic bias control
    // codec_reg15_t reg15={.}
    
    CODEC_REG_R(15);
    CODEC_REG_R(12);

    reg15_.PGA_STG1D=0;
    reg15_.PGA_STG2D=0;
    reg15_.PGA_STG2_INVD=0;
    reg15_.MICBIASD=0;
    reg15_.BIASD=0;

    reg12_.PGA_MUTE=0;
    reg12_.ADCD=0;

    reg0c_.VMID_CTL=3;
    reg0c_.BIAS_IB_IS=3;

    CODEC_REG_W(15);
    CODEC_REG_W(12);
}

void codec_disable_adc(void)
{
    codec_write(0x15, codec_read(0x15) | 0x1F);
    codec_write(0x12, codec_read(0x12) | 0x44);
}

void codec_enable_dac(void)
{
    codec_write(0x14, 0x00);
    codec_write(0x12, codec_read(0x12)&0xC4);
    codec_write(0x13, 0x00);
}

void codec_disable_dac(void)
{
    codec_write(0x14, 0xff);
    codec_write(0x12, codec_read(0x12)|0x3b);
    codec_write(0x13, 0xff);
}
/**
 * @brief 
 * 
 * @param value 0~32
 */
void codec_set_vol(uint8_t value)
{
    //NOTE 音量反着来的，127是最小声，95是最大声
    if(value>32)value=32;
    value = 127-value;
    codec_write(0x10, value);
    codec_write(0x11, value);
}

void codec_init(uint8_t sample_rate)
{
    codec_write(0x15, 0x7F);    //pd all
    codec_write(0x0c, 0x1f);    //mic bias control
    codec_write(0x0d, 0x03);    //LMIX RMIX PD
    codec_write(0x0e, 0x3c);    //ADC control
    codec_write(0x0f, 0xd1);
    codec_write(0x10, 0x20);
    codec_write(0x11, 0x20);
    codec_write(0x12, 0xff);    //pd
    codec_write(0x13, 0xff);    //mute
    codec_write(0x14, 0xff);    //pd
    codec_write(0x15, 0x3f);    //pd all
    codec_write(0x16, 0x00);
    codec_write(0x17, 0x07);
    codec_write(0x18, 0x9c);
    codec_write(0x1a, 0x00);
    codec_write(0x00, 0xf5);
    codec_write(0x25, 0x02);
    codec_write(0x19, 0x1f);    //gain, 0x3F ==> noise is big;  0x1F ==> lower volume. result = stg2 * 4 + stg1 *10, 0x2A
    
    codec_write(0x09, 0x02);    //SLAVE_I2S
    codec_write(0x40, 0x81);    //CODEC_STEREO
    
    codec_hw_set_freq(sample_rate);
}

static uint8_t current_speaker_vol_value = 0x01;
void audio_speaker_codec_init(void)
{
    codec_write(0x10, current_speaker_vol_value);
    codec_write(0x11, current_speaker_vol_value);   //0x25
    codec_write(0x12, 0xc4);
    codec_write(0x13, 0x00);
    codec_write(0x14, 0x00);
    codec_write(0x15, 0x0f);
    codec_write(0x16, 0x00);
    codec_write(0x17, 0x07);
    codec_write(0x18, 0x0c);
    codec_write(0x19, 0xb7);
    codec_write(0x1a, 0x00);
    codec_write(0x1d, 0x45);
    codec_write(0x00, 0x55);
    codec_write(0x24, 0x00);
    codec_write(0x25, 0x00);
    codec_write(0x19, 0xb7);    //gain,
    codec_write(0x09, 0x02);    //SLAVE_I2S
    codec_write(0x40, 0x83);    //CODEC_STEREO
    codec_write(0x0b, 0x03);    //set freq to 16000
    codec_hw_set_freq(CODEC_SAMPLE_RATE_8000);
}
void speaker_codec_init(uint8_t sample_rate)
{
    // codec_write(0x10, current_speaker_vol_value);//Left channel power amplifier setting. max:32
    // codec_write(0x11, current_speaker_vol_value);//Right channel power amplifier setting. max:32
    codec_write(0x12, 0xc4);//PGA mute enable, ADC channel power done
    codec_write(0x13, 0x00);//Right mixer mute disable,Right PA mute disable
    codec_write(0x14, 0x00);
    codec_write(0x15, 0x0f);
    codec_write(0x16, 0x00);
    codec_write(0x17, 0x07);
    codec_write(0x18, 0x0c);
    codec_write(0x19, 0xb7);
    codec_write(0x1a, 0x00);
    codec_write(0x1d, 0x45);
    codec_write(0x00, 0x55);
    codec_write(0x24, 0x00);
    codec_write(0x25, 0x00);
    codec_write(0x19, 0xb7);    //gain,
    codec_write(0x09, 0x02);    //SLAVE_I2S
    codec_write(0x40, 0x83);    //CODEC_STEREO
    codec_write(0x0b, 0x03);    //set freq to 16000
    codec_hw_set_freq(sample_rate);
}

void codec_adc_init(uint8_t sample_rate)
{
    codec_write(0x15, 0x7F);    //pd all
    codec_write(0x0c, 0x1f);    //mic bias control
    codec_write(0x0d, 0x03);    //LMIX RMIX PD
    codec_write(0x0e, 0x3c);    //ADC control
    codec_write(0x0f, 0xd1);
    codec_write(0x10, 0x20);
    codec_write(0x11, 0x20);
    codec_write(0x12, 0xff);    //pd
    codec_write(0x13, 0xff);    //mute
    codec_write(0x14, 0xff);    //pd
    codec_write(0x15, 0x3f);    //pd all
    codec_write(0x16, 0x00);
    codec_write(0x17, 0x07);
	
    codec_write(0x18, 0x18);//PGA  P/N exchange, P enable  N enable
    codec_write(0x1a, 0x00);
    codec_write(0x00, 0xa5);
    codec_write(0x25, 0x02);
    codec_write(0x19, 0x17);    //gain
    
    codec_write(0x09, 0x02);    //SLAVE_I2S
    codec_write(0x40, 0x81);    //CODEC_STEREO
    
    codec_write(0x00, codec_read(0x00) & 0xfe);   
    
    codec_hw_set_freq(sample_rate);
}


