/*
 * @Author: LVGRAPE
 * @Date: 2023-05-30 18:29:42
 * @LastEditTime: 2023-08-29 14:32:02
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE_V1\components\ble\profiles\ble_woodmaster\woodmaster_service.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
 /**
  * Copyright (c) 2019, Freqchip
  *
  * All rights reserved.
  *
  *
  */

  /*
   * INCLUDES (包含头文件)
   */
#include <stdio.h>
#include <string.h>
#include "co_printf.h"
#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"
#include "woodmaster_service.h"
#include "ble_dev.h"

rcval_t rcValue;

#define WOODMASTER_STATE_VALUE_LEN      8
#define WOODMASTER_STATE_DESC_LEN       18
#define WOODMASTER_RCVAL_VALUE_LEN      12
#define WOODMASTER_RCCMD_VALUE_LEN      12
#define WOODMASTER_RCVAL_DESC_LEN       18
#define WOODMASTER_RCCFG_VALUE_LEN      8
#define WOODMASTER_RCCFG_DESC_LEN       17
#define WOODMASTER_RCCMD_DESC_LEN       17
#define WOODMASTER_CTRL_VALUE_LEN       16
#define WOODMASTER_TEXT_VALUE_LEN       24
#define WOODMASTER_CTRL_DESC_LEN        8
#define WOODMASTER_TEXT_DESC_LEN        10
const uint8_t woodmaster_svc_uuid[] = UUID16_ARR(WOODMASTER_SVC_UUID);
const uint8_t woodmaster_state_desc[WOODMASTER_STATE_DESC_LEN]  = "woodmaster status";
const uint8_t woodmaster_rcval_desc[WOODMASTER_RCVAL_DESC_LEN]  = "rc channel values";
const uint8_t woodmaster_rccfg_desc[WOODMASTER_RCCFG_DESC_LEN]  = "RC channel cofig";
const uint8_t woodmaster_rccmd_desc[WOODMASTER_RCCMD_DESC_LEN]  = "RC cmd channel";
const uint8_t woodmaster_ctrl_desc[WOODMASTER_CTRL_DESC_LEN]    = "control";
const uint8_t woodmaster_text_desc[WOODMASTER_TEXT_DESC_LEN]    = "any text";
static uint16_t stateNotifyValu;
static uint8_t woodmaster_state_value_bf[WOODMASTER_STATE_VALUE_LEN];
static uint8_t woodmaster_rcval_value_bf[WOODMASTER_RCVAL_VALUE_LEN];
static uint8_t woodmaster_rccfg_value_bf[WOODMASTER_RCCFG_VALUE_LEN];
static uint8_t woodmaster_ctrl_value_bf[WOODMASTER_CTRL_VALUE_LEN];
static uint8_t woodmaster_text_value_bf[WOODMASTER_TEXT_VALUE_LEN];

uint8_t woodmaster_svc_id = 0;
// uint8_t ntf_char1_enable = 0;
static gatt_service_t woodmaster_profile_svc;

const gatt_attribute_t woodmaster_att_table[WOODMASTER_IDX_NB] =
{
    // woodmaster Service Declaration
    GATT_ATT_SVC16(WOODMASTER_IDX_SERVICE,woodmaster_svc_uuid),
    /**att state r/w/n */
    GATT_ATT_DECL(WOODMASTER_IDX_STATE_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_STATE_VALUE,WOODMASTER_STATE_UUID,GATT_PROP_READ | GATT_PROP_NOTI,WOODMASTER_STATE_VALUE_LEN),
    GATT_ATT_CCCD(WOODMASTER_IDX_STATE_CFG,GATT_PROP_READ | GATT_PROP_WRITE),
    GATT_ATT_DESC(WOODMASTER_IDX_STATE_USER_DESCRIPTION,WOODMASTER_STATE_DESC_LEN, woodmaster_state_desc),
    /**att rcval r/w */
    GATT_ATT_DECL(WOODMASTER_IDX_RCVAL_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_RCVAL_VALUE,WOODMASTER_RCVAL_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_RCVAL_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_IDX_RCVAL_USER_DESCRIPTION,WOODMASTER_RCVAL_DESC_LEN, woodmaster_rcval_desc),
    /**att rccfg r/w */
    GATT_ATT_DECL(WOODMASTER_IDX_RCCFG_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_RCCFG_VALUE,WOODMASTER_RCCFG_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_RCCFG_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_IDX_RCCFG_USER_DESCRIPTION,WOODMASTER_RCCFG_DESC_LEN,woodmaster_rccfg_desc),
    /**att rccfg r/w */
    GATT_ATT_DECL(WOODMASTER_IDX_RCCMD_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_RCCMD_VALUE,WOODMASTER_RCCMD_UUID,GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTI,WOODMASTER_RCCMD_VALUE_LEN),
    GATT_ATT_CCCD(WOODMASTER_IDX_RCCMD_CFG,GATT_PROP_READ | GATT_PROP_WRITE),
    GATT_ATT_DESC(WOODMASTER_IDX_RCCMD_USER_DESCRIPTION,WOODMASTER_RCCMD_DESC_LEN,woodmaster_rccfg_desc),
    /**att ctrl r/w */
    GATT_ATT_DECL(WOODMASTER_IDX_CTRL_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_CTRL_VALUE,WOODMASTER_CTRL_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_CTRL_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_IDX_CTRL_USER_DESCRIPTION,WOODMASTER_CTRL_DESC_LEN,woodmaster_ctrl_desc),
    
    GATT_ATT_DECL(WOODMASTER_IDX_TEXT_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_IDX_TEXT_VALUE,WOODMASTER_TEXT_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_TEXT_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_IDX_TEXT_USER_DESCRIPTION,WOODMASTER_TEXT_DESC_LEN,woodmaster_text_desc),

};

/*********************************************************************
 * @brief   woodmaster Profile user application handles read request in this callback.
 *			应用层在这个回调函数里面处理读的请求。
 *
 * @param   p_msg->param.msg.p_msg_data  - the pointer to read buffer. NOTE: It's just a pointer from lower layer, please create the buffer in application layer.
 *					  指向读缓冲区的指针。 请注意这只是一个指针，请在应用程序中分配缓冲区. 为输出函数, 因此为指针的指针.
 *          p_msg->param.msg.msg_len     - the pointer to the length of read buffer. Application to assign it.
 *       读缓冲区的长度，用户应用程序去给它赋值.
 *          p_msg->att_idx - index of the attribute value in it's attribute table.
 *					  Attribute的偏移量.
 *
 * @return  读请求的长度.
 */
uint16_t woodmaster_read_req_callback(gatt_msg_t* p_msg)
{
    co_printf("\t read: %d %d %d\r\n",p_msg->conn_idx,p_msg->handle,p_msg->svc_id);
    switch (p_msg->att_idx)
    {
    case WOODMASTER_IDX_STATE_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_state_value_bf, WOODMASTER_STATE_VALUE_LEN);
        co_printf("status\r\n");
        return WOODMASTER_STATE_VALUE_LEN;
        break;
    case WOODMASTER_IDX_RCVAL_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rcval_value_bf, WOODMASTER_RCVAL_VALUE_LEN);
        co_printf("rcval\r\n");
        return WOODMASTER_RCVAL_VALUE_LEN;
    case WOODMASTER_IDX_RCCFG_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rccfg_value_bf, WOODMASTER_RCCFG_VALUE_LEN);
        co_printf("rccfg\r\n");
        return WOODMASTER_RCCFG_VALUE_LEN;
    case WOODMASTER_IDX_CTRL_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_ctrl_value_bf, WOODMASTER_CTRL_VALUE_LEN);
        co_printf("rccfg\r\n");
        return WOODMASTER_CTRL_VALUE_LEN;
    default:
        co_printf("unknow idx:%d\r\n", p_msg->att_idx);
        break;
    }
    return 0;
}
/*********************************************************************
 * @brief   Simple Profile user application handles write request in this callback.
 *			应用层在这个回调函数里面处理写的请求。
 *
 * @param   p_msg->param.msg.p_msg_data   - the buffer for write
 *			 写操作的数据.
 *
 *          p_msg->param.msg.msg_len      - the length of write buffer.
 *           写缓冲区的长度.
 *          att_idx     - index of the attribute value in it's attribute table.
 *					      Attribute的偏移量.
 *
 * @return  写请求的长度.
 */
uint16_t woodmaster_write_req_callback(gatt_msg_t* p_msg)
{
    // co_printf("\t write: idx:%d ",p_msg->att_idx);
    // printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
    switch (p_msg->att_idx)
    {
    case WOODMASTER_IDX_STATE_CFG:
        co_printf("state notify\r\n");
        memcpy(&stateNotifyValu, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        // printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
        break;
    case WOODMASTER_IDX_RCVAL_VALUE:
        co_printf("rcval\r\n");
        memcpy(woodmaster_rcval_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        rcValueHandle();
        break;
    case WOODMASTER_IDX_RCCFG_VALUE:
        co_printf("rccfg\r\n");
        // printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
        memcpy(woodmaster_rccfg_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    case WOODMASTER_IDX_CTRL_VALUE:
        co_printf("rcctrl\r\n");
        // printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
        memcpy(woodmaster_ctrl_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    default:
        co_printf("unknow idx:%d\r\n", p_msg->att_idx);
        // printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
        break;
    }
    return p_msg->param.msg.msg_len;
}
/*********************************************************************
 * @fn      woodmaster_gatt_msg_handler
 *
 * @brief   Simple Profile callback funtion for GATT messages. GATT read/write
 *			operations are handeled here.
 *
 * @param   p_msg       - GATT messages from GATT layer.
 *
 * @return  uint16_t    - Length of handled message.
 */
static uint16_t woodmaster_gatt_msg_handler(gatt_msg_t* p_msg)
{
    co_printf("woodmaster msg: \r\n");
    switch (p_msg->msg_evt)
    {
    case GATTC_MSG_READ_REQ:
        return woodmaster_read_req_callback(p_msg);
        break;
    case GATTC_MSG_WRITE_REQ:
        woodmaster_write_req_callback(p_msg);
        break;
    case GATTC_MSG_LINK_CREATE:
        co_printf("link_created\r\n");
        break;
    case GATTC_MSG_LINK_LOST:
        co_printf("link_lost\r\n");
        // ntf_char1_enable = 0;
        break;
    default:
        break;
    }
    return p_msg->param.msg.msg_len;
}

/*********************************************************************
 * @fn      woodmaster_gatt_add_service
 *
 * @brief   Simple Profile add GATT service function.
 *			添加GATT service到ATT的数据库里面。
 *
 * @param   None.
 *
 *
 * @return  None.
 */
void woodmaster_gatt_add_service(void)
{
    woodmaster_profile_svc.p_att_tb = woodmaster_att_table;
    woodmaster_profile_svc.att_nb = WOODMASTER_IDX_NB;
    woodmaster_profile_svc.gatt_msg_handler = woodmaster_gatt_msg_handler;
    woodmaster_svc_id = gatt_add_service(&woodmaster_profile_svc);
}

void rcValueHandle()
{
    if(woodmaster_rcval_value_bf[0]==0x01)
    {
        static uint16_t dt_pre;
        memcpy(rcValue.axis.values, &woodmaster_rcval_value_bf[1], 10);
        uint16_t dt = rcValue.axis.values[4] - dt_pre;
        co_printf("%04d %04d %04d %04d %05d - %04d\r\n",rcValue.axis.values[0],rcValue.axis.values[1],rcValue.axis.values[2],rcValue.axis.values[3],rcValue.axis.values[4], dt);
        dt_pre = rcValue.axis.values[4];
    }
    else if(woodmaster_rcval_value_bf[0]==0x02)
    {
        memcpy(&rcValue.keys, &woodmaster_rcval_value_bf[1], 4);
    }
    else if(woodmaster_rcval_value_bf[0]==0x03)
    {
        rcValue.keyActions[woodmaster_rcval_value_bf[1]]=woodmaster_rcval_value_bf[2];
    }
}




