/**
 * Copyright (c) 2019, Freqchip
 * 
 * All rights reserved.
 * 
 * 
 */

#ifndef WOODMASTER_GATT_PROFILE_H
#define WOODMASTER_GATT_PROFILE_H

/*
 * INCLUDES (包含头文件)
 */
#include <stdio.h>
#include <string.h>
#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"


/*
 * MACROS (宏定义)
 */

/*
 * CONSTANTS (常量定义)
 */
// Simple Profile attributes index. 
enum
{
    WOODMASTER_IDX_SERVICE=0,

    WOODMASTER_IDX_STATE_DECLARATION,
    WOODMASTER_IDX_STATE_VALUE,//状态信息，R/N
	WOODMASTER_IDX_STATE_CFG,
    WOODMASTER_IDX_STATE_USER_DESCRIPTION,

    WOODMASTER_IDX_RCVAL_DECLARATION,
    WOODMASTER_IDX_RCVAL_VALUE,//RC CHANNEL R/W
    WOODMASTER_IDX_RCVAL_USER_DESCRIPTION,

    WOODMASTER_IDX_RCCFG_DECLARATION,
    WOODMASTER_IDX_RCCFG_VALUE,//RC CHANNEL CFG R/W
    WOODMASTER_IDX_RCCFG_USER_DESCRIPTION,

    WOODMASTER_IDX_RCCMD_DECLARATION,
    WOODMASTER_IDX_RCCMD_VALUE,//RC CHANNEL CFG R/W
    WOODMASTER_IDX_RCCMD_CFG,//RC CHANNEL CFG R/W
    WOODMASTER_IDX_RCCMD_USER_DESCRIPTION,

    WOODMASTER_IDX_CTRL_DECLARATION,
    WOODMASTER_IDX_CTRL_VALUE,//CTRL R/W
    WOODMASTER_IDX_CTRL_USER_DESCRIPTION,

    WOODMASTER_IDX_TEXT_DECLARATION,
    WOODMASTER_IDX_TEXT_VALUE,//CTRL R/W
    WOODMASTER_IDX_TEXT_USER_DESCRIPTION,

    WOODMASTER_IDX_NB,
};

// Simple GATT Profile Service UUID
#define WOODMASTER_SVC_UUID              0xFFE0

#define WOODMASTER_STATE_UUID           0xFFF1 //woodmaster status
#define WOODMASTER_RCVAL_UUID           0xFFF2 //rc raw channel
#define WOODMASTER_RCCFG_UUID           0xFFF3 //RC config
#define WOODMASTER_RCCMD_UUID           0xFFF4 //RC CMD OUTPUT
#define WOODMASTER_CTRL_UUID            0xFFF5 //CTRL
#define WOODMASTER_TEXT_UUID            0xFFF6 //TEXT

typedef union
{
    struct {
        int16_t x;
        int16_t y;
        int16_t z;
        int16_t w;
        uint16_t c;
    };
    int16_t values[5];
}axis4_t;
/*
 * TYPEDEFS (类型定义)
 */
typedef struct {
    axis4_t axis;
    uint32_t keys;
    uint8_t keyActions[16];
}rcval_t;
/*
 * GLOBAL VARIABLES (全局变量)
 */
extern uint8_t woodmaster_svc_id;
extern rcval_t rcValue;
/*
 * LOCAL VARIABLES (本地变量)
 */


/*
 * PUBLIC FUNCTIONS (全局函数)
 */
/*********************************************************************
 * @fn      woodmaster_gatt_add_service
 *
 * @brief   Simple Profile add GATT service function.
 *			添加GATT service到ATT的数据库里面。
 *
 * @param   None. 
 *        
 *
 * @return  None.
 */
void woodmaster_gatt_add_service(void);
void rcValueHandle();


#endif







