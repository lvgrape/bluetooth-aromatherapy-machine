/**
 * Copyright (c) 2019, Freqchip
 *
 * All rights reserved.
 *
 *
 */

#ifndef DEV_INFO_SERVICE_H
#define DEV_INFO_SERVICE_H

/*
 * INCLUDES (包含头文件)
 */
#include <stdint.h>

/*
 * MACROS (宏定义)
 */

/// Manufacturer Name Value
#ifndef DIS_MANUFACTURER_NAME
#define DIS_MANUFACTURER_NAME       ("\x0055\x0041\x0056\x0049\x0020\x0054\x0045\x0043\x0048")
#endif // !DIS_MANUFACTURER_NAME
#define DIS_MANUFACTURER_NAME_LEN   (sizeof(DIS_MANUFACTURER_NAME))

/// Model Number String Value
#ifndef DIS_MODEL_NB_STR
// #define DIS_MODEL_NB_STR            ("WOODMASTER")
#error "DIS_MODEL_NB_STR"
#endif // !DIS_MODEL_NB_STR_LEN
#define DIS_MODEL_NB_STR_LEN        (sizeof(DIS_MODEL_NB_STR))

/// Firmware Revision
#ifndef DIS_FIRM_REV_STR
// #define DIS_FIRM_REV_STR            ("0.0.1")
#error "DIS_FIRM_REV_STR"
#endif // !DIS_FIRM_REV_STR
#define DIS_FIRM_REV_STR_LEN        (sizeof(DIS_FIRM_REV_STR))

/// System ID Value - LSB -> MSB
#ifndef DIS_SYSTEM_ID
#define DIS_SYSTEM_ID               ("\x02\x54\x36\xFF\xFE\x9A\xBC\xDE")
#endif // !DIS_SYSTEM_ID
#define DIS_SYSTEM_ID_LEN           (sizeof(DIS_SYSTEM_ID))

/// Software Revision String
#ifndef DIS_SW_REV_STR
#define DIS_SW_REV_STR              ("0.0.0")
#endif // !DIS_SW_REV_STR
#define DIS_SW_REV_STR_LEN          (sizeof(DIS_SW_REV_STR))

/**
 * PNP ID Value - LSB -> MSB
 *      Vendor ID Source : 0x02 (USB Implementer Forum assigned Vendor ID value)
 *      Vendor ID : 0x045E      (Microsoft Corp)
 *      Product ID : 0x0040
 *      Product Version : 0x0300
 */
#define DIS_PNP_ID               ("\x01\x17\x27\xb0\x32\x10\x24")
#define DIS_PNP_ID_LEN           (7)

/// Serial Number
#define DIS_SERIAL_NB_STR           ("COM5")
#define DIS_SERIAL_NB_STR_LEN       (5)

/// Hardware Revision String
#ifndef DIS_HARD_REV_STR
#define DIS_HARD_REV_STR           ("0.0.1")
#endif // !DIS_HARD_REV_STR
#define DIS_HARD_REV_STR_LEN       (sizeof(DIS_HARD_REV_STR))

/// IEEE
#define DIS_IEEE                    ("\xFF\xEE\xDD\xCC\xBB\xAA")
#define DIS_IEEE_LEN                (6)



///Indicate if Manufacturer Name String Char. is supported
#define    DIS_MANUFACTURER_NAME_CHAR_SUP       0x0001
    ///Indicate if Model Number String Char. is supported
#define    DIS_MODEL_NB_STR_CHAR_SUP            0x0002
    ///Indicate if Serial Number String Char. is supported
#define    DIS_SERIAL_NB_STR_CHAR_SUP           0x0004
    ///Indicate if Hardware Revision String Char. supports indications
#define    DIS_HARD_REV_STR_CHAR_SUP            0x0008
    ///Indicate if Firmware Revision String Char. is writable
#define    DIS_FIRM_REV_STR_CHAR_SUP            0x0010
    ///Indicate if Software Revision String Char. is writable
#define    DIS_SW_REV_STR_CHAR_SUP              0x0020
    ///Indicate if System ID Char. is writable
#define    DIS_SYSTEM_ID_CHAR_SUP               0x0040
    ///Indicate if IEEE Char. is writable
#define    DIS_IEEE_CHAR_SUP                    0x0080
    ///Indicate if PnP ID Char. is writable
#define    DIS_PNP_ID_CHAR_SUP                  0x0100
    ///All features are supported
#define    DIS_ALL_FEAT_SUP                     0x01FF

#define DIS_FEATURES             (DIS_MANUFACTURER_NAME_CHAR_SUP    | \
                                      DIS_HARD_REV_STR_CHAR_SUP     | \
                                      DIS_MODEL_NB_STR_CHAR_SUP     | \
                                      DIS_SERIAL_NB_STR_CHAR_SUP    | \
                                      DIS_SYSTEM_ID_CHAR_SUP        | \
                                      DIS_FIRM_REV_STR_CHAR_SUP     | \
                                      DIS_SW_REV_STR_CHAR_SUP       | \
                                      DIS_PNP_ID_CHAR_SUP)

/*
 * CONSTANTS (常量定义)
 */
// Simple Profile attributes index.
enum
{
    DIS_IDX_SERVICE,

#if ((DIS_FEATURES & DIS_MANUFACTURER_NAME_CHAR_SUP) == DIS_MANUFACTURER_NAME_CHAR_SUP)
    DIS_IDX_MANUFACTURER_NAME_CHAR_DECLARATION,
    DIS_IDX_MANUFACTURER_NAME_VALUE,
#endif

#if ((DIS_FEATURES & DIS_MODEL_NB_STR_CHAR_SUP) == DIS_MODEL_NB_STR_CHAR_SUP)
    DIS_IDX_MODEL_NUMBER_CHAR_DECLARATION,
    DIS_IDX_MODEL_NUMBER_VALUE,
#endif

#if ((DIS_FEATURES & DIS_SERIAL_NB_STR_CHAR_SUP) == DIS_SERIAL_NB_STR_CHAR_SUP)
    DIS_IDX_SERIAL_NUMBER_CHAR_DECLARATION,
    DIS_IDX_SERIAL_NUMBER_VALUE,
#endif

#if ((DIS_FEATURES & DIS_HARD_REV_STR_CHAR_SUP) == DIS_HARD_REV_STR_CHAR_SUP)
    DIS_IDX_HARDWARE_REVISION_CHAR_DECLARATION,
    DIS_IDX_HARDWARE_REVISION_VALUE,
#endif

#if ((DIS_FEATURES & DIS_FIRM_REV_STR_CHAR_SUP) == DIS_FIRM_REV_STR_CHAR_SUP)
    DIS_IDX_FIRMWARE_REVISION_CHAR_DECLARATION,
    DIS_IDX_FIRMWARE_REVISION_VALUE,
#endif

#if ((DIS_FEATURES & DIS_SW_REV_STR_CHAR_SUP) == DIS_SW_REV_STR_CHAR_SUP)
    DIS_IDX_SOFTWARE_REVISION_CHAR_DECLARATION,
    DIS_IDX_SOFTWARE_REVISION_VALUE,
#endif

#if ((DIS_FEATURES & DIS_SYSTEM_ID_CHAR_SUP) == DIS_SYSTEM_ID_CHAR_SUP)
    DIS_IDX_SYSTEM_ID_CHAR_DECLARATION,
    DIS_IDX_SYSTEM_ID_VALUE,
#endif

#if ((DIS_FEATURES & DIS_IEEE_CHAR_SUP) == DIS_IEEE_CHAR_SUP)
    DIS_IDX_IEEE_11073_CHAR_DECLARATION,
    DIS_IDX_IEEE_11073_VALUE,
#endif

#if ((DIS_FEATURES & DIS_PNP_ID_CHAR_SUP) == DIS_PNP_ID_CHAR_SUP)
    DIS_IDX_PNP_ID_CHAR_DECLARATION,
    DIS_IDX_PNP_ID_VALUE,
#endif

    DIS_IDX_NB,
};


/*
 * TYPEDEFS (类型定义)
 */

/*
 * GLOBAL VARIABLES (全局变量)
 */
extern uint8_t dis_svc_id;

/*
 * LOCAL VARIABLES (本地变量)
 */


/*
 * PUBLIC FUNCTIONS (全局函数)
 */
/*********************************************************************
* @fn      dis_gatt_add_service
*
* @brief   Create device information server.
*
* @param   None.
* 
* @return  None.
*/
void dis_gatt_add_service(void);



#endif







