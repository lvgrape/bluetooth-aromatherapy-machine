/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-31 14:24:44
 * @LastEditTime: 2023-06-05 17:46:07
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\components\ble\profiles\ble_woodmaster_rc\woodmaster_rc_service.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef WOODMASTER_RC_RC_GATT_PROFILE_H
#define WOODMASTER_RC_RC_GATT_PROFILE_H

/*
 * INCLUDES (包含头文件)
 */
#include <stdio.h>
#include <string.h>
#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"


/*
 * MACROS (宏定义)
 */

/*
 * CONSTANTS (常量定义)
 */
// Simple Profile attributes index. 
enum
{
    WOODMASTER_RC_IDX_SERVICE,

    WOODMASTER_RC_IDX_STATE_DECLARATION,
    WOODMASTER_RC_IDX_STATE_VALUE,//状态信息，R/N
	WOODMASTER_RC_IDX_STATE_CFG,
    WOODMASTER_RC_IDX_STATE_USER_DESCRIPTION,

    WOODMASTER_RC_IDX_RCVAL_DECLARATION,
    WOODMASTER_RC_IDX_RCVAL_VALUE,//RC CHANNEL R/W
    WOODMASTER_RC_IDX_RCVAL_CFG,//RC CHANNEL R/W
    WOODMASTER_RC_IDX_RCVAL_USER_DESCRIPTION,

    WOODMASTER_RC_IDX_RCCFG_DECLARATION,
    WOODMASTER_RC_IDX_RCCFG_VALUE,//RC CHANNEL CFG R/W
    WOODMASTER_RC_IDX_RCCFG_USER_DESCRIPTION,

    WOODMASTER_RC_IDX_CTRL_DECLARATION,
    WOODMASTER_RC_IDX_CTRL_VALUE,//CTRL R/W
    WOODMASTER_RC_IDX_CTRL_USER_DESCRIPTION,

    WOODMASTER_RC_IDX_NB,
};

// Simple GATT Profile Service UUID
#define WOODMASTER_RC_SVC_UUID              0xFFE1

#define WOODMASTER_RC_STATE_UUID           0xFFC1 //woodmaster status
#define WOODMASTER_RC_RCVAL_UUID           0xFFC2 //rc raw channel
#define WOODMASTER_RC_RCCMD_UUID           0xFFC3 //RC CMD OUTPUT
#define WOODMASTER_RC_RCCFG_UUID           0xFFC4 //RC config
#define WOODMASTER_RC_CTRL_UUID            0xFFC5 //state cmd
#define WOODMASTER_RC_TURRET_UUID          0xFFC6 //state cmd
#define WOODMASTER_RC_TURRET_CFG_UUID      0xFFC7 //state cmd
#define WOODMASTER_RC_FOOTHBAL_UUID        0xFFC8 //state cmd
#define WOODMASTER_RC_FOOTHBAL_CFG_UUID    0xFFC9 //state cmd


/*
 * TYPEDEFS (类型定义)
 */

/*
 * GLOBAL VARIABLES (全局变量)
 */
extern uint8_t woodmaster_rc_svc_id;
/*
 * LOCAL VARIABLES (本地变量)
 */


/*
 * PUBLIC FUNCTIONS (全局函数)
 */
/*********************************************************************
 * @fn      woodmaster_gatt_add_service
 *
 * @brief   Simple Profile add GATT service function.
 *			添加GATT service到ATT的数据库里面。
 *
 * @param   None. 
 *        
 *
 * @return  None.
 */
void woodmaster_rc_gatt_add_service(void);



#endif







