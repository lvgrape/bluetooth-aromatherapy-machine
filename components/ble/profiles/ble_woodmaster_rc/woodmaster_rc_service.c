/*
 * @Author: LVGRAPE
 * @Date: 2023-05-30 18:29:42
 * @LastEditTime: 2023-06-07 16:58:35
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \ZINO_BLE\components\ble\profiles\ble_woodmaster_rc\woodmaster_rc_service.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
 /**
  * Copyright (c) 2019, Freqchip
  *
  * All rights reserved.
  *
  *
  */

  /*
   * INCLUDES (包含头文件)
   */
#include <stdio.h>
#include <string.h>
#include "co_printf.h"
#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"
#include "woodmaster_rc_service.h"
#include "ble_dev.h"

#define WOODMASTER_RC_STATE_VALUE_LEN   8
#define WOODMASTER_RC_STATE_DESC_LEN    18
#define WOODMASTER_RC_RCVAL_VALUE_LEN   12
#define WOODMASTER_RC_RCVAL_DESC_LEN    18
#define WOODMASTER_RC_RCCFG_VALUE_LEN   8
#define WOODMASTER_RC_RCCFG_DESC_LEN    17
#define WOODMASTER_RC_CTRL_VALUE_LEN    16
#define WOODMASTER_RC_CTRL_DESC_LEN     8

const uint8_t woodmaster_rc_svc_uuid[] = UUID16_ARR(WOODMASTER_RC_SVC_UUID);
const uint8_t woodmaster_rc_state_desc[WOODMASTER_RC_STATE_DESC_LEN] = "woodmaster status";
const uint8_t woodmaster_rc_rcval_desc[WOODMASTER_RC_RCVAL_DESC_LEN] = "rc channel values";
const uint8_t woodmaster_rc_rccfg_desc[WOODMASTER_RC_RCCFG_DESC_LEN] = "RC channel cofig";
const uint8_t woodmaster_rc_ctrl_desc[WOODMASTER_RC_CTRL_DESC_LEN] = "control";
static uint16_t stateNotifyValu;
static uint8_t woodmaster_rc_state_value_bf[WOODMASTER_RC_STATE_VALUE_LEN];
static uint8_t woodmaster_rc_rcval_value_bf[WOODMASTER_RC_RCVAL_VALUE_LEN];
static uint8_t woodmaster_rc_rccfg_value_bf[WOODMASTER_RC_RCCFG_VALUE_LEN];
static uint8_t woodmaster_rc_ctrl_value_bf[WOODMASTER_RC_CTRL_VALUE_LEN];

uint8_t woodmaster_rc_svc_id = 0;
// uint8_t ntf_char1_enable = 0;

/*
 * LOCAL VARIABLES (本地变量)
 */
static gatt_service_t woodmaster_rc_profile_svc;




const gatt_attribute_t woodmaster_rc_att_table[WOODMASTER_RC_IDX_NB] =
{
    // woodmaster Service Declaration
    GATT_ATT_SVC16(WOODMASTER_RC_IDX_SERVICE,woodmaster_rc_svc_uuid),
    /**att state r/w/n */
    GATT_ATT_DECL(WOODMASTER_RC_IDX_STATE_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_RC_IDX_STATE_VALUE,WOODMASTER_RC_STATE_UUID,GATT_PROP_READ | GATT_PROP_NOTI,WOODMASTER_RC_STATE_VALUE_LEN),
    GATT_ATT_CCCD(WOODMASTER_RC_IDX_STATE_CFG,GATT_PROP_READ | GATT_PROP_WRITE),
    GATT_ATT_DESC(WOODMASTER_RC_IDX_STATE_USER_DESCRIPTION,WOODMASTER_RC_STATE_DESC_LEN, woodmaster_rc_state_desc),
    /**att rcval r/w */
    GATT_ATT_DECL(WOODMASTER_RC_IDX_RCVAL_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_RC_IDX_RCVAL_VALUE,WOODMASTER_RC_RCVAL_UUID,GATT_PROP_READ | GATT_PROP_NOTI,WOODMASTER_RC_RCVAL_VALUE_LEN),
    GATT_ATT_CCCD(WOODMASTER_RC_IDX_RCVAL_CFG,GATT_PROP_READ | GATT_PROP_WRITE),
    GATT_ATT_DESC(WOODMASTER_RC_IDX_RCVAL_USER_DESCRIPTION,WOODMASTER_RC_RCVAL_DESC_LEN, woodmaster_rc_rcval_desc),
    /**att rccfg r/w */
    GATT_ATT_DECL(WOODMASTER_RC_IDX_RCCFG_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_RC_IDX_RCCFG_VALUE,WOODMASTER_RC_RCCFG_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_RC_RCCFG_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_RC_IDX_RCCFG_USER_DESCRIPTION,WOODMASTER_RC_RCCFG_DESC_LEN,woodmaster_rc_rccfg_desc),
    /**att ctrl r/w */
    GATT_ATT_DECL(WOODMASTER_RC_IDX_CTRL_DECLARATION),
    GATT_ATT_VALU(WOODMASTER_RC_IDX_CTRL_VALUE,WOODMASTER_RC_CTRL_UUID,GATT_PROP_READ | GATT_PROP_WRITE,WOODMASTER_RC_CTRL_VALUE_LEN),
    GATT_ATT_DESC(WOODMASTER_RC_IDX_CTRL_USER_DESCRIPTION,WOODMASTER_RC_CTRL_DESC_LEN,woodmaster_rc_ctrl_desc),

};

/*********************************************************************
 * @brief   woodmaster Profile user application handles read request in this callback.
 *			应用层在这个回调函数里面处理读的请求。
 *
 * @param   p_msg->param.msg.p_msg_data  - the pointer to read buffer. NOTE: It's just a pointer from lower layer, please create the buffer in application layer.
 *					  指向读缓冲区的指针。 请注意这只是一个指针，请在应用程序中分配缓冲区. 为输出函数, 因此为指针的指针.
 *          p_msg->param.msg.msg_len     - the pointer to the length of read buffer. Application to assign it.
 *       读缓冲区的长度，用户应用程序去给它赋值.
 *          p_msg->att_idx - index of the attribute value in it's attribute table.
 *					  Attribute的偏移量.
 *
 * @return  读请求的长度.
 */
uint16_t woodmaster_rc_read_req_callback(gatt_msg_t* p_msg)
{
    co_printf("\t read: \r\n");
    switch (p_msg->att_idx)
    {
    case WOODMASTER_RC_IDX_STATE_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rc_state_value_bf, WOODMASTER_RC_STATE_VALUE_LEN);
        co_printf("status\r\n");
        return WOODMASTER_RC_STATE_VALUE_LEN;
        break;
    case WOODMASTER_RC_IDX_RCVAL_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rc_rcval_value_bf, WOODMASTER_RC_RCVAL_VALUE_LEN);
        co_printf("rcval\r\n");
        return WOODMASTER_RC_RCVAL_VALUE_LEN;
    case WOODMASTER_RC_IDX_RCCFG_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rc_rccfg_value_bf, WOODMASTER_RC_RCCFG_VALUE_LEN);
        co_printf("rccfg\r\n");
        return WOODMASTER_RC_RCCFG_VALUE_LEN;
    case WOODMASTER_RC_IDX_CTRL_VALUE:
        memcpy(p_msg->param.msg.p_msg_data, woodmaster_rc_ctrl_value_bf, WOODMASTER_RC_CTRL_VALUE_LEN);
        co_printf("rccfg\r\n");
        return WOODMASTER_RC_CTRL_VALUE_LEN;
    default:
        co_printf("unknow idx:%d\r\n", p_msg->att_idx);
        break;
    }
    return 0;
}
/*********************************************************************
 * @brief   Simple Profile user application handles write request in this callback.
 *			应用层在这个回调函数里面处理写的请求。
 *
 * @param   p_msg->param.msg.p_msg_data   - the buffer for write
 *			 写操作的数据.
 *
 *          p_msg->param.msg.msg_len      - the length of write buffer.
 *           写缓冲区的长度.
 *          att_idx     - index of the attribute value in it's attribute table.
 *					      Attribute的偏移量.
 *
 * @return  写请求的长度.
 */
uint16_t woodmaster_rc_write_req_callback(gatt_msg_t* p_msg)
{
    co_printf("\t write: \r\n");
    printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
    switch (p_msg->att_idx)
    {
    case WOODMASTER_RC_IDX_STATE_CFG:
        co_printf("state notify\r\n");
        memcpy(&stateNotifyValu, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    case WOODMASTER_RC_IDX_RCVAL_VALUE:
        co_printf("rcval\r\n");
        memcpy(woodmaster_rc_rcval_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    case WOODMASTER_RC_IDX_RCCFG_VALUE:
        co_printf("rccfg\r\n");
        memcpy(woodmaster_rc_rccfg_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    case WOODMASTER_RC_IDX_CTRL_VALUE:
        co_printf("rcctrl\r\n");
        memcpy(woodmaster_rc_ctrl_value_bf, p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len);
        break;
    default:
        co_printf("unknow idx:%d\r\n", p_msg->att_idx);
        break;
    }
}
/*********************************************************************
 * @fn      woodmaster_rc_gatt_msg_handler
 *
 * @brief   Simple Profile callback funtion for GATT messages. GATT read/write
 *			operations are handeled here.
 *
 * @param   p_msg       - GATT messages from GATT layer.
 *
 * @return  uint16_t    - Length of handled message.
 */
static uint16_t woodmaster_rc_gatt_msg_handler(gatt_msg_t* p_msg)
{
    co_printf("woodmaster rc msg: \r\n");
    switch (p_msg->msg_evt)
    {
    case GATTC_MSG_READ_REQ:
        return woodmaster_rc_read_req_callback(p_msg);
        break;
    case GATTC_MSG_WRITE_REQ:
        woodmaster_rc_write_req_callback(p_msg);
        break;
    case GATTC_MSG_LINK_CREATE:
        co_printf("link_created\r\n");
        break;
    case GATTC_MSG_LINK_LOST:
        co_printf("link_lost\r\n");
        // ntf_char1_enable = 0;
        break;
    default:
        break;
    }
    return p_msg->param.msg.msg_len;
}

/*********************************************************************
 * @fn      woodmaster_rc_gatt_add_service
 *
 * @brief   Simple Profile add GATT service function.
 *			添加GATT service到ATT的数据库里面。
 *
 * @param   None.
 *
 *
 * @return  None.
 */
void woodmaster_rc_gatt_add_service(void)
{
    woodmaster_rc_profile_svc.p_att_tb = woodmaster_rc_att_table;
    woodmaster_rc_profile_svc.att_nb = WOODMASTER_RC_IDX_NB;
    woodmaster_rc_profile_svc.gatt_msg_handler = woodmaster_rc_gatt_msg_handler;
    woodmaster_rc_svc_id = gatt_add_service(&woodmaster_rc_profile_svc);
}





