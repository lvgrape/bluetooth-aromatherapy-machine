/*
 * INCLUDE FILES
 ****************************************************************************************
 */
#include <stdio.h>
#include <string.h>
#include "gatt_api.h"
#include "gatt_sig_uuid.h"
#include "sys_utils.h"
#include "woodmaster_client.h"
#include "woodmaster_service.h"
#include "ble_dev.h"
// #include "woodmaster_rc.h"
#include "zino_config.h"
#include "zino.h"
ZNIO_CONFIG_DECLARE(woodmaster_state_t, woodmaster_config);
static woodmaster_state_t* config;
uint8_t woodmaster_client_id;
/**
 * @brief write cmd , write req都会返回，没信就不返回了，
 * 这时再发送就会消息阻塞了。确保上一次成功后再发送！
 */
static uint8_t last_op_state = 0xff;
const uint8_t WOODMASTER_SVC_UUID_GROUP[] = UUID16_ARR(WOODMASTER_SVC_UUID);
static uint16_t woodmaster_client_handles[6] =
{
    71,//Idx[0] handle:71 prop:18 uuid:0xFFF1
    75,//Idx[1] handle:75 prop:14 uuid:0xFFF2
    78,//Idx[2] handle:78 prop:14 uuid:0xFFF3
    81,//Idx[3] handle:81 prop:30 uuid:0xFFF4
    85,//Idx[4] handle:85 prop:14 uuid:0xFFF5
    88,//Idx[5] handle:88 prop:14 uuid:0xFFF6
};
static gatt_uuid_t woodmaster_client_att_tb[6] =
{
    [0] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_STATE_UUID)},//71
    [1] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_RCVAL_UUID)},//75
    [2] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_RCCFG_UUID)},//81
    [3] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_RCCMD_UUID)},//78
    [4] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_CTRL_UUID)},
    [5] = { UUID_SIZE_2, UUID16_ARR(WOODMASTER_TEXT_UUID)},

};
struct woodmaster_client_att {
    uint8_t handle; //handle of the charateristic in the descriptor.
    uint8_t prop;
    uint16_t uuid;
};

struct woodmaster_client_att woodmaster_client_att_list[WOODMASTER_CLT_IDX_NB] =
{
    [0] = {.handle = 71, .prop = 18, .uuid = 0xFFF1},
    [1] = {.handle = 75, .prop = 14, .uuid = 0xFFF2},
    [2] = {.handle = 78, .prop = 14, .uuid = 0xFFF3},
    [3] = {.handle = 81, .prop = 30, .uuid = 0xFFF4},
    [4] = {.handle = 85, .prop = 14, .uuid = 0xFFF5},
    [5] = {.handle = 88, .prop = 14, .uuid = 0xFFF6},
};
void get_woodmaster_client_att_list(gatt_svc_report_t* svc_rpt)
{
    uint16_t uuid = *(uint16_t*)(svc_rpt->uuid);
    if (uuid == WOODMASTER_SVC_UUID)
    {
        uint8_t wd_idx = 0;
        bool gotDecl = 0;
        for (uint16_t i = 0; i <= (svc_rpt->end_hdl - svc_rpt->start_hdl); i++)
        {
            // co_printf("\n info[%d].att_type:%d\r\n",i,svc_rpt->info[i].att_type);
            switch (svc_rpt->info[i].att_type)
            {
            case ATT_TYPE_NONE:
                break;
            case ATT_TYPE_INC_SVC:      /// Included Service
                break;
            case ATT_TYPE_CHAR_DECL:     /// Characteristic Declaration
                if (gotDecl)//next charateristic declaration is found.
                {
                    wd_idx++;
                }
                woodmaster_client_att_list[wd_idx].handle = svc_rpt->info[i].char_decl.handle;
                woodmaster_client_att_list[wd_idx].prop = svc_rpt->info[i].char_decl.prop;
                woodmaster_client_handles[wd_idx] = woodmaster_client_att_list[wd_idx].handle;
                gotDecl = 1;
                break;
            case ATT_TYPE_VAL:     /// Characteristic Attribute Value
                if (svc_rpt->info[i].att_value.uuid_len == 2)
                {
                    woodmaster_client_att_list[wd_idx].uuid = *(uint16_t*)svc_rpt->info[i].att_value.uuid;
                }
                break;
            case ATT_TYPE_DESC:     /// Characteristic Attribute Descriptor

                break;
            default:
                break;
            }
            // if (svc_rpt->info[i].att_type > ATT_TYPE_DESC)
            // {
            //     co_printf("\n info[%d].att_type:%d\r\n", i + svc_rpt->start_hdl, svc_rpt->info[i].att_type);
            //     continue;
            // }
            // co_printf("\n [%d]:%s\t", i + svc_rpt->start_hdl, svc_att_type_str(svc_rpt->info[i].att_type));
            // // co_printf("\n info[%d].att_type:%s\r\n",i+svc_rpt->start_hdl,svc_att_type_str(svc_rpt->info[i].att_type));
            // if (svc_rpt->info[i].att_type == ATT_TYPE_CHAR_DECL)
            // {
            //     // co_printf("[%d],char_decl, prop:0x%X,handle:%d ",i+svc_rpt->start_hdl,svc_rpt->info[i].char_decl.prop,svc_rpt->info[i].char_decl.handle);
            //     // co_printf("",svc_rpt->info[i].att_type,svc_rpt->info[i].char_decl.att_type,svc_rpt->info[i].char_decl)
            //     co_printf("handle:%d ", svc_rpt->info[i].char_decl.handle);
            //     get_gatt_prop_str(svc_rpt->info[i].char_decl.prop);
            //     co_printf("\n");
            // }

            // if (svc_rpt->info[i].att_type == ATT_TYPE_VAL)
            // {
            //     co_printf("uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
            //     // co_printf("[%d],char, uuid_len:%d,uuid:",i+svc_rpt->start_hdl,svc_rpt->info[i].att_value.uuid_len);
            //     // co_printf("%d ")
            //     if (svc_rpt->info[i].att_value.uuid_len == 2) co_printf("0x%X \n", *(uint16_t*)svc_rpt->info[i].att_value.uuid);
            //     else
            //         show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
            // }
            // if (svc_rpt->info[i].att_type == ATT_TYPE_DESC)
            // {
            //     // co_printf("[%d],char_desc&cfg, uuid_len:%d,uuid:",i+svc_rpt->start_hdl,svc_rpt->info[i].att_value.uuid_len);
            //     co_printf("uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
            //     if (svc_rpt->info[i].att_value.uuid_len == 2) co_printf("0x%X \n", *(uint16_t*)svc_rpt->info[i].att_value.uuid);
            //     else
            //         show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
            // }
        }
        co_printf("woodmaster_client_att_list:\n");
        for (uint8_t i = 0;i < WOODMASTER_CLT_IDX_NB;i++)
        {
            co_printf("Idx[%d] handle:%d prop:%d uuid:0x%04X\n",
                i,
                woodmaster_client_att_list[i].handle,
                woodmaster_client_att_list[i].prop,
                woodmaster_client_att_list[i].uuid
            );
        }
    }


}
void woodmaster_client_read_responsed_callback(gatt_msg_t* p_msg)
{
    co_printf("read responsed: ");
    printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
    co_printf("\r\n");

}
void woodmaster_client_svc_report_callback(gatt_msg_t* p_msg)
{
    gatt_svc_report_t* svc_rpt = (gatt_svc_report_t*)(p_msg->param.msg.p_msg_data);
    co_printf("GATTC_MSG_SVC_REPORT svc_uuid_len:%d,uuid:0x%x,shdl:%d,ehdl:%d\r\n", svc_rpt->uuid_len, *(uint16_t*)(svc_rpt->uuid), svc_rpt->start_hdl, svc_rpt->end_hdl);
#if 1   //show service attributors

    for (uint16_t i = 0; i <= (svc_rpt->end_hdl - svc_rpt->start_hdl); i++)
    {
        // co_printf("\n info[%d].att_type:%d\r\n",i,svc_rpt->info[i].att_type);
        co_printf("\n [%d]:%s\t", i + svc_rpt->start_hdl, svc_att_type_str(svc_rpt->info[i].att_type));
        if (svc_rpt->info[i].att_type > ATT_TYPE_DESC)
        {
            co_printf("info[%d].att_type:%d\r\n", i + svc_rpt->start_hdl, svc_rpt->info[i].att_type);
            // continue;
        }
        // co_printf("\n info[%d].att_type:%s\r\n",i+svc_rpt->start_hdl,svc_att_type_str(svc_rpt->info[i].att_type));
        else if (svc_rpt->info[i].att_type == ATT_TYPE_CHAR_DECL)
        {
            // co_printf("[%d],char_decl, prop:0x%X,handle:%d ",i+svc_rpt->start_hdl,svc_rpt->info[i].char_decl.prop,svc_rpt->info[i].char_decl.handle);
            // co_printf("",svc_rpt->info[i].att_type,svc_rpt->info[i].char_decl.att_type,svc_rpt->info[i].char_decl)
            co_printf("handle:%d ", svc_rpt->info[i].char_decl.handle);
            get_gatt_prop_str(svc_rpt->info[i].char_decl.prop);
            co_printf("\n");
        }

        else if (svc_rpt->info[i].att_type == ATT_TYPE_VAL)
        {
            co_printf("uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
            // co_printf("[%d],char, uuid_len:%d,uuid:",i+svc_rpt->start_hdl,svc_rpt->info[i].att_value.uuid_len);
            // co_printf("%d ")
            if (svc_rpt->info[i].att_value.uuid_len == 2) co_printf("0x%X \n", *(uint16_t*)svc_rpt->info[i].att_value.uuid);
            else
                show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
        }
        else if (svc_rpt->info[i].att_type == ATT_TYPE_DESC)
        {
            // co_printf("[%d],char_desc&cfg, uuid_len:%d,uuid:",i+svc_rpt->start_hdl,svc_rpt->info[i].att_value.uuid_len);
            co_printf("uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
            if (svc_rpt->info[i].att_value.uuid_len == 2) co_printf("0x%X \n", *(uint16_t*)svc_rpt->info[i].att_value.uuid);
            else
                show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
        }
    }
    get_woodmaster_client_att_list(svc_rpt);
#endif

#if 0
    if (memcmp(svc_rpt->uuid, hid_svc_uuid, sizeof(hid_svc_uuid)) == 0)
    {
        co_printf("\n\nhid svc uuid\n");
        for (uint16_t i = 0; i < (svc_rpt->end_hdl - svc_rpt->start_hdl); i++)
        {
            // co_printf("info[%d].att_type:%d %04X\r\n",i,svc_rpt->info[i].att_type,*(uint16_t*)svc_rpt->info[i].att_value.uuid);
            if (svc_rpt->info[i].att_type == ATT_TYPE_CHAR_DECL)
            {
                // co_printf("decl, prop:%x,handle:%d ",svc_rpt->info[i].char_decl.prop,svc_rpt->info[i].char_decl.handle);
                // get_gatt_prop_str(svc_rpt->info[i].char_decl.prop);
                // co_printf("\r\n");
                for (uint8_t att_idx = 0; att_idx < (HID_CLT_ATT_NB); att_idx++)
                {
                    if (hid_client_handles[att_idx] == 0
                        && memcmp(hid_client_att_tb[att_idx].p_uuid, svc_rpt->info[i + 1].att_value.uuid, svc_rpt->info[i + 1].att_value.uuid_len) == 0
                        )
                    {
                        hid_client_props[att_idx] = svc_rpt->info[i].char_decl.prop;
                        hid_client_handles[att_idx] = svc_rpt->info[i].char_decl.handle;
                        co_printf("idx:[%d] UUID:0x%04X handle:%d props:% ", att_idx, *(uint16_t*)hid_client_att_tb[att_idx].p_uuid, hid_client_handles[att_idx]);
                        get_gatt_prop_str(hid_client_props[att_idx]);
                        co_printf("\n");
                        if (memcmp(hid_client_att_tb[att_idx].p_uuid, "\x4D\x2A", 2) == 0)
                        {
                            hid_client_handles[att_idx + 1] = hid_client_handles[att_idx] + 1;
                            hid_client_handles[att_idx + 2] = hid_client_handles[att_idx] + 2;
                            hid_client_props[att_idx + 1] = hid_client_props[att_idx];
                            hid_client_props[att_idx + 2] = hid_client_props[att_idx];
                            co_printf("idx:[%d] = %d\r\n", att_idx + 1, hid_client_handles[att_idx + 1]);
                            co_printf("idx:[%d] = %d\r\n", att_idx + 2, hid_client_handles[att_idx + 2]);
                        }
                        break;
                    }
                }
            }
            else if (svc_rpt->info[i].att_type == ATT_TYPE_VAL)
            {
                co_printf("\t att_val, uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
                show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
            }
            else if (svc_rpt->info[i].att_type == ATT_TYPE_DESC)
            {
                co_printf("\t desc, uuid_len:%d,uuid:", svc_rpt->info[i].att_value.uuid_len);
                show_reg(svc_rpt->info[i].att_value.uuid, svc_rpt->info[i].att_value.uuid_len, 1);
            }
        }
        co_printf("\t hid_client_handles: ");
        show_reg16(hid_client_handles, HID_CLT_ATT_NB, 1);
    }
#endif
}
/**
 * @brief
 *
 * @param s
    one of:
 * @param GATT_OP_NOTIFY
 * @param GATT_OP_INDICA
 * @param GATT_OP_PEER_SVC_REGISTERED
 * @param GATT_OP_WRITE_REQ
 * @param GATT_OP_WRITE_CMD
 * @param GATT_OP_READ
 * @param GATT_OP_PEER_SVC_DISC_END
 */
void woodmaster_op_state_set(uint8_t s)
{
    last_op_state |= (1 << (s - GATT_OP_NOTIFY));
}
/**
 * @brief
 *
 * @param s
    one of:
 * @param GATT_OP_NOTIFY
 * @param GATT_OP_INDICA
 * @param GATT_OP_PEER_SVC_REGISTERED
 * @param GATT_OP_WRITE_REQ
 * @param GATT_OP_WRITE_CMD
 * @param GATT_OP_READ
 * @param GATT_OP_PEER_SVC_DISC_END
 */
void woodmaster_op_state_reset(uint8_t s)
{
    last_op_state &= (1 << (s - GATT_OP_NOTIFY));
}
/**
 * @brief check last op state
 *
 * @param s
    one of:
 * @param GATT_OP_NOTIFY
 * @param GATT_OP_INDICA
 * @param GATT_OP_PEER_SVC_REGISTERED
 * @param GATT_OP_WRITE_REQ
 * @param GATT_OP_WRITE_CMD
 * @param GATT_OP_READ
 * @param GATT_OP_PEER_SVC_DISC_END
 * @return 0 fail, none 0 ok
 */
uint8_t woodmaster_op_state_check(uint8_t s)
{
    return (last_op_state & (1 << (s - GATT_OP_NOTIFY)));
}
void woodmaster_client_msg_complete_callback(gatt_msg_t* p_msg)
{
    // co_printf("GATTC_MSG_CMP_EVT op:%d handle:%d idx:%d svc:%d done\r\n", p_msg->param.op.operation, p_msg->handle, p_msg->att_idx, p_msg->svc_id);
    // hid_client_read(p_msg->conn_idx, readidx++);
    // if (readidx >= 25)readidx = 0;

    switch (p_msg->param.op.operation)
    {
    case GATT_OP_NOTIFY:
        woodmaster_op_state_set(GATT_OP_NOTIFY);
        co_printf("notify  done\r\n");
        break;
    case GATT_OP_INDICA:
        woodmaster_op_state_set(GATT_OP_INDICA);
        co_printf("indicate  done\r\n");
        break;
    case GATT_OP_PEER_SVC_REGISTERED:
        woodmaster_op_state_set(GATT_OP_PEER_SVC_REGISTERED);
        woodmaster_op_state_set(GATT_OP_WRITE_CMD);
        co_printf("GATT_OP_PEER_SVC_REGISTERED  done\r\n");
        break;
    case GATT_OP_WRITE_REQ:
        woodmaster_op_state_set(GATT_OP_WRITE_REQ);
        co_printf("write req done\r\n");
        break;
    case GATT_OP_WRITE_CMD:
        woodmaster_op_state_set(GATT_OP_WRITE_CMD);
        co_printf("write cmd done\r\n");
        break;
    case GATT_OP_READ:
        woodmaster_op_state_set(GATT_OP_READ);
        co_printf("read  done\r\n");
        break;
    case GATT_OP_PEER_SVC_DISC_END:
        woodmaster_op_state_set(GATT_OP_PEER_SVC_DISC_END);
        co_printf("peer svc discovery done\r\n");
        gatt_add_client_uuid(woodmaster_client_att_tb, 6, p_msg->conn_idx, woodmaster_client_handles);
        
        break;
    default:
        co_printf("GATTC_MSG_CMP_EVT op:%d handle:%d idx:%d svc:%d done\r\n", p_msg->param.op.operation, p_msg->handle, p_msg->att_idx, p_msg->svc_id);
        break;
    }
}
uint16_t woodmaster_client_msg_handler(gatt_msg_t* p_msg)
{

    static uint8_t readidx = 0;
    switch (p_msg->msg_evt)
    {
    case GATTC_MSG_NTF_REQ:
        co_printf("notify received:%d ->", p_msg->att_idx);
        printf_hex(p_msg->param.msg.p_msg_data, p_msg->param.msg.msg_len, 1);
        co_printf("\r\n");
        break;
    case GATTC_MSG_READ_IND:
        woodmaster_client_read_responsed_callback(p_msg);
        break;
    case GATTC_MSG_SVC_REPORT:
        woodmaster_client_svc_report_callback(p_msg);
        break;
    case GATTC_MSG_CMP_EVT:
        woodmaster_client_msg_complete_callback(p_msg);
        break;
    default:
        co_printf("\r\n woodmaster_client_msg_handler CCC:%x %s\r\n \r\n", p_msg->msg_evt, get_gatt_msg_str(p_msg->msg_evt));
        break;
    }

    return 0;
}

void woodmaster_client_read(uint8_t conidx, uint8_t att_idx)
{
    if (woodmaster_client_att_list[att_idx].handle != 0)
    {
        gatt_client_read_t read;
        read.conidx = conidx;
        read.client_id = woodmaster_client_id;
        read.att_idx = 0;
        gatt_client_read_with_handle(read, woodmaster_client_att_list[att_idx].handle);
    }
}
void woodmaster_client_write_cmd(uint8_t conidx, uint8_t att_idx, uint8_t* p_data, uint16_t len)
{
    if (woodmaster_client_att_list[att_idx].handle)
    {
        gatt_client_write_t wrt;
        wrt.client_id = woodmaster_client_id;
        wrt.conidx = conidx;
        wrt.att_idx = 0;
        wrt.p_data = p_data;
        wrt.data_len = len;
        gatt_client_write_cmd_with_handle(wrt, woodmaster_client_att_list[att_idx].handle);
    }
}
void woodmaster_client_write_req(uint8_t conidx, uint8_t att_idx, uint8_t* p_data, uint16_t len)
{
    if (woodmaster_client_att_list[att_idx].handle)
    {
        gatt_client_write_t wrt;
        wrt.client_id = woodmaster_client_id;
        wrt.conidx = conidx;
        wrt.att_idx = 0;
        wrt.p_data = p_data;
        wrt.data_len = len;
        gatt_client_write_req_with_handle(wrt, woodmaster_client_att_list[att_idx].handle);
    }
}

void woodmaster_client_create(void)
{
    gatt_client_t client;
    // client.p_att_tb = 0;
    // client.att_nb = 0;
    client.p_att_tb = woodmaster_client_att_tb;
    client.att_nb = 6;
    client.gatt_msg_handler = woodmaster_client_msg_handler;
    woodmaster_client_id = gatt_add_client(&client);
    co_printf("woodmaster_client_id:%d\r\n", woodmaster_client_id);

    config = woodmaster_config();
}
void woodmaster_svc_discovery(uint8_t conidx)
{
    gatt_discovery_peer_svc(woodmaster_client_id, conidx, 2, (uint8_t*)WOODMASTER_SVC_UUID_GROUP);
}
static uint8_t rcval_tx_buff[12];
void ble_send_sticks_value(uint16_t* v)
{
    uint32_t dt = millis();
    rcval_tx_buff[0] = 0x01;
    memcpy(&rcval_tx_buff[1], v, 10);
    // memcpy(&rcval_tx_buff[9], &dt, 4);

    // woodmaster_client_write_req(woodmaster_link_conidx, WOODMASTER_CLT_IDX_RCVAL, rcval_tx_buff, 11);

    if (woodmaster_op_state_check(GATT_OP_WRITE_CMD))
    {
        woodmaster_op_state_reset(GATT_OP_WRITE_CMD);
        woodmaster_client_write_cmd(woodmaster_link_conidx, WOODMASTER_CLT_IDX_RCVAL, rcval_tx_buff, 11);
    }
}
void ble_send_button_value(uint32_t* v)
{
    rcval_tx_buff[0] = 0x02;
    memcpy(&rcval_tx_buff[1], v, 4);
    if (woodmaster_op_state_check(GATT_OP_WRITE_CMD))
    {
        woodmaster_op_state_reset(GATT_OP_WRITE_CMD);
        woodmaster_client_write_cmd(woodmaster_link_conidx, WOODMASTER_CLT_IDX_RCVAL, rcval_tx_buff, 5);
    }
}
void ble_send_button_action(uint8_t key, uint8_t action)
{
    rcval_tx_buff[0] = 0x03;
    rcval_tx_buff[1] = key;
    rcval_tx_buff[2] = action;
    woodmaster_client_write_cmd(woodmaster_link_conidx, WOODMASTER_CLT_IDX_RCVAL, rcval_tx_buff, 3);
}