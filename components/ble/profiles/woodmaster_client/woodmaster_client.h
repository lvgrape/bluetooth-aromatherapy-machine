/*** 
 * @Author: LVGRAPE
 * @Date: 2023-05-31 15:16:51
 * @LastEditTime: 2023-06-06 11:12:28
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \ZINO_BLE\components\ble\profiles\woodmaster_client\woodmaster_client.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __WOODMASTER_CLIENT_H_
#define __WOODMASTER_CLIENT_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stdint.h>
enum
{
    WOODMASTER_CLT_IDX_STATE,//状态信息，R/N
    WOODMASTER_CLT_IDX_RCVAL,//RC CHANNEL R/W
    WOODMASTER_CLT_IDX_RCCFG,//RC CHANNEL CFG R/W
    WOODMASTER_CLT_IDX_RCCMD,//RC CHANNEL CFG R/W
    WOODMASTER_CLT_IDX_CTRL,//CTRL R/W
    WOODMASTER_CLT_IDX_TEXT,//CTRL R/W
    WOODMASTER_CLT_IDX_NB,
};
extern uint8_t woodmaster_client_id; //Used for the event callback function to get the index of the event.

void woodmaster_client_create(void);
void woodmaster_svc_discovery(uint8_t conidx);
void woodmaster_client_write_req(uint8_t conidx, uint8_t att_idx, uint8_t* p_data, uint16_t len);
void woodmaster_client_write_cmd(uint8_t conidx, uint8_t att_idx, uint8_t* p_data, uint16_t len);
void woodmaster_client_read(uint8_t conidx, uint8_t att_idx);
void ble_send_sticks_value(uint16_t *v);
void ble_send_button_value(uint32_t *v);
void ble_send_button_action(uint8_t key, uint8_t action);
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
