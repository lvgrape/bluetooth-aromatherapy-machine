/*** 
 * @Author: LVGRAPE
 * @Date: 2023-03-23 10:31:49
 * @LastEditTime: 2023-03-23 10:59:30
 * @LastEditors: LVGRAPE
 * @Description: 
 * @FilePath: \FR801xH-SDK-master\components\ble\profiles\ble_spp\spp_service.h
 * @可以输入预定的版权声明、个性签名、空行等
 */

#ifndef __SPP_SERVICE_H_
#define __SPP_SERVICE_H_
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// spp service attributes index.
// Attribute index enumeration-- these indexes match array elements above
enum
{
    SPP_SERVICE_IDX,                // SPP Service
    SPP_INFO_DECL_IDX,              // HID Information characteristic declaration
    SPP_INFO_IDX,                   // HID Information characteristic
    SPP_DATA_RECV_CHAR_IDX,
    SPP_DATA_RECV_VAL_IDX,
    SPP_DATA_NOTIFY_CHAR_IDX,
    SPP_DATA_NOTIFY_VAL_IDX,
    SPP_DATA_NOTIFY_CFG_IDX,
    SPP_COMMAND_CHAR_IDX,
    SPP_COMMAND_VAL_IDX,
    SPP_STATUS_CHAR_IDX,
    SPP_STATUS_VAL_IDX,
    SPP_STATUS_CFG_IDX,
    SPP_NB_IDX,
};
void spp_gatt_add_service(void);
#ifdef __cplusplus
}
#endif // __cplusplus
#endif
