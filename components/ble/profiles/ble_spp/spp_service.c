/*
 * @Author: LVGRAPE
 * @Date: 2023-03-23 10:31:39
 * @LastEditTime: 2023-03-23 11:44:47
 * @LastEditors: LVGRAPE
 * @Description:
 * @FilePath: \FR801xH-SDK-master\components\ble\profiles\ble_spp\spp_service.c
 * 可以输入预定的版权声明、个性签名、空行等
 */
#include <stdio.h>
#include <string.h>
#include "gap_api.h"
#include "gatt_api.h"
#include "gatt_sig_uuid.h"
#include "sys_utils.h"
#include "os_task.h"

#include "spp_service.h"
 // SPP service UUID: 4880c12c-fdcb-4077-8920-a450d7f9b907
 // SPP data UUID: fec26ec4-6d71-4442-9f81-55bc21d658d6

#define serviceUUID { 0x07,\
                               0xb9,\
                               0xf9,\
                               0xd7,\
                               0x50,\
                               0xa4,\
                               0x20,\
                               0x89,\
                               0x77,\
                               0x40,\
                               0xcb,\
                               0xfd,\
                               0x2c,\
                               0xc1,\
                               0x80,\
                               0x48 }
                               
#define  charUUID { 0xd6,\
                            0x58,\
                            0xd6,\
                            0x21,\
                            0xbc,\
                            0x55,\
                            0x81,\
                            0x9f,\
                            0x42,\
                            0x44,\
                            0x71,\
                            0x6d,\
                            0xc4,\
                            0x6e,\
                            0xc2,\
                            0xfe }
const uint8_t spp_svc_uuid[] = serviceUUID;                    
const gatt_attribute_t spp_profile_att_table[] =
{
    [0]               =   { 
                                            { UUID_SIZE_2, UUID16_ARR(GATT_PRIMARY_SERVICE_UUID) },
                                            GATT_PROP_READ, 
                                            UUID_SIZE_16, 
                                            spp_svc_uuid,
                                        },
    // [1] = {
    //                                         { UUID_SIZE_16, serviceUUID },
    //                                         GATT_PROP_READ,
    //                                         16,
    //                                         NULL,
    //                                     },
    [2] = {
                                            { UUID_SIZE_16, charUUID },
                                            GATT_PROP_READ|GATT_PROP_NOTI|GATT_PROP_WRITE,
                                            16,
                                            NULL,
                                        },
    [1]       =   { 
                                            { UUID_SIZE_2, UUID16_ARR(GATT_CHARACTER_UUID) },
                                            GATT_PROP_READ, 
                                            16, 
                                            NULL,
                                        },
};
uint8_t spp_svc_id = 0;
static uint16_t spp_gatt_msg_handler(gatt_msg_t *p_msg)
{
    
    switch(p_msg->msg_evt)
    {
        case GATTC_MSG_READ_REQ:
        co_printf("GATTC_MSG_READ_REQ\n");
            //if(hid_link_enable[p_msg->conn_idx] == false)
               // return 0xffff;  //save this msg
                
            // if(p_msg->att_idx == HID_REPORT_MAP_IDX)
            // {
            //     co_printf("report_map request:%d\r\n",sizeof(hid_report_map));
            //     memcpy(p_msg->param.msg.p_msg_data, hid_report_map, sizeof(hid_report_map));
            //     return sizeof(hid_report_map);
            // }
            // else if (p_msg->att_idx == HID_INFO_IDX)
            // {
            //     co_printf("hid info request\r\n");
            //     memcpy(p_msg->param.msg.p_msg_data, hid_info_value, sizeof(hid_info_value));
            //     return sizeof(hid_info_value);
            // }
            // else
            // {
            //     for(uint8_t idx=0; idx<HID_NUM_REPORTS; idx++)
            //     {
            //         if(p_msg->att_idx == HID_REPORT_REF_FEATURE_IDX + 4 * idx)
            //         {
            //             co_printf("report_ref[%d] request\r\n",idx);
            //             memcpy(p_msg->param.msg.p_msg_data, (uint8_t *)&hid_rpt_info[idx], sizeof(hid_report_ref_t));
            //             return sizeof(hid_report_ref_t);
            //         }
            //     }
            // }
            break;

        case GATTC_MSG_WRITE_REQ:
            co_printf("GATTC_MSG_WRITE_REQ\n");
            //if(hid_link_enable[p_msg->conn_idx] == false)
                //return 0xffff;  //save this msg

            // if(p_msg->att_idx == HID_BOOT_KEY_IN_CCCD_IDX)
            // {
            //     co_printf("boot_key_ntf_enable:");
            //     show_reg(p_msg->param.msg.p_msg_data,p_msg->param.msg.msg_len,1);
            // }
            // else if(p_msg->att_idx == HID_FEATURE_IDX + 4 * 3)
            // {
            //     co_printf("write report_id 3:");
            //     show_reg(p_msg->param.msg.p_msg_data,p_msg->param.msg.msg_len,1);
            //     uint8_t *data = p_msg->param.msg.p_msg_data;
            //     if(data[0] & (1<<0))
            //         co_printf("num_lock on\r\n");
            //     else
            //         co_printf("num_lock off\r\n");
            //     if (data[0] & (1<<1))
            //         co_printf("caps_lock on\r\n");
            //     else
            //         co_printf("caps_lock off\r\n");
            //     hid_link_ntf_enable[p_msg->conn_idx] = true;
            // }
            // else
            // {
            //     for(uint8_t idx=0; idx<HID_NUM_REPORTS; idx++)
            //     {
            //         if(p_msg->att_idx == HID_FEATURE_CCCD_IDX + 4 * idx)
            //         {
            //             co_printf("ntf_enable[%d]:",idx);
            //             show_reg(p_msg->param.msg.p_msg_data,p_msg->param.msg.msg_len,1);
            //         }
            //         if(idx == 2)
            //             hid_link_ntf_enable[p_msg->conn_idx] = true;
            //     }
            // }
            break;

        case GATTC_MSG_CMP_EVT:
            co_printf("GATTC_MSG_CMP_EVT\n");
            // hid_gatt_op_cmp_handler((gatt_op_cmp_t*)&(p_msg->param.op));
            break;
        case GATTC_MSG_LINK_CREATE:
            co_printf("link[%d] create\r\n",p_msg->conn_idx);
            // hid_link_ntf_enable[p_msg->conn_idx] = true;
            break;
        case GATTC_MSG_LINK_LOST:
            co_printf("link[%d] lost\r\n",p_msg->conn_idx);
            // hid_link_ntf_enable[p_msg->conn_idx] = false;
            // hid_link_enable[p_msg->conn_idx] = false;
            break;
        default:
            break;
    }
    return 0;
}
void spp_gatt_add_service(void)
{
    gatt_service_t spp_profie_svc;

    spp_profie_svc.p_att_tb = spp_profile_att_table;
    spp_profie_svc.att_nb = 2;
    spp_profie_svc.gatt_msg_handler = spp_gatt_msg_handler;

    // hid_rpt_info[0].report_id = 1;     //refer to report map, this is Mouse
    // hid_rpt_info[0].report_type = HID_REPORT_TYPE_INPUT;

    // hid_rpt_info[1].report_id = 2;      //refer to report map, this is Cosumer Controller
    // hid_rpt_info[1].report_type = HID_REPORT_TYPE_INPUT;

    // hid_rpt_info[2].report_id = 3;      //refer to report map, this is Keyboard input.
    // hid_rpt_info[2].report_type = HID_REPORT_TYPE_INPUT;     //att_table, perm must be GATT_PROP_READ | GATT_PROP_NOTI

    // hid_rpt_info[3].report_id = 3;      //refer to report map, this is Keyboard output.
    // hid_rpt_info[3].report_type = HID_REPORT_TYPE_OUTPUT;   //att_table, perm must be GATT_PROP_READ | GATT_PROP_WRITE

    spp_svc_id = gatt_add_service(&spp_profie_svc);
}