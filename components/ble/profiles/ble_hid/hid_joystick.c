#include "hid_service.h"
#define MAIN_ITEM_INPUT (0x80)
#define MAIN_ITEM_OUPUT (0x90)
#define MAIN_ITEM_COLLECTION (0xA0)
#define MAIN_ITEM_FEATURE (0xB0)
#define MAIN_ITEM_END_COLLECTION (0xC0)

#define GLOBAL_ITEM_USAGE_PAGE (0x04)
#define GLOBAL_ITEM_LOGICAL_MINI (0x14)
#define GLOBAL_ITEM_LOGICAL_MAXI (0x24)
#define GLOBAL_ITEM_PHYSICAL_MINI (0x34)
#define GLOBAL_ITEM_PHYSICAL_MAXI (0x44)
#define GLOBAL_ITEM_UNIT_EXPONENT (0x54)
#define GLOBAL_ITEM_UNIT (0x64)
#define GLOBAL_ITEM_REPORT_SIZE (0x74)
#define GLOBAL_ITEM_REPORT_ID (0x84)
#define GLOBAL_ITEM_REPORT_COUNT (0x94)
#define GLOBAL_ITEM_PUSH (0xA4)
#define GLOBAL_ITEM_POP (0xB4)

#define LOCAL_ITEM_USAGE (0x08)
#define LOCAL_ITEM_USAGE_MINI (0x18)
#define LOCAL_ITEM_USAGE_MAXI (0x28)
#define LOCAL_ITEM_DESIGNATOR_INDEX (0x38)
#define LOCAL_ITEM_DESIGNATOR_MINI (0x48)
#define LOCAL_ITEM_DESIGNATOR_MAXI (0x58)
#define LOCAL_ITEM_STRING_INDEX (0x68)
#define LOCAL_ITEM_STRING_MINI (0x78)
#define LOCAL_ITEM_STRING_MAXI (0x88)
#define LOCAL_ITEM_DELIMITER (0x98)

#define bSize_0bytes (0x00)
#define bSize_1bytes (0x01)
#define bSize_2bytes (0x02)
#define bSize_4bytes (0x03)

#define Physical (0x00)
#define Application (0x01)
#define Logical (0x02)
#define Report (0x03)
#define NameArray (0x04)
#define UasgeSwitch (0x05)
#define UsageModifier (0x06)

#define GenericDesktopPage (0x01)
#define SimulationControlsPage (0x02)
#define VRControlsPage (0x03)
#define SportControlsPage (0x04)
#define GameControlsPage (0x05)
#define GenericDeviceControlsPage (0x06)
#define KeyboardKeypadPage (0x07)
#define LEDPage (0x08)
#define ButtonPage (0x09)
#define OrdinalPage (0x0A)
#define TelephonyDevicePage (0x0B)
#define ConsumerPage (0x0C)
#define DigitizersPage (0x0D)
#define HapticsPage (0x0E)
#define PhysicalInputDevicePage (0x0F)
#define UnicodePage (0x10)
#define SoCPage (0x11)
#define EyeandHeadTrackersPage (0x12)
#define AuxiliaryDisplayPage (0x14)
#define SensorsPage (0x20)
#define MedicalInstrumentPage (0x40)
#define BrailleDisplayPage (0x41)
#define LightingAndIlluminationPage (0x59)
#define MonitorPage (0x80)
#define MonitorEnumeratedPage (0x81)
#define VESAVirtualControlsPage (0x82)
#define PowerPage (0x84)
#define BatterySystemPage (0x85)
#define BarcodeScannerPage (0x8C)
#define ScalesPage (0x8D)
#define MagneticStripeReaderPage (0x8E)
#define CameraControlPage (0x90)
#define ArcadePage (0x91)
#define GamingDevicePage (0x92)
#define FIDOAlliancePage (0xF1D0)

#define Pointer 0X01
#define Mouse 0X02
#define Joystick 0x04
#define GamePad 0x05
#define Keyboard 0x06
#define NumKeyboard 0x07
#define SystemControl 0x80

#define X (0X30)
#define Y (0X31)
#define Z (0X32)
#define Hatswitch (0x32)
#define UsagePage(x)        (GLOBAL_ITEM_USAGE_PAGE | bSize_1bytes),(x)
#define Usage(x)            (LOCAL_ITEM_USAGE | bSize_1bytes),(x)
#define Collection(x)       (MAIN_ITEM_COLLECTION | bSize_1bytes),(x)
#define UsageMinimum(x)     (LOCAL_ITEM_USAGE_MINI | bSize_1bytes),(x)
#define UsageMaximum(x)     (LOCAL_ITEM_USAGE_MAXI | bSize_1bytes),(x)
#define LogicalMinimum(x)   (GLOBAL_ITEM_LOGICAL_MINI | bSize_1bytes),(x)
#define LogicalMaximum(x)   (GLOBAL_ITEM_LOGICAL_MINI | bSize_1bytes),(x)
#define PhysicalMinimum(x)  (GLOBAL_ITEM_PHYSICAL_MINI | bSize_1bytes),(x)
#define PhysicalMaximum(x)  (GLOBAL_ITEM_PHYSICAL_MINI | bSize_1bytes),(x)
#define ReportId(x)         (GLOBAL_ITEM_REPORT_ID | bSize_1bytes),(x)
#define ReportCount(x)      (GLOBAL_ITEM_REPORT_COUNT | bSize_1bytes),(x)
#define ReportSize(x)       (GLOBAL_ITEM_REPORT_SIZE | bSize_1bytes),(x)
#define Input(x)            (MAIN_ITEM_INPUT | bSize_1bytes),(x)
#define EndCollection       (MAIN_ITEM_END_COLLECTION | bSize_0bytes)
#define Push                (GLOBAL_ITEM_PUSH)
#define Pop                 (GLOBAL_ITEM_POP)
#define Unit(x)             (GLOBAL_ITEM_UNIT | bSize_1bytes),(x)
#define Unit16(x)             (GLOBAL_ITEM_UNIT | bSize_2bytes),(x)>>8,(x)&0xFF
// #define Data (0)
// #define Constant (0x01)
// #define Array (0)
// #define Variable (0x02)
// #define Absolute (0x00)
// #define Relative (0x04)
// #define NoWrap (0x00)
// #define Warp (0x08)
// #define Linear (0x00)
// #define NonLinear (0x10)
/******************************* HID Report Map characteristic defination */
const uint8_t hid_joystick_descriptor[79] = {
    UsagePage(GenericDesktopPage),
    Usage(Joystick),
    Collection(Application),
        UsagePage(GenericDesktopPage),
        Usage(Pointer),
        Collection(Physical),

            LogicalMinimum(-127),
            LogicalMaximum(127),
            ReportSize(8),
            ReportCount(2),
            Push,
            Usage(X),//#USAGE x
            Usage(Y),//#USAGE y
            Input(0x02),//Input(Data,Variable,Absolute)
            
            Usage(Hatswitch),
            LogicalMinimum(0),
            LogicalMaximum(3),
            PhysicalMinimum(0),
            PhysicalMaximum(255),
            Unit16(954),//Unit(Degree)
            ReportCount(1),
            ReportSize(4),
            Input(0x02), //Input(Data,Variable,Absolute,NullState)

            LogicalMinimum(0),
            LogicalMaximum(1),
            ReportCount(8),
            ReportSize(1),
            UsagePage(ButtonPage),
            UsageMinimum(1),
            UsageMaximum(10),
            Unit(0),
            Input(0x02), //Input(Data,Variable,Absolute)

        EndCollection,

        UsageMinimum(1),
        UsageMinimum(4),
        Input(0x02), //Input(Data,Variable,Absolute),
        Pop,
        Usage(0xbb),
        ReportCount(1),
        Input(0x02), //Input(Data,Variable,Absolute),

    EndCollection
};